<?php

Yii::import('application.models._base.BaseCmsIblockElements');

class CmsIblockElements extends BaseCmsIblockElements
{
	const IMAGES_DIR = 'uploads/iBlockImageElements/';
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function getImage($prefix = null) {
		return self::IMAGES_DIR . ($prefix ? $prefix.$this->image : $this->image);
	}
}