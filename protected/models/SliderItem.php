<?php

Yii::import('application.models._base.BaseSliderItem');

use WebPConvert\WebPConvert;

class SliderItem extends BaseSliderItem
{
	const IMAGE_DIRECTORY = '/uploads/slider/';
	const IMAGE_MAX_SIZE = 6242880;
	const IMAGE_BIG_WIDTH = 1920;
	const IMAGE_LARGE_WIDTH = 1240;

	protected $imageExtension = 'jpg';

	public $image;
	public $position;
	public $active;
	public $title;
	public $content;
	public $video;
	public $imageName;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array_merge(parent::rules(), [
			['image', 'file', 'types' => 'jpg, png, gif, jpeg', 'allowEmpty' => true, 'maxSize' => self::IMAGE_MAX_SIZE],
		]);
	}

	private function newImage() {
		return uniqid() . '.' . $this->imageExtension;
	}

	public function getImage() {
		if ($this->imageName) {
			return self::IMAGE_DIRECTORY . $this->imageName;
		}
	}

	public function afterSave() {
		if(!empty($this->image))
			$this->saveFiles();
		return parent::afterSave();
	}

	private function saveFiles() {
		$temp_name = $this->image->getTempName();
		$directory = self::IMAGE_DIRECTORY;

		$this->deleteFiles();
		$this->imageName  = $this->newImage();

		list($width, $height) = getimagesize($temp_name);

		$image = new EasyImage($temp_name);
		$width = $width > self::IMAGE_BIG_WIDTH ? self::IMAGE_BIG_WIDTH : $width;

		$image
			->resize($width)
			->save(getcwd() . $directory . $this->imageName);

		WebPConvert::convert(getcwd() . $directory . $this->imageName, getcwd() . $directory . $this->imageName.'.webp', [
			'quality' => 'auto',
			'max-quality' => 80,
		]);

		$this->isNewRecord = false;
		$this->saveAttributes($this->attributes);
	}

	private function deleteFiles() {
		if ($this->isNewRecord == false) {
			$directory = self::IMAGE_DIRECTORY;
			if ($this->imageName)   @unlink(getcwd() . $directory . $this->imageName);
			if ($this->imageName)   @unlink(getcwd() . $directory . $this->imageName.'.webp');
		}
	}

	public function beforeDelete()
	{
		$this->deleteImages();
		return parent::beforeDelete();
	}

	public function deleteImages() {
		if ($this->isNewRecord == false) {
			$this->deleteFiles();
			$this->updateByPk($this->id, [
				'imageName' => null,
			]);
		}
	}

}