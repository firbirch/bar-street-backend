<?php

Yii::import('application.models._base.BaseCmsModuleAdvantage');

class CmsModuleAdvantage extends BaseCmsModuleAdvantage
{

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getAdvantages(CmsModuleService $service) {
		$result = []; $i = 0;
		$query = $this->findAllByAttributes(['cms_module_service_service_id' => $service->service_id]);
		$oAdvantage = CmsModuleServiceAdvantages::model();

		foreach($query as $item) {
			if(isset($item->cms_module_params) && !empty($item->cms_module_params)) {
				$advantage = $oAdvantage->findByPk($item->cms_module_service_advantages_advantage_id, 'advantage_active = 1');
				if(!$advantage) continue;

				foreach($advantage->cmsAdvantageParams as $params) {
					if($params->params_id == $item->cms_module_params) {
						$advantage->advantage_content = $params->params_content;
						break;
					}
				}
			}
			else {
				$advantage = $oAdvantage->findByPk($item->cms_module_service_advantages_advantage_id);
			}
			if(isset($advantage->advantage_content) && !empty($advantage->advantage_content)) {
				$advantage->advantage_content = str_replace(['&nbsp;', '&ndash;', '&mdash;'], '', $advantage->advantage_content);
			}
			$result[$i] = $advantage;
			$i++;
		}
		return $result;
	}

}