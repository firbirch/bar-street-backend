<?php

Yii::import('application.models._base.BaseCmsModuleGalleryImage');

class CmsModuleGalleryImage extends BaseCmsModuleGalleryImage
{
	const IMAGES_DIR = '/uploads/gallery/';
	const IMAGES_DIR_WEB = '/uploads/webp/gallery/';

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function getImage($prefix = null, $jpg = false) { //@TODO Правка
		$path_parts = pathinfo(self::IMAGES_DIR.$this->image);
		$pathWebp = DOC_ROOT.self::IMAGES_DIR_WEB . $path_parts['filename']. '.webp';
		if (!$jpg && file_exists($pathWebp) == true)
			return self::IMAGES_DIR_WEB . $path_parts['filename']. '.webp';
		else
			return $prefix ? self::IMAGES_DIR . $prefix . $this->image : self::IMAGES_DIR . $this->image;
	}

	public function getImageAlt() {
	    return $this->imageAlt;
    }
    public function getImageTitle() {
        return $this->imageTitle;
    }

	/*public function getImage($prefix = null, $jpg = false) {
		if(!$jpg) {
			//$convertedImage = new WebpConverted();
			//$convertedImage->convertedImage(DOC_ROOT . self::IMAGES_DIR . $this->image, '/gallery/');
			$path_parts = pathinfo(self::IMAGES_DIR.$this->image);
			$pathWebp = DOC_ROOT.self::IMAGES_DIR_WEB . $path_parts['filename']. '.webp';
			if(!file_exists($pathWebp)) {
				$convertedImage = new WebpConverted();
				$convertedImage->convertedImage(DOC_ROOT . self::IMAGES_DIR . $this->image, '/gallery/');
			}
		}
		if($this->image) {
			if (!$jpg && file_exists($pathWebp) == true)
				return self::IMAGES_DIR_WEB . $path_parts['filename']. '.webp';
			else
				return $prefix ? self::IMAGES_DIR . $prefix . $this->image : self::IMAGES_DIR . $this->image;
		}
	}*/
}
