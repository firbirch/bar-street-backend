<?php

Yii::import('application.models._base.BaseCmsSiteSettings');

class CmsSiteSettings extends BaseCmsSiteSettings
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function rules() {
		return array_merge(parent::rules(), [
			['field_name, field_value', 'required'],
			['field_name', 'unique']
		]);
	}

	public function getSettings() {
		$result = [];
		$oSettings = self::model()->findAll('id != 1');
		foreach($oSettings as $setting) {
			if(empty($setting->field_value))
				$setting->field_value = '<!-- empty -->';
			$result[$setting->field_name] = $setting;
		}
		return $result;
	}

}