<?php

Yii::import('application.models._base.BaseSliderCategories');

class SliderCategories extends BaseSliderCategories
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function relations() {
		return array_merge(
			parent::relations(),
			[
				'sliderItems' => [self::HAS_MANY, 'SliderItem', 'categoryId', 'order' => 'active desc, position asc'],
				'sliderActiveItems' => [self::HAS_MANY, 'SliderItem', 'categoryId', 'order' => 'position asc', 'on' => 'active = 1'],
			]
		);
	}

}