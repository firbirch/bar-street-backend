<?php

Yii::import('application.models._base.BaseCmsModuleServicesItemsRelations');

class CmsModuleServicesItemsRelations extends BaseCmsModuleServicesItemsRelations
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function getItem()
	{
		return CmsModuleServiceItems::model()->findByPk($this->serviceItemId);
	}

}