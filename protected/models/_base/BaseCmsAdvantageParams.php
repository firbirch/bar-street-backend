<?php

/**
 * This is the model base class for the table "cms_advantage_params".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CmsAdvantageParams".
 *
 * Columns in table "cms_advantage_params" available as properties of the model,
 * followed by relations of table "cms_advantage_params" available as properties of the model.
 *
 * @property integer $params_id
 * @property string $params_content
 * @property string $params_tag
 * @property integer $cms_module_service_advantages_advantage_id
 *
 * @property CmsModuleServiceAdvantages $cmsModuleServiceAdvantagesAdvantage
 */
abstract class BaseCmsAdvantageParams extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'cms_advantage_params';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CmsAdvantageParams|CmsAdvantageParams', $n);
	}

	public static function representingColumn() {
		return 'params_content';
	}

	public function rules() {
		return array(
			array('cms_module_service_advantages_advantage_id', 'required'),
			array('cms_module_service_advantages_advantage_id', 'numerical', 'integerOnly'=>true),
			array('params_tag', 'length', 'max'=>60),
			array('params_content', 'safe'),
			array('params_content, params_tag', 'default', 'setOnEmpty' => true, 'value' => null),
			array('params_id, params_content, params_tag, cms_module_service_advantages_advantage_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'cmsModuleServiceAdvantagesAdvantage' => array(self::BELONGS_TO, 'CmsModuleServiceAdvantages', 'cms_module_service_advantages_advantage_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'params_id' => Yii::t('app', 'Params'),
			'params_content' => Yii::t('app', 'Params Content'),
			'params_tag' => Yii::t('app', 'Params Tag'),
			'cms_module_service_advantages_advantage_id' => null,
			'cmsModuleServiceAdvantagesAdvantage' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('params_id', $this->params_id);
		$criteria->compare('params_content', $this->params_content, true);
		$criteria->compare('params_tag', $this->params_tag, true);
		$criteria->compare('cms_module_service_advantages_advantage_id', $this->cms_module_service_advantages_advantage_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}