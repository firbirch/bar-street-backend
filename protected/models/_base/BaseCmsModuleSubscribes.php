<?php

/**
 * This is the model base class for the table "cms_module_subscribes".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CmsModuleSubscribes".
 *
 * Columns in table "cms_module_subscribes" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $subscribe_id
 * @property string $subscribe_email
 * @property integer $subscribe_active
 * @property string $subscribe_date
 * @property integer $subscribe_sort
 *
 */
abstract class BaseCmsModuleSubscribes extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'cms_module_subscribes';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CmsModuleSubscribes|CmsModuleSubscribes', $n);
	}

	public static function representingColumn() {
		return 'subscribe_email';
	}

	public function rules() {
		return array(
			array('subscribe_active, subscribe_sort', 'numerical', 'integerOnly'=>true),
			array('subscribe_email', 'length', 'max'=>120),
			array('subscribe_date', 'safe'),
			array('subscribe_email, subscribe_active, subscribe_date, subscribe_sort', 'default', 'setOnEmpty' => true, 'value' => null),
			array('subscribe_id, subscribe_email, subscribe_active, subscribe_date, subscribe_sort', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'subscribe_id' => Yii::t('app', 'Subscribe'),
			'subscribe_email' => Yii::t('app', 'Subscribe Email'),
			'subscribe_active' => Yii::t('app', 'Subscribe Active'),
			'subscribe_date' => Yii::t('app', 'Subscribe Date'),
			'subscribe_sort' => Yii::t('app', 'Subscribe Sort'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('subscribe_id', $this->subscribe_id);
		$criteria->compare('subscribe_email', $this->subscribe_email, true);
		$criteria->compare('subscribe_active', $this->subscribe_active);
		$criteria->compare('subscribe_date', $this->subscribe_date, true);
		$criteria->compare('subscribe_sort', $this->subscribe_sort);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}