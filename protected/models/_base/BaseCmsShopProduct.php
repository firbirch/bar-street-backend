<?php

/**
 * This is the model base class for the table "cms_shop_product".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CmsShopProduct".
 *
 * Columns in table "cms_shop_product" available as properties of the model,
 * followed by relations of table "cms_shop_product" available as properties of the model.
 *
 * @property string $product_id
 * @property string $product_name
 * @property integer $product_cost
 * @property integer $product_sort
 * @property integer $product_active
 * @property string $product_slug
 * @property string $product_content
 * @property string $product_preview_content
 * @property integer $cms_shop_category_category_id
 * @property integer $cms_shop_product_labels_label_id
 *
 * @property CmsShopProductLabels $cmsShopProductLabelsLabel
 * @property CmsShopCategory $cmsShopCategoryCategory
 * @property CmsShopProductImages[] $cmsShopProductImages
 * @property CmsShopProductParams[] $cmsShopProductParams
 */
abstract class BaseCmsShopProduct extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'cms_shop_product';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CmsShopProduct|CmsShopProducts', $n);
	}

	public static function representingColumn() {
		return 'product_name';
	}

	public function rules() {
		return array(
			array('cms_shop_category_category_id, cms_shop_product_labels_label_id', 'required'),
			array('product_cost, product_sort, product_active, cms_shop_category_category_id, cms_shop_product_labels_label_id', 'numerical', 'integerOnly'=>true),
			array('product_name', 'length', 'max'=>45),
			array('product_slug', 'length', 'max'=>100),
			array('product_content, product_preview_content', 'safe'),
			array('product_name, product_cost, product_sort, product_active, product_slug, product_content, product_preview_content', 'default', 'setOnEmpty' => true, 'value' => null),
			array('product_id, product_name, product_cost, product_sort, product_active, product_slug, product_content, product_preview_content, cms_shop_category_category_id, cms_shop_product_labels_label_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'cmsShopProductLabelsLabel' => array(self::BELONGS_TO, 'CmsShopProductLabels', 'cms_shop_product_labels_label_id'),
			'cmsShopCategoryCategory' => array(self::BELONGS_TO, 'CmsShopCategory', 'cms_shop_category_category_id'),
			'cmsShopProductImages' => array(self::HAS_MANY, 'CmsShopProductImages', 'cms_shop_product_product_id'),
			'cmsShopProductParams' => array(self::HAS_MANY, 'CmsShopProductParams', 'cms_shop_parmas_product_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'product_id' => Yii::t('app', 'Product'),
			'product_name' => Yii::t('app', 'Product Name'),
			'product_cost' => Yii::t('app', 'Product Cost'),
			'product_sort' => Yii::t('app', 'Product Sort'),
			'product_active' => Yii::t('app', 'Product Active'),
			'product_slug' => Yii::t('app', 'Product Slug'),
			'product_content' => Yii::t('app', 'Product Content'),
			'product_preview_content' => Yii::t('app', 'Product Preview Content'),
			'cms_shop_category_category_id' => null,
			'cms_shop_product_labels_label_id' => null,
			'cmsShopProductLabelsLabel' => null,
			'cmsShopCategoryCategory' => null,
			'cmsShopProductImages' => null,
			'cmsShopProductParams' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('product_id', $this->product_id, true);
		$criteria->compare('product_name', $this->product_name, true);
		$criteria->compare('product_cost', $this->product_cost);
		$criteria->compare('product_sort', $this->product_sort);
		$criteria->compare('product_active', $this->product_active);
		$criteria->compare('product_slug', $this->product_slug, true);
		$criteria->compare('product_content', $this->product_content, true);
		$criteria->compare('product_preview_content', $this->product_preview_content, true);
		$criteria->compare('cms_shop_category_category_id', $this->cms_shop_category_category_id);
		$criteria->compare('cms_shop_product_labels_label_id', $this->cms_shop_product_labels_label_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}