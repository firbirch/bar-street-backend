<?php

/**
 * This is the model base class for the table "sliderItem".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SliderItem".
 *
 * Columns in table "sliderItem" available as properties of the model,
 * followed by relations of table "sliderItem" available as properties of the model.
 *
 * @property string $id
 * @property string $categoryId
 * @property string $content
 * @property string $title
 * @property string $imageName
 * @property string $video
 * @property integer $position
 * @property integer $active
 *
 * @property SliderCategories $category
 */
abstract class BaseSliderItem extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'sliderItem';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'SliderItem|SliderItems', $n);
	}

	public static function representingColumn() {
		return 'content';
	}

	public function rules() {
		return array(
			array('categoryId', 'required'),
			array('position, active', 'numerical', 'integerOnly'=>true),
			array('categoryId', 'length', 'max'=>10),
			array('imageName', 'length', 'max'=>255),
			array('content, title, video', 'safe'),
			array('content, title, imageName, video, position, active', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, categoryId, content, title, imageName, video, position, active', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'category' => array(self::BELONGS_TO, 'SliderCategories', 'categoryId'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'categoryId' => null,
			'content' => Yii::t('app', 'Content'),
			'title' => Yii::t('app', 'Title'),
			'imageName' => Yii::t('app', 'Image Name'),
			'video' => Yii::t('app', 'Video'),
			'position' => Yii::t('app', 'Position'),
			'active' => Yii::t('app', 'Active'),
			'category' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('categoryId', $this->categoryId);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('imageName', $this->imageName, true);
		$criteria->compare('video', $this->video, true);
		$criteria->compare('position', $this->position);
		$criteria->compare('active', $this->active);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}