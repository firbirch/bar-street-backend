<?php

/**
 * This is the model base class for the table "cms_module_service_category".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CmsModuleServiceCategory".
 *
 * Columns in table "cms_module_service_category" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $category_id
 * @property string $category_active
 * @property string $category_sort
 * @property string $category_name
 * @property string $category_name_en
 * @property string $category_icon
 *
 */
abstract class BaseCmsModuleServiceCategory extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'cms_module_service_category';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'CmsModuleServiceCategory|CmsModuleServiceCategories', $n);
	}

	public static function representingColumn() {
		return 'category_active';
	}

	public function rules() {
		return array(
			array('category_active, category_sort', 'length', 'max'=>45),
			array('category_name', 'length', 'max'=>100),
			array('category_name_en', 'length', 'max'=>255),
			array('category_icon', 'length', 'max'=>120),
			array('category_active, category_sort, category_name, category_name_en, category_icon', 'default', 'setOnEmpty' => true, 'value' => null),
			array('category_id, category_active, category_sort, category_name, category_name_en, category_icon', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'cmsModuleServices' => array(self::HAS_MANY, 'CmsModuleService', 'cms_module_service_category_category_id'),
		);
	}


	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'category_id' => Yii::t('app', 'Category'),
			'category_active' => Yii::t('app', 'Category Active'),
			'category_sort' => Yii::t('app', 'Category Sort'),
			'category_name' => Yii::t('app', 'Category Name'),
			'category_name_en' => Yii::t('app', 'Category Name En'),
			'category_icon' => Yii::t('app', 'Category Icon'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('category_active', $this->category_active, true);
		$criteria->compare('category_sort', $this->category_sort, true);
		$criteria->compare('category_name', $this->category_name, true);
		$criteria->compare('category_name_en', $this->category_name_en, true);
		$criteria->compare('category_icon', $this->category_icon, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}