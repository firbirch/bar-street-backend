<?php

Yii::import('application.models._base.BaseCmsModuleSlider');

class CmsModuleSlider extends BaseCmsModuleSlider
{
	public $context;

	const IMAGES_DIR = '/uploads/slider/';

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(), [
			['name, active', 'required']
		]);
	}

	public function getImage($prefix = null) {
		if(!$this->image) return false;
		return self::IMAGES_DIR . ($prefix ? $prefix.$this->image : $this->image);
	}
	public function getMobileImage($prefix = null) {
		if(!$this->mobile_image) return false;
		return self::IMAGES_DIR . ($prefix ? $prefix.$this->mobile_image : $this->mobile_image);
	}


	public function getCss() { return $this->css_class; }

}