<?php

Yii::import('application.models._base.BaseCmsModuleSubcategory');

class CmsModuleSubcategory extends BaseCmsModuleSubcategory
{
	public $subcategory_name_en;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}