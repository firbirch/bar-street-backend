<?php

Yii::import('application.models._base.BaseCmsModuleServiceItems');

class CmsModuleServiceItems extends BaseCmsModuleServiceItems
{
	const IMAGES_DIR = '/uploads/servicesItems/';

	static public $types = [
		'wear' => 'Одежда',
		'hookah' => 'Кальян',
	];

	public $image;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function getImage() {
		if(!$this->imageName) return '';
		return self::IMAGES_DIR.$this->imageName;
	}

	public function saveRelations(array $servicesId)
	{
		if(!empty($servicesId)) {
			CmsModuleServicesItemsRelations::model()->deleteAllByAttributes(['serviceItemId' => $this->id]);
		}
		foreach ($servicesId as $id) {
			$rel = new CmsModuleServicesItemsRelations;
			$rel->serviceId = $id;
			$rel->serviceItemId = $this->id;
			$rel->save(false);
		}
	}

	public function relations()
	{
		return [
			'services' => [self::HAS_MANY, 'CmsModuleServicesItemsRelations', 'serviceItemId']
		];
	}

	public function getUsedServices()
	{
		foreach ($this->services as $service) {
			$r[] = $service->serviceId;
		}
		return $r ?? [];
	}
}