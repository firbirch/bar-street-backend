<?php

Yii::import('application.models._base.BaseCmsModuleGallery');
Yii::import('application.components.Helpers');

class CmsModuleGallery extends BaseCmsModuleGallery
{

	const IMAGES_DIR = '/uploads/gallery/';
	const IMAGES_DIR_WEB = '/uploads/webp/gallery/';


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules()
	{
		return array_merge(parent::rules(), [
			['name, g_active, date', 'required']
		]);
	}


	public function getGalleries() {
		return CmsModuleGalleryImage::model()->findAllByAttributes(['cms_module_gallery_id' => $this->id], ['order' => 'i_sort ASC']);
	}

	public function getAnnounceImage($prefix = null, $jpg = false) {
		if(!$jpg) {
			$convertedImage = new WebpConverted();
			$convertedImage->convertedImage(DOC_ROOT . self::IMAGES_DIR . $this->announce_image, '/gallery/');
			$path_parts = pathinfo(self::IMAGES_DIR.$this->announce_image);
			$pathWebp = DOC_ROOT.self::IMAGES_DIR_WEB . $path_parts['filename']. '.webp';
		}
		if($this->announce_image) {
			if (!$jpg && file_exists($pathWebp) == true)
				return self::IMAGES_DIR_WEB . $path_parts['filename']. '.webp';
			else
				return $prefix ? self::IMAGES_DIR . $prefix . $this->announce_image : self::IMAGES_DIR . $this->announce_image;
		}
	}

	public function getAnnounce() { return $this->announce_content; }
	public function getName() { return (string)$this->name; }


	public function getGalleryUrl() {
		return Yii::app()->createUrl('/photoalbum/show', [
			'show' => $this->date
		]);
	}

	public function getDate() {
		return (new Helpers())->date_rus($this->date);
	}



	public function getRandomImagesFromService() {

		$criteria = new CDbCriteria();
		$criteria->limit = 4;
		$criteria->order = 'RAND()';
		$criteria->condition = 'id != :id AND id != 1 AND name IS NOT NULL';
		$criteria->params = [':id' => $this->id];
		return self::findAll($criteria);
	}

	public function getServices() {

		$oService = CmsModuleService::model();
		$result = [];
		if(is_array($this->cmsGalleryRelationServices)) {
			foreach($this->cmsGalleryRelationServices as $service) {
				$result[] = $oService->findByAttributes(['service_id' => $service->cms_module_service_service_id]);
			}
		}
		return $result;
	}

	public function getServiceIds() {

		$result = [];
		if(is_array($this->cmsGalleryRelationServices)) {
			foreach ($this->cmsGalleryRelationServices as $service) {
				$result[] = $service->cms_module_service_service_id;
			}
		}
		return $result;
	}
	public function getJsonMap() {
		$result = [];
		$result['type'] = 'FeatureCollection';
		foreach($this->findAll('id != 1') as $row) {
			if(empty($row->lon)) continue;
			$result['features'][] = [
				'type' => 'Feature',
				'geometry' => [
					'type' => 'Point',
					'coordinates' => [
						$row->lat,
						$row->lon
					]
				],
				'properties' => [
					'id' => $row->id,
					'title' => $row->name,
					'date' => $row->date,
					'link' => $row->getGalleryUrl(),
					'img' => $row->getAnnounceImage(null, true),
					'placeName' => $row->map_name
				]
			];
		}
		return $this->renderJSON($result);
	}
	protected function renderJSON($data)
	{
		header('Content-type: application/json');
		echo CJSON::encode($data);

		foreach (Yii::app()->log->routes as $route) {
			if($route instanceof CWebLogRoute) {
				$route->enabled = false; // disable any weblogroutes
			}
		}
		Yii::app()->end();
	}
}