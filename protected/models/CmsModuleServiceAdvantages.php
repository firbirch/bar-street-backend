<?php

Yii::import('application.models._base.BaseCmsModuleServiceAdvantages');

class CmsModuleServiceAdvantages extends BaseCmsModuleServiceAdvantages
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	const IMAGES_DIR = '/uploads/services/advantage/';


	public function getContent() { return $this->advantage_content; }


	public function rules()
	{
		return array_merge(parent::rules(),[
			['advantage_name', 'unique'],
			['advantage_name', 'required']
		]);
	}
}