<?php

Yii::import('application.models._base.BaseCmsModuleProductOrder');

class CmsModuleProductOrder extends BaseCmsModuleProductOrder
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	const STATUS = [
		'Ожидание', 'Исполняется', 'Исполнен'
	];


	public function rules()
	{
		return array_merge(parent::rules(), [
			['order_name, order_phone, order_email', 'required'],
			['order_email', 'email']
		]); // TODO: Change the autogenerated stub
	}



	public function getProducts() {
		return json_decode($this->order_products);
	}

	public function beforeSave()
	{
		if($this->isNewRecord) {
			$result = null;
			foreach (Yii::app()->shoppingCart->getPositions() as $key => $position) {
				$result[$key]['product_name'] = $position->product_name;
				$result[$key]['product_quantity'] = $position->getQuantity();
				$result[$key]['product_price'] = $position->getSumPrice();
			}
			$this->order_products = json_encode($result);
			$this->order_cost = Yii::app()->shoppingCart->getCost() + 6000;
			$this->order_date = date('Y-m-d');
		}
		return parent::beforeSave();
	}

	public function afterSave()
	{

		if($this->isNewRecord) {
			$oSettings = CmsSiteSettings::model()->findByAttributes(['field_name' => 'sendMail']);
			if($oSettings && !empty($oSettings->field_value)) {
				$email = $oSettings->field_value;

				$mail = new YiiMailer();
				$mail->setFrom('no-reply@bar-street.ru');

				if(filter_var($this->order_email,FILTER_VALIDATE_EMAIL))
					$mail->setTo([$email, $this->order_email]);
				else
					$mail->setTo($email);

				$mail->setSubject('[Покупка коктелей] Заказ #'.$this->order_id);

				$mail->setData([
					'name' => $this->order_name,
					'phone' => $this->order_phone,
					'mail' => $this->order_email,
					'total' => Helpers::format_price($this->order_cost)
				]);
				$mail->setLayout('mailOrder');
				$mail->setView('ProductOrder');


				if(!$mail->send())
					throw new Exception($mail->getError());

				Yii::app()->shoppingCart->clear();
			}
		}
		return parent::afterSave(); // TODO: Change the autogenerated stub
	}
}