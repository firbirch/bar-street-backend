<?php

Yii::import('application.models._base.BaseCmsShopCategory');

class CmsShopCategory extends BaseCmsShopCategory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules()
	{
		return array_merge(parent::rules(), [
			['category_name', 'unique'],
		]);
	}

}