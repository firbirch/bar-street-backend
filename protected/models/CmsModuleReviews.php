<?php

Yii::import('application.models._base.BaseCmsModuleReviews');

class CmsModuleReviews extends BaseCmsModuleReviews
{
	public $review_rang;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function rules()
	{
		return array_merge(parent::rules(), [
			['review_name', 'required']
		]);
	}
}