<?php

Yii::import('application.models._base.BaseCmsShopProduct');

class CmsShopProduct extends BaseCmsShopProduct implements IECartPosition
{


	const NO_IMAGE_PATH = '/image_admin/images/no-product.png';
	const IMAGE_PATH = '/uploads/products/';

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function relations()
	{
		return array_merge(parent::relations(), [
			'cmsShopProductImagesPreview' => array(self::HAS_ONE, 'CmsShopProductImages', 'cms_shop_product_product_id', 'condition' => 'image_preview = 1'),
		]);
	}

	public function getImage() {

		return isset($this->cmsShopProductImagesPreview->image_url) ? self::IMAGE_PATH.$this->cmsShopProductImagesPreview->image_url : self::NO_IMAGE_PATH;
	}

	function getId(){
		return 'Product'.$this->product_id;
	}
	function getPrice(){
		return $this->product_cost;
	}

}