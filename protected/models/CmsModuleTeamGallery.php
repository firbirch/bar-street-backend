<?php

Yii::import('application.models._base.BaseCmsModuleTeamGallery');

class CmsModuleTeamGallery extends BaseCmsModuleTeamGallery
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	const IMAGES_DIR = '/uploads/team/';

	public function getImage($prefix = null) {
		return self::IMAGES_DIR . ($prefix ? $prefix.$this->gallery_image : $this->gallery_image);
	}
}