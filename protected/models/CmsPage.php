<?php

Yii::import('application.models._base.BaseCmsPage');

class CmsPage extends BaseCmsPage
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function rules() {
		return array_merge(parent::rules(), [
			['name, active', 'required']
		]);
	}

	public function getPagesWithoutNested() {
		$nestedIds = [];
		$menu = CmsNestedPage::model()->findAll();
		foreach($menu as $item) {
			$nestedIds[] = $item->cms_page_id;
		}
		$criteria = $this->getDbCriteria();
		$criteria->addNotInCondition('id', $nestedIds);
		return CmsPage::model()->findAll($criteria);
	}
	public function getUrl() {

		$menu = CmsNestedPage::model()->findByAttributes(['cms_page_id' => $this->id]);
		$ancestors = $menu->ancestors()->findAll();
		$url = [];
		foreach ($ancestors as $parentPage) {
			if($parentPage->cmsPage->slug === '/') continue;
			$url[] = $parentPage->cmsPage->slug;
		}
		$url[] = $this->slug;

		return '/' . implode('/', $url);
	}


	public function getContent() {

		return $this->content;
		//return $this->findIblock($this->content);
	}




	private function findIblock($iblock) {

		if(empty($iblock)) return '';

		$iblock = htmlspecialchars_decode($iblock);
		$iblock = str_replace('&#39;', "'",$iblock);
		preg_match_all("/{iblock\('(.*?)'\)}/", $iblock, $matches);

		echo '<pre>'.print_r($matches, true).'</pre>';






		/*if(empty($this->$attr)) return '';
		preg_match_all(
			'/#([a-zA-Zа-яА-Я|]+)#/u',
			$this->$attr,
			$matches
		);*/


	}




	/*protected function beforeRender($view) {

		CVarDumper::dump($view, 10, true);


		exit;
	}*/

}