<?php

Yii::import('application.models._base.BaseCmsShopProductImages');

class CmsShopProductImages extends BaseCmsShopProductImages
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	const IMAGE_PATH = '/uploads/products/';

	public function getImage($prefix = null) {
		return self::IMAGE_PATH . ($prefix ? $prefix.$this->image_url : $this->image_url);
	}

}