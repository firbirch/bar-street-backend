<?php

Yii::import('application.models._base.BaseCmsModuleTeamEvent');

class CmsModuleTeamEvent extends BaseCmsModuleTeamEvent
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function getImage($key, $prefix = null)
	{
		if(!isset($this->cmsModuleTeamGalleries[$key])) return false;
		return CmsModuleTeamGallery::IMAGES_DIR.($prefix ? $prefix.$this->cmsModuleTeamGalleries[$key]->gallery_image : $this->cmsModuleTeamGalleries[$key]->gallery_image);
	}
}