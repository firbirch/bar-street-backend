<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 15/02/2019
 * Time: 14:32
 */


use WebPConvert\WebPConvert;

class WebpConverted {

	const WEBP_IMAGES_PATH = DOC_ROOT.'uploads/webp/';

	public function convertedImage($imageOriginal, $prefix = null) {

		return true; //@TODO Конвертация отключена

		$path_parts = pathinfo($imageOriginal);
		if(!isset($path_parts['extension']))
			return false;
		if($path_parts['extension'] == 'gif')
			return false;
		if($path_parts['extension'] == 'svg')
			return false;
		
		if(empty($path_parts['basename']))
			throw new Exception('imageOriginal basename error');

		$imageNew = $path_parts['filename'].'.webp';
		$folder = $prefix ? self::WEBP_IMAGES_PATH.$prefix.$imageNew : self::WEBP_IMAGES_PATH.$imageNew;

		if(file_exists($folder)) return true;

		$success = WebPConvert::convert($imageOriginal, $folder, [
			'quality' => 'auto',
			'max-quality' => 100,
			//'converters' => ['imagick']
		]);

		//CVarDumper::dump($success, 10, true);
		//die;
		return $success;

	}
}