<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 22.05.2019
 * Time: 16:09
 */
use sokolnikov911\YandexTurboPages\{Channel, Feed, Item, Counter};

class YandexFeed
{

	public static function articles()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'article_active = 1';
		$criteria->order = 'article_date DESC';
		$oArticles = CmsModuleArticles::model()->findAll($criteria);
		$feed = new Feed();
		$channel = new Channel();$channel
			->title('Статьи bar-street.ru')
			->link('https://bar-street.ru/article')
			->description('Читайте на bar-street.ru')
			->language('ru')
			->adNetwork(Channel::AD_TYPE_YANDEX, 'RA-123456-7', 'first_ad_place')
			->appendTo($feed);
		$yandexCounter = new Counter(Counter::TYPE_YANDEX, 10660447);
		$yandexCounter->appendTo($channel);
		$googleCounter = new Counter(Counter::TYPE_GOOGLE_ANALYTICS, 'UA-140656445-1');
		$googleCounter->appendTo($channel);
		foreach ($oArticles as $article) {
			$item = new Item();
			$item
				->title($article->article_name)
				->link('https://bar-street.ru'.$article->getUrl())
				->turboContent(Helpers::templateBuffer('articles/article', ['article' => $article]))
				->appendTo($channel);
		}
		echo $feed;
	}

	public static function servicesx()
	{
		$oService = CmsModuleService::model();
		$feed = new Feed();
		$channel = new Channel();
		$channel
			->title('Услуги bar-street.ru')
			->link('https://bar-street.ru/services')
			->description('Услуги на bar-street.ru')
			->language('ru')
			->adNetwork(Channel::AD_TYPE_YANDEX, 'RA-123456-7', 'first_ad_place')
			->appendTo($feed);
		$yandexCounter = new Counter(Counter::TYPE_YANDEX, 10660447);
		$yandexCounter->appendTo($channel);
		$googleCounter = new Counter(Counter::TYPE_GOOGLE_ANALYTICS, 'UA-140656445-1');
		$googleCounter->appendTo($channel);
		foreach ($oService->getServices() as $type => $services) {
			foreach ($services as $service) {
				$item = new Item();
				$item
					->title($service->service_name)
					->link('https://bar-street.ru/services/' . $service->service_slug)
					->turboContent(Helpers::templateBuffer('services/service', ['service' => $service]))
					->appendTo($channel);
			}
		}
		echo $feed;
	}

}