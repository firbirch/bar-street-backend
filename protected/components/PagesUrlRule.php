<?php

class PagesUrlRule extends CBaseUrlRule
{
	public $connectionID;

	public function createUrl($manager, $route, $params, $ampersand) {
		return false;
	}

	public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {

		if(empty($rawPathInfo)) {
			$_GET['pageId'] = 1;
		}
		$parts = explode('/', $rawPathInfo);
		$slug = trim(array_pop($parts));
		$pages = CmsPage::model()->findAll('slug=:slug', [':slug' => $slug]);

		foreach ($pages as $page) {
			if(!empty($page->controller) && !empty($page->action)) {
				return $page->controller.'/'.$page->action;
			}
			if($this->getParentUrl($page) === '/' . $rawPathInfo) {
				$_GET['pageId'] = $page->id;
				return 'page/view';
			}
		}
		return false;
	}
	private function getParentUrl(CmsPage $page)
	{
		$menu = CmsNestedPage::model()->findByAttributes(['cms_page_id' => $page->id]);

		if(!$menu)
			return '/'.$page->slug;

		$ancestors = $menu->ancestors()->findAll();
		$url = [];
		foreach ($ancestors as $parentPage) {
			if($parentPage->cmsPage->slug === '/') continue;
			$url[] = $parentPage->cmsPage->slug;
		}
		$url[] = $page->slug;
		return '/' . implode('/', $url);
	}
}