<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 13:20
 */

class LinkPager extends CLinkPager
{
	const CSS_FIRST_PAGE = 'first';
	const CSS_LAST_PAGE = 'last';
	const CSS_INTERNAL_PAGE = 'pagination__item';
	const CSS_HIDDEN_PAGE = 'hidden';
	const CSS_SELECTED_PAGE = '_disabled _active';


	public $previousPageCssClass = self::CSS_PREVIOUS_PAGE;
	public $nextPageCssClass = self::CSS_NEXT_PAGE;
	public $internalPageCssClass = self::CSS_INTERNAL_PAGE;
	public $hiddenPageCssClass = self::CSS_HIDDEN_PAGE;
	public $selectedPageCssClass = self::CSS_SELECTED_PAGE;
	public $maxButtonCount = 5;
	public $htmlOptions = [];


	public function init()
	{
		$this->htmlOptions = 'pagination__list';
	}


	public function run()
	{
		echo sprintf('<ul class = "%s">%s%s%s</ul>',
			$this->htmlOptions,
			$this->createPrevButton(),
			implode(PHP_EOL, $this->createPageButtons()),
			$this->createNextButton()
		);
	}

	protected function createPrevButton()
	{
		if($this->getPageCount() <= 1)
			return false;

		$currentPage = $this->getCurrentPage(false);

		if (($page = $currentPage - 1) < 0)
			$page = 0;


		return '<li class = "pagination__item">
				<a href = "' . $this->createPageUrl($page) . '" class="pagination__toggle">
					<span class="pagination__arrow _left">
						<svg class="icon-svg">
							<use xlink:href="#icon-nav" />
						</svg>
					</span>
				</a>
			</li>';
	}

	protected function createNextButton()
	{
		if($this->getPageCount() <= 1)
			return false;

		$pageCount = $this->getPageCount();
		$currentPage = $this->getCurrentPage(false);

		if (($page = $currentPage + 1) >= $pageCount - 1)
			$page = $pageCount - 1;


		return '<li class = "pagination__item">
				<a href = "' . $this->createPageUrl($page) . '" class="pagination__arrow _right">
					<svg class="icon-svg">
                        <use xlink:href="#icon-nav" />
                    </svg>
				</a>
			</li>';
	}

	protected function createPageButtons()
	{
		if (($pageCount = $this->getPageCount()) <= 1)
			return array();


		list($beginPage, $endPage) = $this->getPageRange();
		$currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons = array();

		// first page
		if ($currentPage >= 5) {
			$buttons[] = $this->createPageButton(1, 0, self::CSS_INTERNAL_PAGE, $currentPage <= 0, false);
			if ($currentPage > 5) {
				$buttons[] = $this->createSpacer();
			}
		}

		// internal pages
		for ($i = $beginPage; $i <= $endPage; ++$i) {
			$buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);
		}

		// last page
		if (($pageCount - $currentPage - 1) >= 5) {
			if (($pageCount - $currentPage - 1) > 5) {
				$buttons[] = $this->createSpacer();
			}
			$buttons[] = $this->createPageButton($pageCount, $pageCount - 1, self::CSS_INTERNAL_PAGE, $currentPage >= $pageCount - 1, false);
		}


		return $buttons;
	}

	protected function createSpacer($label = '...')
	{
		return '<li class="pagination__item _separate">
					' . $label . '
				</li>';
	}

	protected function createPageButton($label, $page, $class, $hidden, $selected)
	{
		if ($hidden || $selected)
			$class .= ' ' . ($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);

		return '<li class="' . $class . '">
			<a href="' . $this->createPageUrl($page) . '" class="pagination__toggle ' . ($selected ? '_disabled _active' : '') . '">
				' . $label . '
			</a>
		</li>';
	}

	protected function getPageRange()
	{
		$currentPage = $this->getCurrentPage();
		$pageCount = $this->getPageCount();

		$beginPage = max(0, $currentPage - (int)($this->maxButtonCount / 2));
		if (($endPage = $beginPage + $this->maxButtonCount - 1) >= $pageCount) {
			$endPage = $pageCount - 1;
			$beginPage = max(0, $endPage - $this->maxButtonCount + 1);
		}
		return array($beginPage, $endPage);
	}

}