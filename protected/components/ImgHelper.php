<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 30.04.2019
 * Time: 12:57
 */

class ImgHelper
{
	public static function img($url, $classes = "", $href = false)
	{
		$webp = file_exists(Yii::app()->basePath.'/../'.$url.'.webp') ? $url.'.webp' : false;
		$hrefurl = $href ? "href='$url'" : '';
		echo "<picture>
			<source class=\"lazyload\" data-srcset=\"$webp\" type=\"image/webp\">
			<img $hrefurl class=\"lazyload $classes\" data-src=\"$url\">
		</picture>";
	}
}