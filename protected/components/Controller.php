<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/base.twig';

	private $theme = 'barstreet';

	public $currentUri;

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	public $main_menu = [];
	public $settings = [];
	public $service_category = [];
	public $og_description = null;
	public $file_css = null;


	public function init() {
		$oMenu = CmsNestedPage::model()->roots()->findAll();
		$this->main_menu = current($oMenu)->children()->findAll();
		$oSettings = CmsSiteSettings::model()->findByAttributes(['field_name' => 'theme']);
		if(!$oSettings)
			throw new Exception('set theme in settings');

		if(isset($oSettings->field_value))
			$this->theme = $oSettings->field_value;

		Helpers::utm_source();

		$this->settings = CmsSiteSettings::model()->getSettings();
		$this->service_category = CmsModuleServiceCategory::model()->getServiceCategory();

		$this->currentUri = $_SERVER['REQUEST_URI'];

		Yii::app()->theme = $this->theme;

		$this->file_css = filemtime(DOC_ROOT.Yii::app()->theme->baseUrl.'/css/main.min.css');
	}





	public function SEOHeader_Meta($seo_title, $seo_description) {
		if($seo_title)
			$this->pageTitle = $seo_title;
		if($seo_description)
			Yii::app()->clientScript->registerMetaTag($seo_description, 'description');
	}

	public function format_price($value)
	{
		if ($value > 0) {
			$value = number_format($value, 2, ',', ' ');
			$value = str_replace(',00', '', $value);
		}
		return $value;
	}


}