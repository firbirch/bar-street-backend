<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 08/02/2019
 * Time: 13:08
 */

class Helpers
{
	public static function format_price($value)
	{
		if ($value > 0) {
			$value = number_format($value, 2, ',', ' ');
			$value = str_replace(',00', '', $value);
		}
		return $value;
	}

	public function date_rus($currentDate)
	{

		$time = strtotime($currentDate);
		$month_name = array(
			1 => 'января',
			2 => 'февраля',
			3 => 'марта',
			4 => 'апреля',
			5 => 'мая',
			6 => 'июня',
			7 => 'июля',
			8 => 'августа',
			9 => 'сентября',
			10 => 'октября',
			11 => 'ноября',
			12 => 'декабря'
		);
		$currentDate = sprintf('%s %s %s',
			date('j', $time),
			$month_name[date('n', $time)],
			date('Y', $time)
		);
		return $currentDate;
	}


	public static function utm_source()
	{ // UTM Source
		$request = Yii::app()->request;
		if (!empty($request->getQuery('utm_source'))) {
			$another_utm = ['utm_medium', 'utm_campaign', 'utm_content', 'utm_term'];
			if (!isset(Yii::app()->request->cookies['utm_source']) || Yii::app()->request->cookies['utm_source']->value != $request->getQuery('utm_source')) {

				$cookie = new CHttpCookie('utm_source', $request->getQuery('utm_source'));
				$cookie->expire = time() + (30 * 24 * 60 * 60);
				Yii::app()->request->cookies['utm_source'] = $cookie;

				foreach ($another_utm as $key => $value) {
					$get = $request->getQuery($another_utm[$key]);
					if (isset($get) && !empty($get)) {
						$cookie = new CHttpCookie($value, $get);
						$cookie->expire = time() + (30 * 24 * 60 * 60);
						Yii::app()->request->cookies[$value] = $cookie;
					}
				}
			}
		}
	}

	public function slugify($string, $allow_slashes = false, $allow_dots = false)
	{
		$slash = "";
		$dots = "\.";
		$reverse = "";
		if ($allow_slashes) $slash = "\/";
		if ($allow_dots) {
			$dots = "";
			$reverse = "\.";
		}

		$cyr = array(
			"Щ", "Ш", "Ч", "Ц", "Ю", "Я", "Ж", "А", "Б", "В", "Г", "Д", "Е", "Ё", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ь", "Ы", "Ъ", "Э", "Є", "Ї",
			"щ", "ш", "ч", "ц", "ю", "я", "ж", "а", "б", "в", "г", "д", "е", "ё", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ь", "ы", "ъ", "э", "є", "ї");
		$lat = array(
			"Shh", "Sh", "Ch", "C", "Ju", "Ja", "Zh", "A", "B", "V", "G", "D", "E", "Jo", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "'", "Y", "`", "E", "Je", "Ji",
			"shh", "sh", "ch", "c", "ju", "ja", "zh", "a", "b", "v", "g", "d", "e", "jo", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "'", "y", "`", "e", "je", "ji"
		);


		$string = preg_replace("/[_\s" . $dots . ",?!\[\](){}]+/", "_", $string);
		$string = preg_replace("/-{2,}/", "--", $string);
		$string = preg_replace("/_-+_/", "--", $string);
		$string = preg_replace("/[_\-]+$/", "", $string);

		if (function_exists('mb_strtolower')) {
			$string = mb_strtolower($string);
		} else {
			$string = strtolower($string);
		}

		$string = preg_replace("/(ь|ъ)/", "", $string);
		$string = str_replace($cyr, $lat, $string);
		$string = preg_replace("/[^" . $slash . $reverse . "0-9a-z_\-]+/", "", $string);

		return $string;
	}

	public function cropString($text, $max_lengh = 20) {
		if(mb_strlen($text, "UTF-8") > $max_lengh) {
			$text_cut = mb_substr($text, 0, $max_lengh, "UTF-8");
			$text_explode = explode(" ", $text_cut);

			unset($text_explode[count($text_explode) - 1]);

			$text_implode = implode(" ", $text_explode);

			return $text_implode."...";
		} else {
			return $text;
		}
	}

	public function print_rss($query, $headerTitle = 'Статьи', $headerDescription = 'BarStreet Статьи') {
		if(!$query)
			throw new Exception('model not found');

		header('Content-Type: application/xml');

		foreach( Yii::app()->log->routes as $route ){
			if( $route instanceof CWebLogRoute ){
				$route->enabled = false;
			}
		}

		$items = null;

		foreach($query as $key => $row) {

			if(!empty($row->article_rss))
				$content = strip_tags(html_entity_decode($row->article_rss));
			else {
				$content = strip_tags(html_entity_decode($row->article_seo_description));
				$content = $this->cropString($content, 400);
			}

			$item = '<item>';
			$item .= '<title>'.$row->article_name.'</title>'.PHP_EOL;
			$item .= '<link>'.Yii::app()->getBaseUrl(true).$row->getUrl().'</link>';
			$item .= '<description>'.$content.'</description>';
			$item .= '<pubDate>'.$row->article_date.'</pubDate>';

			if($row->getImage()) {
				$item .= '<enclosure url="' . Yii::app()->getBaseUrl(true) . $row->getImage() . '" length="110361" type="image/jpeg"/>';
			}

			$item .= '</item>';
			$items .= $item;
		}
		echo '
		  <rss version="2.0">
		   <channel>
			 <title>'.$headerTitle.'</title> 
			 <link>'.Yii::app()->getBaseUrl(true).'</link> 
			 <description>'.$headerDescription.'</description> 
			 <lastBuildDate>'.date('Y-m-d H:i:s').'</lastBuildDate> 	
			 	'.$items.'
		   </channel>
		  </rss>';

	}

	public static function templateBuffer(string $template, array $data) : string
	{
		ob_start();
		Yii::app()->controller->renderPartial($template, $data);
		$content = ob_get_contents();
		ob_clean();
		return $content;
	}

}