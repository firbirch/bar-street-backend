<tr style="border-collapse:collapse;">
    <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:40px;"><p
                style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#E50066;">
            <strong>Информация о заказе:</strong></p></td>
</tr>
<tr style="border-collapse:collapse;">
    <td align="left"
        style="padding:0;Margin:0;padding-left:20px;padding-bottom:40px;padding-right:40px;">

		<?php foreach (Yii::app()->shoppingCart->getPositions() as $key => $position): ?>

        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
			<?=$position->product_name?> (<?=$position->getQuantity()?> шт.) — <?=Helpers::format_price($position->getSumPrice())?> руб.
        </p>

		<?php endforeach; ?>

        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            <strong>Итого: <?=$total?> руб.</strong>
        </p>
    </td>
</tr>
<tr style="border-collapse:collapse;">
    <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:40px;"><p
                style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#E50066;">
            <strong>Ваши данные:</strong></p></td>
</tr>
<tr style="border-collapse:collapse;">
    <td align="left"
        style="padding:0;Margin:0;padding-bottom:200px;padding-left:20px;padding-right:40px;">
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            Имя: <?=$name?></p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            Телефон: <?=$phone?></p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            Почта: <?=$mail?></p></td>
</tr>