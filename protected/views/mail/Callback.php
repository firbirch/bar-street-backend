<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 04/03/2019
 * Time: 11:31
 */
?>

<tr style="border-collapse:collapse;">
    <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:40px;"><p
                style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#E50066;">
            <strong>Ваши данные:</strong></p></td>
</tr>
<tr style="border-collapse:collapse;">
    <td align="left"
        style="padding:0;Margin:0;padding-bottom:200px;padding-left:20px;padding-right:40px;">
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            Имя: <?= $name ?></p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            Телефон: <?= $phone ?></p>
		<?php if ($mail != '-'): ?>
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
                Почта: <?= $mail ?></p>
		<?php endif; ?>
		<?php if($message != '-'): ?>
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
                Сообщение: <?= $message ?></p>
		<?php endif; ?>
	    <?php if ($category != '-'): ?>
		    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
			    Категория: <?= $category ?></p>
	    <?php endif; ?>
	    <?php if ($bar != '-'): ?>
		    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
			    Бар: <?= $bar ?></p>
	    <?php endif; ?>
	    <?php if ($guests != '-'): ?>
		    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
			    Гости: <?= $guests ?></p>
	    <?php endif; ?>
	    <?php if ($hours != '-'): ?>
		    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
			    Часы: <?= $hours ?></p>
	    <?php endif; ?>
    </td>
</tr>