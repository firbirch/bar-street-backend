<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 04/03/2019
 * Time: 11:31
 */
?>

<tr style="border-collapse:collapse;">
    <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:40px;"><p
                style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#E50066;">
            <strong>Ваши данные:</strong></p></td>
</tr>
<tr style="border-collapse:collapse;">
    <td align="left"
        style="padding:0;Margin:0;padding-bottom:200px;padding-left:20px;padding-right:40px;">
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            ФИО: <?= $fio ?></p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
            Телефон: <?= $phone ?></p>
		<?php if ($mail != '-'): ?>
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">
                Почта: <?= $mail ?></p>
		<?php endif; ?>
          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">Количество мебели: <?= $order_mebel_value ?> шт.</p>
          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">Сумма аренды мебели: <?= $order_mebel_price ?>  ₽</p>
          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">Доставка: <?= $order_mebel_delivery ?>  ₽</p>
          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">Обслуживание: <?= $order_mebel_service ?>  ₽</p>
          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:19px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:29px;color:#FFFFFF;">Общая сумма заказа: <?= $sum ?>  ₽</p>

    </td>
</tr>