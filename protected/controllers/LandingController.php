<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 17.10.2019
 * Time: 14:12
 */

class LandingController extends Controller
{
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$this->render('index', [
			'galleries' => CmsModuleGallery::model()->findAll([
				'condition' => 'g_active_eng = 1',
				'order' => 'date DESC',
				'limit' => 3
			]),
			'service_another' => CmsModuleService::model()->getServicesAnotherEn(),
			'categories' => CmsModuleServiceCategory::model()->findAll(),
			'subcategories' => CmsModuleSubcategory::model()->findAll(),
			'bars' => CmsModuleService::model()->findAll([
				'condition' => 'service_active_en = 1',
				'order' => 'service_sort',
				'limit' => 20,
			])
		]);
	}
}