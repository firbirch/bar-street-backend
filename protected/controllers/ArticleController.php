<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 13:33
 */

class ArticleController extends Controller
{

	public function actionIndex() {

		$criteria = new CDbCriteria();
		$criteria->condition = 'article_active = 1';
		$criteria->order = 'article_date DESC';

		$fieldCount = CmsModuleArticles::model()->count($criteria);
		$pages = new CPagination($fieldCount);
		$pages->pageSize = 6;
		$pages->applyLimit($criteria);


		$currentPage = $pages->currentPage + 1;

		if($currentPage != $pages->pageCount)
			Yii::app()->clientScript->registerLinkTag('next', NULL, Yii::app()->request->hostInfo . '/article/page'. ($currentPage + 1));
		if($currentPage > 2)
			Yii::app()->clientScript->registerLinkTag('prev', NULL, Yii::app()->request->hostInfo . '/article/page'. ($currentPage - 1));
		else if($currentPage != 1)
			Yii::app()->clientScript->registerLinkTag('prev', NULL, Yii::app()->request->hostInfo . '/article');


		$this->SEOHeader_Meta(
			'Статьи Bar-Street' . (($currentPage > 1) ? ' - страница №'.$currentPage : ''),
			'Познавательные статьи о работе в сфере ресторанного обслуживания и отдыха. Здесь вы найдете много советов о том, как лучше организовать свое мероприятие и при этом не тратить кучу денег' . (($currentPage > 1) ? ' - страница №'.$currentPage : '')
		);

		$this->og_description = 'Познавательные статьи о работе в сфере ресторанного обслуживания и отдыха. Здесь вы найдете много советов о том, как лучше организовать свое мероприятие и при этом не тратить кучу денег' . (($currentPage > 1) ? ' - страница №'.$currentPage : '');


		$oArticles = CmsModuleArticles::model()->findAll($criteria);
		$this->render('index', [
			'articles' => $oArticles,
			'pages' => $pages
		]);
	}

	public function actionArticles()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'article_active = 1';
		$criteria->order = 'article_date DESC';

		$fieldCount = CmsModuleArticles::model()->count($criteria);
		$pages = new CPagination($fieldCount);
		$pages->pageSize = 6;
		$pages->applyLimit($criteria);

		//когда записи закончатся вернуть футер
		if(($pages->currentPage+1) != Yii::app()->request->getParam('page')){
			$this->render('/common/new-footer');die;
		}

		$oArticles = CmsModuleArticles::model()->findAll($criteria);
		$this->render('blocks/_articles', [
			'articles' => $oArticles,
		]);
	}

	public function actionShow($show) {
		if(!$show)
			throw new Exception('request invalid');
		$oArticle = CmsModuleArticles::model()->findByAttributes(['article_slug' => $show]);
		if(!$oArticle)
			throw new CHttpException(404, 'This post cannot be found');

		$this->og_description = $oArticle->article_seo_description;

		$this->SEOHeader_Meta($oArticle->article_seo_title ?: $oArticle->article_name, $oArticle->article_seo_description);

		$this->render('show/index', [
			'article' => $oArticle,
			'article_next' => $oArticle->getNext(),
			'article_prev' => $oArticle->getPrev()
		]);

	}
}