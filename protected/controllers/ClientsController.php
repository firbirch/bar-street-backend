<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 24/03/2019
 * Time: 12:35
 */

class ClientsController extends Controller
{

	public function actionIndex() {

		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'clients', 'action' => 'index']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO
		$this->og_description = $oPage->seo_description;


		$oClients = CmsModuleClients::model()->findAll(['condition' => 'client_active = 1', 'order' => 'client_sort DESC']);
		$this->render('index', [
			'clients' => $oClients
		]);
	}

}