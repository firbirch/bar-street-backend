<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12/02/2019
 * Time: 10:57
 */


class CallbackController extends Controller
{


	public function actionIndex()
	{
		$oRequest = new CmsModuleForm;
		if (Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('CallbackForm');
			$oRequest->setAttributes($data);
			$captcha_error = [];
			if($oRequest->validate()) {
				$captcha = new GoogleRecaptcha(Yii::app()->request->getPost('g-recaptcha-response'));
				if($captcha->getResult()) {
					$oRequest->save(false);
					Yii::app()->user->setFlash('message_header', $oRequest::MESSAGE_HEADER[0]);
					Yii::app()->user->setFlash('message', $oRequest::MESSAGE[0]);
					if(Yii::app()->request->isAjaxRequest) {
						if(Yii::app()->request->getParam('popup')) {
							echo 'redirect'; die;
						}
						echo 'redirect';die;
					}
					else $this->redirect('/message');
				} else {
					$captcha_error['captcha'] = 'error';
				}
			}
			$error = array_merge($oRequest->getErrors(), $captcha_error);
			echo json_encode(['errors' => $error]);
		}
	}

	public function actionSubscribe() {

		$oSubscribe = new CmsModuleSubscribes();
		if(Yii::app()->request->isAjaxRequest) {

			$oSubscribe->addEmail(Yii::app()->request->getPost('email'));
			if($oSubscribe->validate()) {
				$oSubscribe->save(false);

				Yii::app()->user->setFlash('message_header', 'Спасибо за оформление подписки');
				Yii::app()->user->setFlash('message', '');
				echo 'ok';
			} else {
				echo 'error';
			}
		}

	}

	public function actionRentmebel() {

		$oOrderMebel = new CmsModuleOrderMebel();
		if(Yii::app()->request->isPostRequest) {
			$oOrderMebel->setAttributes(Yii::app()->request->getPost('CallbackForm'));
			if($oOrderMebel->validate()) {
				$oOrderMebel->save(false);
				Yii::app()->user->setFlash('message_header', 'Ваша заявка принята');
				Yii::app()->user->setFlash('message', 'Наш менеджер свяжется с вами в ближайшее время для уточнения деталей заказа и профессионального консультирования.');
				echo "ok";
			}
			else {
				echo json_encode(['status' => 'fail', 'errors' => $oOrderMebel->getErrors()]);
			}
		}
	}

	public function actionOrderProduct() {

		$oOrderMebel = new CmsModuleProductOrder();
		if(Yii::app()->request->isPostRequest) {
			$oOrderMebel->setAttributes(Yii::app()->request->getPost('CallbackForm'));
			if($oOrderMebel->validate()) {
				$oOrderMebel->save(false);
				Yii::app()->user->setFlash('message_header', 'Ваша заявка принята');
				Yii::app()->user->setFlash('message', 'Наш менеджер свяжется с вами в ближайшее время для уточнения деталей заказа и профессионального консультирования.');
				echo "ok";
			}
			else {
				echo json_encode(['status' => 'fail', 'errors' => $oOrderMebel->getErrors()]);
			}
		}
	}
}