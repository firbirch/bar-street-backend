<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 22/01/2019
 * Time: 09:25
 */

class ServicesController extends Controller
{

	public function actionIndex() {

		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'services', 'action' => 'index']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO

		$this->og_description = $oPage->seo_description;

		$oService = CmsModuleService::model();

		$this->render('index', [
			'services' => $oService->getServices('position_services'),
			'service_another' => $oService->getServicesAnother()
		]);
	}


	public function actionSaveRequest() {
		if(Yii::app()->request->isAjaxRequest) {

			$oRequest = CmsModuleForm::model();
			$data = Yii::app()->request->getPost('CmsModuleForm');

			$oRequest->setAttributes($data);
			if($oRequest->validate())
				$oRequest->save(false);
			else
				print_r($oRequest->getErrors());

		}
	}


	public function actionService($service) {

		if(empty($service))
			throw new Exception('request invalid (service)');

		$oService = CmsModuleService::model();
		$service = $oService->findByAttributes(['service_slug' => $service], 'service_active = 1');

		if(!$service)
			throw new CHttpException(404, 'Service not found');

		if(!$service->getController())
			throw new CHttpException(404, 'Controller not found');

		$this->SEOHeader_Meta($service->service_seo_title, $service->service_seo_description);

		$this->og_description = $service->service_seo_description;


//		CVarDumper::dump($service->getController(), 10, true);
//		die;

		$this->render($service->getController(), [
			'staffs' => $service->getServiceStaff(),
			'services' => $oService->getServiceWithBar('position_service'),
			'service' => $service,
			'advantages' => CmsModuleAdvantage::model()->getAdvantages($service),
			'coctails' => $service->getProducts(),
			'reviews' => $service->cmsModuleReviewsWithActive,
			'requestForm' => CmsModuleForm::model(),
			'clients' => CmsModuleClients::model()->findAll('client_active = 1')
		]);


	}

}