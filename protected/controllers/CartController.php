<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12/02/2019
 * Time: 16:51
 */

class CartController extends Controller
{


	public function actionIndex() {

		if(Yii::app()->shoppingCart->isEmpty())
			$this->redirect('/coctails');


		//Коктейльная карта Бара от BAR-STREET - корзина
		//Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль.

		$this->SEOHeader_Meta(
			'Коктейльная карта Бара от BAR-STREET - корзина',
			'Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль.'
		);
		$this->og_description = 'Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль.';


		$this->render('index');
	}


}