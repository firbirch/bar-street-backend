<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/03/2019
 * Time: 20:27
 */

class ExpoController extends Controller
{

	public function actionIndex() {

		$oService = CmsModuleService::model();



		$this->render('index',[
			'services' => $oService->getServiceExpo(),
			'events' => CmsModuleTeamEvent::model()->findAll('event_active = 1')
		]);
	}

}