<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 10:34
 */

class CoctailsController extends Controller
{

	public function actionIndex() {


		$oCoctails = CmsProductCoctails::model();

		$criteria = new CDbCriteria();
		$criteria->condition = 'product_active = 1';
		$criteria->order = 'product_sort ASC';

		$fieldCount = $oCoctails->count($criteria);
		$pages = new CPagination($fieldCount);
		$pages->pageSize = $oCoctails::PAGE_ELEMENTS;
		$pages->applyLimit($criteria);


		$currentPage = $pages->currentPage + 1;
		if($currentPage != $pages->pageCount)
			Yii::app()->clientScript->registerLinkTag('next', NULL, Yii::app()->request->hostInfo . '/coctails/page'. ($currentPage + 1));
		if($currentPage > 2)
			Yii::app()->clientScript->registerLinkTag('prev', NULL, Yii::app()->request->hostInfo . '/coctails/page'. ($currentPage - 1));
		else if($currentPage != 1)
			Yii::app()->clientScript->registerLinkTag('prev', NULL, Yii::app()->request->hostInfo . '/coctails');



		if($pages->currentPage + 1 > 1)
			$this->SEOHeader_Meta('Коктейльная карта Бара от BAR-STREET',
				'Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль');
		else
			$this->SEOHeader_Meta('Коктейльная карта Бара от BAR-STREET', 'Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль.');


		$this->og_description = 'Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль.';


		$this->render('index', [
			'services' => CmsModuleService::model()->getServicesWithBarAndProducts(),
			'main_page' => true,
			'ings' => CmsProductCoctailsIngridients::model()->findAll(),
			'products' => $oCoctails->findAll($criteria),
			'pages' => $pages
		]);
	}


	public function actionTab() {
		if(Yii::app()->request->isAjaxRequest) {
			$service = CmsModuleService::model()->findByAttributes(['service_slug' => Yii::app()->request->getPost('service')]);
			if(!empty($service->service_id)) {
				//return $this->actionFilter($service->service_id);
				$product = CmsProductCoctails::model()->getProductDataFiltered($service->service_id, null);
				$this->render('products', [
					'products' => $product['products'],
					'service' => $product['service'],
					'pages' => $product['pages'],
				]);

			}
			else
				throw new Exception('service not found');
		}
	}

	public function actionFilter($service = null, $ingredients = null) {
		$oCoctails = CmsProductCoctails::model();

		$criteria = new CDbCriteria();
		$criteria->condition = 'product_active = 1';
		$criteria->order = 'product_sort ASC';

		$fieldCount = $oCoctails->count($criteria);
		$pages = new CPagination($fieldCount);
		$service = $service ?: Yii::app()->request->getPost('service');
		$ingridents = $ingredients ?: Yii::app()->request->getPost('ingredient');
		$product = CmsProductCoctails::model()->getProductDataFiltered($service, $ingridents);
		if($pages->currentPage + 1 > 1)
			$this->SEOHeader_Meta('Коктейльная карта Бара от BAR-STREET',
				'Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль.');
		else
			$this->SEOHeader_Meta('Коктейльная карта Бара от BAR-STREET', 'Большой выбор коктейлей и других напитков в коктейльной карте. Цены указаны за 1 коктейль.');
		if(Yii::app()->request->isAjaxRequest) {
			$this->render('products', [
				'products' => $product['products'],
				'service' => $product['service'],
				'pages' => $product['pages'],
			]);
		}
		else {
			$this->render('index', [
				'products' => $product['products'],
				'pages' => $product['pages'],
				'service' => $product['service'],
				'main_page' => !$service && !$ingredients ? true : false,
				'services' => CmsModuleService::model()->getServicesWithBarAndProducts(),
				'ings' => CmsProductCoctailsIngridients::model()->findAll(),
				'currentService' => $service ?: false,
				'currentIng' => $ingredients ?: false
			]);
		}
	}

	public function actionAdd() {
		if(Yii::app()->request->isAjaxRequest) {
			$productId = Yii::app()->request->getPost('product_id');

			$oProduct = CmsProductCoctails::model()->findByPk($productId);
			if(empty($oProduct))
				throw new Exception('not found');

			Yii::app()->shoppingCart->put($oProduct, 25);
		}
	}

	public function actionRemove() {
		if(Yii::app()->request->isAjaxRequest) {
			$productId = Yii::app()->request->getPost('product_id');
			$oProduct = CmsProductCoctails::model()->findByPk($productId);
			if(empty($oProduct))
				throw new Exception('not found');
			Yii::app()->shoppingCart->remove($oProduct->getId());
		}
	}

	public function actionIncrease() {
		if(Yii::app()->request->isAjaxRequest) {
			$productId = Yii::app()->request->getPost('product_id');
			$oProduct = CmsProductCoctails::model()->findByPk($productId);
			if(empty($oProduct))
				throw new Exception('not found');
			Yii::app()->shoppingCart->put($oProduct, 25);
		}
	}

	public function actionDecrease() {
		if(Yii::app()->request->isAjaxRequest) {
			$productId = Yii::app()->request->getPost('product_id');
			$oProduct = CmsProductCoctails::model()->findByPk($productId);
			if(empty($oProduct))
				throw new Exception('not found');
			Yii::app()->shoppingCart->put($oProduct, -25);
		}
	}
}