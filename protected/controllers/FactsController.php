<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/03/2019
 * Time: 18:17
 */

class FactsController extends Controller
{

	public function actionIndex() {

		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'facts', 'action' => 'index']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO

		$this->og_description = $oPage->seo_description;

		$this->render('index');
	}

}