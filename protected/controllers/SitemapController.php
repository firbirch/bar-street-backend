<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 08/04/2019
 * Time: 10:38
 */

class SitemapController extends Controller
{

	public function actionIndex() {

		$urls = []; $index = null;


		$oPage = CmsPage::model()->findAll('active = 1');
		foreach($oPage as $page) {
			$index++;
			if($page->slug == '/') {
				$urls[$index]['slug'] = '';
				$urls[$index]['priority'] = '1.0';
			}
			else {
				$urls[$index]['slug'] = '/'.$page->slug;
				$urls[$index]['priority'] = '0.3';
			}
		}

		$oArticle = CmsModuleArticles::model()->findAll('article_active = 1');
		foreach ($oArticle as $article) {
			$index++;
			$urls[$index]['slug'] = $article->getUrl();
			$urls[$index]['priority'] = '0.9';
		}

		$oService = CmsModuleService::model()->findAll('service_active = 1');
		foreach($oService as $service) {
			$index++;
			$urls[$index]['slug'] = $service->getServiceUrl();
			$urls[$index]['priority'] = '0.9';
		}

		$oGallery = CmsModuleGallery::model()->findAll('g_active = 1');
		foreach($oGallery as $gallery) {
			$index++;
			$urls[$index]['slug'] = $gallery->getGalleryUrl();
			$urls[$index]['priority'] = '0.6';
		}

		$host = Yii::app()->request->hostInfo;
		header('Content-Type: application/xml');
		foreach( Yii::app()->log->routes as $route ){
			if( $route instanceof CWebLogRoute ){
				$route->enabled = false;
			}
		}

		echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
		echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		foreach ($urls as $url){
			echo '<url>
                <loc>' . $host . $url['slug'] . '</loc>
                <changefreq>weekly</changefreq>
                <lastmod>'.date('Y-m-d').'</lastmod>
                <priority>'.$url['priority'].'</priority>
            </url>';
		}
		echo '</urlset>';
		Yii::app()->end();

	}

}