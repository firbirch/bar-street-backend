<?php
//
//Yii::import('application.components.Helpers');

class SiteController extends Controller
{

	public function actionIndex()
	{
		$oSlider = CmsModuleSlider::model()->findAll(['condition' => 'active = 1','order' => 'sort ASC']);
		$oEvents = CmsModuleEvents::model()->findAll(['condition' => 'event_active = 1', 'order' => 'event_date DESC']);
		$oService = CmsModuleService::model()->getServiceWithBar();
		$oClients = CmsModuleClients::model()->findAll(['condition' => 'client_active = 1', 'order' => 'client_sort DESC']);



		$this->SEOHeader_Meta(
			'Выездной мобильный бар - заказать бар на выезд в Москве и Санкт-Петербурге - BAR-STREET',
			'BAR-STREET предлагает услуги выездного бара на любое мероприятие. У нас Вы можете заказать мобильный бар на выезд.'
		);

		$this->og_description = 'BAR-STREET предлагает услуги выездного бара на любое мероприятие. У нас Вы можете заказать мобильный бар на выезд.';


		$this->render('index', [
			'sliders' => $oSlider,
			'events' => $oEvents,
			'services' => $oService,
			'clients' => $oClients
		]);
	}
	public function actionError()
	{
		foreach( Yii::app()->log->routes as $route ){
			if( $route instanceof CWebLogRoute ){
			//	$route->enabled=false;
			}
		}

		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionRss() {

		$helpers = new Helpers();

		$helpers->print_rss(CmsModuleArticles::model()->findAll(['order' => 'article_date DESC', 'condition' => 'article_active = 1']));
		//$helpers->print_rss(CmsModuleArticles::model()->findAll(['order' => 'article_date', 'condition' => 'article_active = 1']));
		/*$this->render('rss', [
			'rss' => CmsModuleArticles::model()->findAll(['order' => 'article_date', 'condition' => 'article_active = 1'])
		]);*/
	}

	public function actionOctober()
	{
		$this->render('../single_page/october');
	}
}