<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 03/04/2019
 * Time: 21:35
 */

class WorklevelsController extends Controller
{

	public function actionIndex() {

		//throw new Exception(404, 'Pagenot found');
		//die;

		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'worklevels', 'action' => 'index']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO

		$this->og_description = $oPage->seo_description;

		$this->render('index');

	}

}