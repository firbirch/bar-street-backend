<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12/02/2019
 * Time: 10:38
 */

class EventController extends Controller
{
	public function actionIndex() {


		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'event', 'action' => 'index']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO

		$this->og_description = $oPage->seo_description;

		$this->render('index');

	}
}