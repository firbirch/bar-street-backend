<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 17:00
 */

class PhotoalbumController extends Controller
{

	public function actionIndex() {

		$criteria = new CDbCriteria();
		$criteria->condition = 'g_active = 1';
		$criteria->order = 'date DESC';

		$fieldCount = CmsModuleGallery::model()->count($criteria);
		$pages = new CPagination($fieldCount);
		$pages->pageSize = 12;
		$pages->applyLimit($criteria);


		$currentPage = $pages->currentPage + 1;
		if($currentPage != $pages->pageCount)
			Yii::app()->clientScript->registerLinkTag('next', NULL, Yii::app()->request->hostInfo . '/photoalbum/page'. ($currentPage + 1));
		if($currentPage > 2)
			Yii::app()->clientScript->registerLinkTag('prev', NULL, Yii::app()->request->hostInfo . '/photoalbum/page'. ($currentPage - 1));
		else if($currentPage != 1)
			Yii::app()->clientScript->registerLinkTag('prev', NULL, Yii::app()->request->hostInfo . '/photoalbum');

		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'photoalbum', 'action' => 'index']);
		$this->SEOHeader_Meta(
			$oPage->seo_title . (($currentPage > 1) ? ' - страница №'.$currentPage : ''),
			$oPage->seo_description . (($currentPage > 1) ? ' - страница №'.$currentPage : '')
		);
		//END SEO

		$this->og_description = $oPage->seo_description;

		$this->render('index', [
			'galleries' => CmsModuleGallery::model()->findAll($criteria),
			'pages' => $pages
		]);
	}

	public function actionPhotoReports()
	{

		$criteria = new CDbCriteria();
		$criteria->condition = 'g_active = 1';
		$criteria->order = 'date DESC';

		$fieldCount = CmsModuleGallery::model()->count($criteria);
		$pages = new CPagination($fieldCount);
		$pages->pageSize = 12;
		$pages->applyLimit($criteria);

		//когда записи закончатся вернуть футер
		if(($pages->currentPage+1) != Yii::app()->request->getParam('page')){
			$this->render('/common/new-footer');die;
		}
		$this->render('blocks/_galleries', [
			'galleries' => CmsModuleGallery::model()->findAll($criteria),
		]);
	}

	public function actionShow($show = '') {

		$oGallery = CmsModuleGallery::model()->findByAttributes(['date' => $show]);
		if(!$oGallery)
			throw new CHttpException(404, 'Photoalbum not found');

		$desc = null;

		if($oGallery->seo_description)
			$desc = $oGallery->seo_description;
		else
			$desc .= 'Фотоотчет: '.$oGallery->name;

		$this->og_description = $desc;

		$iPhone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");

		$this->SEOHeader_Meta($oGallery->seo_title ?: $oGallery->name, $desc);
		$this->render('show/index',[
			'gallery' => $oGallery,
			'iphone' => $iPhone
		]);
	}

	public function actionMap() {

		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'photoalbum', 'action' => 'map']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO

		$this->og_description = $oPage->seo_description;


		if(Yii::app()->request->isAjaxRequest) {
			if(Yii::app()->request->getPost('crutch') == 1) {
				return CmsModuleGallery::model()->getJsonMap();
			}
		}
		$this->render('map');
	}
	
}