<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12/02/2019
 * Time: 11:37
 */

class MessageController extends Controller
{

	public function actionIndex() {

		$this->render('index', [
			'message_header' => Yii::app()->user->getFlash('message_header'),
			'message' => Yii::app()->user->getFlash('message')
		]);
	}

}