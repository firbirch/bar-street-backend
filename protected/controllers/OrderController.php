<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12/02/2019
 * Time: 17:14
 */

class OrderController extends Controller
{
	public function actionIndex() {
		if(Yii::app()->shoppingCart->isEmpty())
			$this->redirect('/coctails');

		$this->render('index');
	}


	public function actionComplete() {

		if(Yii::app()->request->isPostRequest) {
			$oProductOrder = new CmsModuleProductOrder;
			$data = Yii::app()->request->getPost('ProductOrder');
			$oProductOrder->setAttributes($data);

			if($oProductOrder->validate()) {
				$oProductOrder->save(false);
				Yii::app()->user->setFlash('message_header', 'Ваша заявка принята');
				Yii::app()->user->setFlash('message', 'Наш менеджер свяжется с вами в ближайшее время для уточнения деталей заказа и профессионального консультирования.');
				$this->redirect('/message');
			}
			else {
				$this->redirect('/order/index');
			}

		}
	}

	public function actionProduct() {
		if(Yii::app()->request->isAjaxRequest) {

			Yii::app()->shoppingCart->clear();

			$data = Yii::app()->request->getPost('products');
			$data = json_decode($data);
			$oProduct = CmsProductCoctails::model();
			foreach($data as $key => $row) {
				$product = $oProduct->findByPk($key);
				Yii::app()->shoppingCart->put($product, (int)$row);
			}
			echo 'ok';
		} else {
			$this->redirect('/coctails');
		}
	}
}