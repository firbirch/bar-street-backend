<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 09/01/2019
 * Time: 17:58
 */

class PageController extends Controller
{
	public function actionView($pageId)
	{
		$page = CmsPage::model()->findByPk($pageId);

		$this->SEOHeader_Meta($page->seo_title, $page->seo_description);
		$this->og_description = $page->seo_description;

		$this->render('index', ['page' => $page]);

	}

}