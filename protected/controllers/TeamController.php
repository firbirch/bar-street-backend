<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 08/02/2019
 * Time: 11:49
 */

class TeamController extends Controller
{

	public function actionIndex() {


		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'team', 'action' => 'index']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO

		$this->og_description = $oPage->seo_description;

		$this->render('index', [
			'team' => CmsModuleEmployee::model()->getTeam(),
			'events' => CmsModuleTeamEvent::model()->findAll('event_active = 1')
		]);

	}

}