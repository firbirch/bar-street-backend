<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 11/02/2019
 * Time: 10:49
 */

class ContactsController extends Controller
{

	public function actionIndex() {


		//SEO
		$oPage = CmsPage::model()->findByAttributes(['controller' => 'contacts', 'action' => 'index']);
		$this->SEOHeader_Meta($oPage->seo_title, $oPage->seo_description);
		//END SEO

		$this->og_description = $oPage->seo_description;

		$this->render('index', [
			'settings' => CmsSiteSettings::model()->getSettings()
		]);

	}

}