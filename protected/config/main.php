<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'BarStreet',

	// preloading 'log' component
	'preload'=>array('log'),

	//'theme' => 'barstreet', // Current Theme

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.modules.admin.*',
		'application.components.*',
		'application.extensions.*',
		'application.vendor.assisrafael.giix.components.*',
		'ext.Mailer.YiiMailer',
		'ext.easyimage.EasyImage',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password'=>'dimka123',
			'generatorPaths' => array(
				'application.vendor.assisrafael.giix.generators', // giix generators
			),
			'ipFilters'=>array('10.0.2.2','::1'),
		),

		'admin' => array(
			'layout' => 'admin_main'
		)
	),


	// application components
	'components'=>array(

		'user'=>array(
			'class' => 'WebUser',
			'allowAutoLogin'=>true,
			'loginUrl' => '/admin',
		),

		'shoppingCart' => array(
			'class' => 'application.extensions.shoppingCart.EShoppingCart',
		),

		'viewRenderer' => array(
			'class' => 'application.extensions.Twig.ETwigViewRenderer',
			'fileExtension' => '.twig',
			'options' => array(
				'autoescape' => true,
				'debug' => true,
			),
			'globals' => array(
				'html' => 'CHtml',
				'app' => Yii::app(),
				'yii' => new Yii,
				'ImgHelper' => 'ImgHelper',
				'morphos' => application\extensions\MorphosAdapter::class,
				'dataHelper' => 'DataHelper',
			),
			'functions' => array(
				'rot13' => 'str_rot13',
				'filemtime' => 'filemtime',
				'date' => 'date'
			),
			'filters' => array(
				'jencode' => 'CJSON::encode',
				'dump' => 'var_dump'
			),

		),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'appendParams' => false,
			'rules' => require (dirname(__FILE__).'/urlrewrite.php')
		),


		// database settings are configured in database.php
		'db' => YII_DEBUG ? require(dirname(__FILE__).'/database.php') : require(dirname(__FILE__).'/database.production.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction' => YII_DEBUG ? null : 'site/error',
			//'errorAction' => null
		),

		'onBeginRequest'=>function($event){
			$route=Yii::app()->getRequest()->getPathInfo();
			$module=substr($route,0,strpos($route,'/'));

			if(Yii::app()->hasModule($module))
			{
				$module=Yii::app()->getModule($module);
				if(isset($module->urlRules))
				{
					$urlManager=Yii::app()->getUrlManager();
					$urlManager->addRules($module->urlRules);
				}
			}
			return true;
		},

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages

//				array(
//					'class'=>'CWebLogRoute',
//				),

			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
