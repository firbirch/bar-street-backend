<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/12/2018
 * Time: 14:25
 */

return array(

	'https://october.bar-street.ru' => 'site/october',
	'http://october.bar-street.ru' => 'site/october',
	'/october-offer' => 'site/october',
	'http://october.barstreet.local' => 'site/october',

	'/en' => 'landing/index',

	//---------------------------------------------------------------
	'<module:(admin)>' =>'admin',
	'admin/<_c:\w+>/<_a:\w+>/<id:\d+>' =>'admin/<_c>/<_a>/<id>',
	'admin/<_c:\w+>/<_a:\w+>'          =>'admin/<_c>/<_a>',
	'admin/<_c:\w+>'                   =>'admin/<_c>',
	//---------------------------------------------------------------

	['class' => 'application.components.PagesUrlRule'],
	'<controller:(page)>/view/<pageId:\d+>' => '<controller>/view',

	'work-levels' => 'worklevels/index',
	'sitemap.xml' => 'sitemap/index',
	'rss' => 'site/rss',

	'yandexrss/<mode>' => 'yandexFeed/index',

	'<controller:(services)>/<service:[A-Za-z0-9-]+>'  => '<controller>/service',
	'<controller:(article)>/articles' => '<controller>/articles',
	'<controller:(article)>/<show:[A-Za-z0-9-]+>' => '<controller>/show',



	//'<controller:(photoalbum)>/<show>' => '<controller>/show',

	'<controller:(photoalbum)>/<show:[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])>' => '<controller>/show',
	'<controller:(coctails)>/page<page>' => '<controller>/index',


	'<controller:\w+>' => '<controller>/index',
	'<controller:\w+>/<id:\d+>'=>'<controller>/view',
	'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
	'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
);