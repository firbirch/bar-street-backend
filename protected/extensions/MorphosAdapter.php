<?php
namespace application\extensions;
use morphos\Russian\NounPluralization;
use morphos\Russian\NounDeclension;

class MorphosAdapter
{
	public static function pluralize($count, $item)
	{
		if(!empty($count) && !empty($item))
			return \morphos\Russian\pluralize($count, $item);
	}

	public static function npluralize($word, $count, $case)
	{
		return NounPluralization::pluralize($word, $count = 2, $animateness = false, $case = null);
	}
}