<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 11.10.2018
 * Time: 11:40
 */


Yii::import('zii.widgets.CBreadcrumbs');
class ABreadCrumbs extends CBreadcrumbs
{
    public $tagName = 'nav';
    public $htmlOptions = ['id' => 'breadcrumbs', 'class' => 'breadcrumb bg-white'];
    public $encodeLabel = true;
    public $homeLink = '<li class="breadcrumb-item"><a href = "/admin/">Панель управления</a></li>';
    public $links = [];
    public $activeLinkTemplate = '<li class="breadcrumb-item"><a href = "{url}">{label}</a></li>';
    public $inactiveLinkTemplate = '<li class = "breadcrumb-item active"><a>{label}</a></li>';
    public $separator = '';
}