<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 11.10.2018
 * Time: 11:56
 */

class AController extends Controller
{
    public $layout = 'admin_main';
    public $views;

	public function setBreadCrumbs(array $items = []) {
		return $this->breadcrumbs = $items;
	}

    // Для вывода в Layouts
	public function init() {


		$oIblock = CmsIblock::model()->findAll();
		$this->views['iblock'] = $oIblock;
	}

	public function slugify($string, $allow_slashes = false, $allow_dots = false)
	{
		$slash = "";
		$dots = "\.";
		$reverse = "";
		if ($allow_slashes) $slash = "\/";
		if ($allow_dots) {
			$dots = "";
			$reverse = "\.";
		}

		$cyr = array(
			"Щ", "Ш", "Ч", "Ц", "Ю", "Я", "Ж", "А", "Б", "В", "Г", "Д", "Е", "Ё", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ь", "Ы", "Ъ", "Э", "Є", "Ї",
			"щ", "ш", "ч", "ц", "ю", "я", "ж", "а", "б", "в", "г", "д", "е", "ё", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ь", "ы", "ъ", "э", "є", "ї");
		$lat = array(
			"Shh", "Sh", "Ch", "C", "Ju", "Ja", "Zh", "A", "B", "V", "G", "D", "E", "Jo", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "'", "Y", "`", "E", "Je", "Ji",
			"shh", "sh", "ch", "c", "ju", "ja", "zh", "a", "b", "v", "g", "d", "e", "jo", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "'", "y", "`", "e", "je", "ji"
		);


		$string = preg_replace("/[_\s" . $dots . ",?!\[\](){}]+/", "_", $string);
		$string = preg_replace("/-{2,}/", "--", $string);
		$string = preg_replace("/_-+_/", "--", $string);
		$string = preg_replace("/[_\-]+$/", "", $string);

		if (function_exists('mb_strtolower')) {
			$string = mb_strtolower($string);
		} else {
			$string = strtolower($string);
		}

		$string = preg_replace("/(ь|ъ)/", "", $string);
		$string = str_replace($cyr, $lat, $string);
		$string = preg_replace("/[^" . $slash . $reverse . "0-9a-z_\-]+/", "", $string);

		return $string;
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['admin']],
			['deny', 'users'=> ['guest']]
		];
	}

}