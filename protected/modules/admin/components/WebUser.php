<?php

class WebUser extends CWebUser
{
    function getRole()
    {
        return $this->isGuest ? 'guest' : 'admin';
    }
}