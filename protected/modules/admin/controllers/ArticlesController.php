<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 16:31
 */

class ArticlesController extends AController
{

	public function actionIndex() {

		$this->render('index', [
			'articles' => CmsModuleArticles::model()->findAllByAttributes([],['order' => 'article_date desc'])
		]);
	}

	public function actionShow($article = 0) {

		$oArticle = CmsModuleArticles::model();

		if($article) {
			$article = $oArticle->findByPk($article);
		} else {
			$article = $oArticle;
		}

		$this->render('show/index', [
			'article' => $article
		]);
	}


	public function actionDelete() {
		if(Yii::app()->request->isAjaxRequest) {
			$data = Yii::app()->request->getPost('article');

			$oArticle = CmsModuleArticles::model()->findByPk($data);
			if($oArticle) {

				if ($oArticle->article_image) {
					Files::delete('main_' . $oArticle->article_image, $oArticle::IMAGES_DIR);
					Files::delete('thumb_' . $oArticle->article_image, $oArticle::IMAGES_DIR);
					Files::delete('icon_' . $oArticle->article_image, $oArticle::IMAGES_DIR);
					Files::delete($oArticle->article_image, $oArticle::IMAGES_DIR);
				}
				$oArticle->delete();

			}
			else
				throw new Exception('Cannot remove');

		}
	}

	private static function SaveImage(CmsModuleArticles $model) {
		if ($imageNameMain = Files::upload($model::IMAGES_DIR, 'MainImage')) {
			if ($model->article_image) {
				Files::delete('main_' . $model->article_image, $model::IMAGES_DIR);
				Files::delete($model->article_image, $model::IMAGES_DIR);
			}
			if (Files::copy($imageNameMain, 'main_' . $imageNameMain, $model::IMAGES_DIR)) {
				Files::imageResize('main_' . $imageNameMain, $model::IMAGES_DIR, 370, 450);
				Files::imageWebp('main_' . $imageNameMain, $model::IMAGES_DIR);
				Files::imageWebp($imageNameMain, $model::IMAGES_DIR);
			}
			$model->article_image = $imageNameMain;
		}
	}

	public function actionSaveAll($article = 0) {

		if(Yii::app()->request->isPostRequest) {
			$oArticle = new CmsModuleArticles;

			if ($article) {
				$article = $oArticle->findByPk($article);
			} else {
				$article = $oArticle;
			}

			$data = Yii::app()->request->getPost('CmsModuleArticles');
			$article->setAttributes($data);

			if($article->validate()) {
				self::SaveImage($article);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$article->save(false);
			}
			else
				print_r($article->getErrors());

			$this->render('show/index', [
				'article' => $article
			]);

		}
	}
}