<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 31/01/2019
 * Time: 14:40
 */

class RequestsController extends AController
{

	public function actionIndex() {
		$oRequests = CmsModuleForm::model()->getRequestList();
		$this->render('index', [
			'requests' => $oRequests
		]);
	}
	public function actionRemove($form_id) {

		$oRequests = new CmsModuleForm;
		$oRequests->deleteByPk($form_id);
		$this->redirect('/admin/requests');
	}

}