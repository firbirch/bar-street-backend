<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 13/12/2018
 * Time: 10:25
 */

class IblockController extends AController
{
	public function actionIndex()
	{
		$oIblock = CmsIblock::model();
		$this->render('index', [
			'iblocks' => $oIblock->findAll(),
			'iblock' => $oIblock,
		]);
	}

	public function actionIblock($iblock)
	{
		$iblock = (new CmsIblock())->findByPk($iblock);
		$this->render('iblock/index', [
			'iblocks' => $iblock,
			'iblockElements' => $iblock->cmsIblockElements,
			'element' => (new CmsIblockElements())
		]);
	}

	public function actionElement($element)
	{
		$iblockElement = (new CmsIblockElements())->findByPk($element);
		$this->render('iblock/element/index', [
			'iblock' => (new CmsIblock())->findByPk($iblockElement->cms_iblock_id),
			'element' => $iblockElement
		]);
	}


	public function actionAdd($type, $iblock = null)
	{
		if (Yii::app()->request->isPostRequest) {
			$oIblock = (new CmsIblock());
			$oElement = (new CmsIblockElements());

			$data = Yii::app()->request->getPost($type == 1 ? 'CmsIblock' : 'CmsIblockElements');
			if ($type == 1) {
				$oIblock->setAttributes($data);
				if ($oIblock->validate()) {
					$oIblock->save(false);
				}
				$this->render('iblock/index', [
					'iblocks' => $oIblock,
					'iblockElements' => $oIblock->cmsIblockElements,
					'element' => (new CmsIblockElements())
				]);
			} else {
				$oElement->setAttributes($data);
				$oElement->cms_iblock_id = $iblock;
				$oElement->save(false);
				$this->render('iblock/element/index', [
					'iblock' => (new CmsIblock())->findByPk($oElement->cms_iblock_id),
					'element' => $oElement
				]);
			}
		}
	}

	public function actionSave($type, $id)
	{
		if (Yii::app()->request->isPostRequest) {

			$data = Yii::app()->request->getPost($type == 1 ? 'CmsIblock' : 'CmsIblockElements');

			if ($type == 1) {
				$oIblock = (new CmsIblock())->findByPk($id);
				$oIblock->setAttributes($data);
				$oIblock->save(false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				return $this->render('iblock/index', [
					'iblocks' => $oIblock,
					'iblockElements' => $oIblock->cmsIblockElements,
					'element' => (new CmsIblockElements())
				]);
			} else {
				$oElement = (new CmsIblockElements())->findByPk($id);

				$oElement->setAttributes($data);
				self::SaveImage($oElement);

				$oElement->save(false);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$this->render('iblock/element/index', [
					'iblock' => (new CmsIblock())->findByPk($oElement->cms_iblock_id),
					'element' => $oElement
				]);
			}
		}
	}

	private static function SaveImage($oElement)
	{
		if ($imageNameMain = Files::upload($oElement::IMAGES_DIR, 'Images')) {

			if ($oElement->image) {
				Files::delete('main_' . $oElement->image, $oElement::IMAGES_DIR);
				Files::delete($oElement->image, $oElement::IMAGES_DIR);
			}
			if (Files::copy($imageNameMain, 'main_' . $imageNameMain, $oElement::IMAGES_DIR)) {
				Files::imageResize('main_' . $imageNameMain, $oElement::IMAGES_DIR, 370, 450);
			}
			$oElement->image = $imageNameMain;
		}
	}

	public function actionRemove()
	{

		$iD = Yii::app()->request->getPost('iD');
		$element = Yii::app()->request->getPost('element');

		if (!$iD)
			throw new Exception('id is empty');

		if (!$element) {
			$oElement = CmsIblockElements::model();
			$oElements = $oElement->findAllByAttributes(['cms_iblock_id' => $iD]);
			foreach ($oElements as $element) {
				self::DeleteIBlockElementImages($element);
			}
			$oElement->deleteAllByAttributes(['cms_iblock_id' => $iD]);
			CmsIblock::model()->deleteByPk($iD);
		} else {
			$oElement = CmsIblockElements::model();
			self::DeleteIBlockElementImages($oElement->findByPk($iD));
			$oElement->deleteByPk($iD);
		}
	}

	public function actionDeleteImageElement() {

		if(Yii::app()->request->isAjaxRequest) {
			$image_id = Yii::app()->request->getPost('imageID');
			if (!$image_id)
				throw new Exception('imageID empty');

			echo $image_id;

			$oIBlockElementImages = new CmsIblockElementImages;
			$image = $oIBlockElementImages->findByPk($image_id);

			self::DeleteIBlockElementImages($image);

			$oIBlockElementImages->deleteByPk($image_id);
		}
	}


	private static function DeleteIBlockElementImages($element)
	{
		if ($element->image) {
			Files::delete('main_' . $element->image, $element::IMAGES_DIR);
			Files::delete($element->image, $element::IMAGES_DIR);
		}
		if (isset($element->cmsIblockElementImages))
			foreach ($element->cmsIblockElementImages as $image) {
				Files::delete('main_' . $image->image, $image::IMAGES_DIR);
				Files::delete($image->image, $image::IMAGES_DIR);
			}

	}

	public function actionUploadImage()
	{
		$oIBlockElementImages = new CmsIblockElementImages;
		if (Yii::app()->request->isPostRequest) {
			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$data = json_decode(Yii::app()->request->getPost('params'));
				if ($imageName = Files::upload($oIBlockElementImages::IMAGES_DIR, 'File')) {

					if (Files::copy($imageName, 'icon_' . $imageName, $oIBlockElementImages::IMAGES_DIR)) {
						Files::imageResize('icon_' . $imageName, $oIBlockElementImages::IMAGES_DIR, 100, 300);
					}
					if (Files::copy($imageName, 'main_' . $imageName, $oIBlockElementImages::IMAGES_DIR)) {
						Files::imageResize('main_' . $imageName, $oIBlockElementImages::IMAGES_DIR, 200, 400);
					}
				}
				$oIBlockElementImages->cms_iblock_elements_id = $data->element_id;
				$oIBlockElementImages->image = $imageName;
				$oIBlockElementImages->el_i_active = 1;
				$oIBlockElementImages->save();
			}
		}
	}


}