<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/12/2018
 * Time: 15:38
 */

class MenuController extends AController
{
	public function actionIndex() {

		$this->render('index', [
			'menu' => CmsNestedPage::model()->roots()->findAll(),
			'pages' => CmsPage::model()->getPagesWithoutNested()
		]);
	}


	public function actionDelete($root)
	{

		if($root == 1) {
			Yii::app()->user->setFlash('error', 'Невозможно удалить "Главную страницу"');
			$this->redirect($this->createUrl('/admin/menu/'));
			return 1;
		}
		$model = CmsNestedPage::model()->findByPk($root);
		if($model->children()->find()){
			Yii::app()->user->setFlash('error', 'Невозможно удалить элемент имеющий дочерние блоки.');
			$this->redirect($this->createUrl('/admin/menu/'));
		}
		try {
			CmsNestedPage::model()->deleteByPk($root);
			Yii::app()->user->setFlash('success', 'Элемент удален');
		} catch (Exception $e) {
			Yii::app()->user->setFlash('error', $e->getMessage());
		}
		$this->redirect($this->createUrl('/admin/menu/'));
	}

	public function actionMenuAdd() {
		if(Yii::app()->request->isPostRequest) {
			$page_id = Yii::app()->request->getPost('MenuID');

			$page = new CmsNestedPage;
			$category = CmsNestedPage::model()->findByPk(1);
			$page->cms_page_id = $page_id;
			$page->appendTo($category);

		}
		$this->redirect('/admin/menu');
	}

	public function actionChangePosition()
	{
		$request = Yii::app()->request->getPost('pos');
		$element = CmsNestedPage::model()->findByPk($request['id']);
		$target = CmsNestedPage::model()->findByPk($request['targetId']);
		switch ($request['action']) {
			case 'before':
				$element->moveBefore($target);
				break;
			case 'after':
				$element->moveAfter($target);
				break;
			case 'append':
				$element->moveAsFirst($target);
				break;
		}
	}
}