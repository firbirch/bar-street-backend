<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 10/01/2019
 * Time: 16:19
 */

class SettingsController extends AController
{
	public function actionIndex() {
		$oSetting = CmsSiteSettings::model();

		//CVarDumper::dump($oSetting->getSettings(), 10, true);
		//echo '<pre>'.print_r($oSetting->getSettings(), true).'</pre>';

		$this->render('index', [
			'settings' => $oSetting->findAll(),
			'settingParams' => $oSetting
		]);
	}


	public function actionRemove($field_id) {
		if(!$field_id)
			throw new Exception('field_id empty');

		$oSetting = CmsSiteSettings::model();
		if($field_id != 1)
			$oSetting->deleteByPk($field_id);

		$this->render('index', [
			'settings' => $oSetting->findAll(),
			'settingParams' => $oSetting
		]);
	}

	public function actionSaveAll() {
		$oSetting = CmsSiteSettings::model();
		if(Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('CmsSiteSettings');

			$fields = Yii::app()->request->getPost('CurrentSetting');
			foreach($fields as $key => $field) {
				$setting = $oSetting->findByPk($key);
				$setting->field_value = $field;
				$setting->save(false);
			}

			$oSetting->setAttributes($data);
			if ($oSetting->validate()) {
				$oSetting->isNewRecord = true;
				$oSetting->id = NULL;
				$oSetting->save(false);

				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
			}



		}

		$this->render('index', [
			'settings' => $oSetting->findAll(),
			'settingParams' => $oSetting
		]);
	}

}