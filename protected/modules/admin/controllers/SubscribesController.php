<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12/02/2019
 * Time: 12:49
 */

class SubscribesController extends AController
{

	public function actionIndex() {
		$oSubscribe = CmsModuleSubscribes::model()->findAll();
		$this->render('index', [
			'subscribes' => $oSubscribe
		]);
	}

	public function actionRemove() {
		if(Yii::app()->request->isAjaxRequest) {
			CmsModuleSubscribes::model()->deleteByPk(Yii::app()->request->getPost('subscribe_id'));
		}
	}

	public function actionExport() {


		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="export_' . date('d.m.Y') . '.csv"');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Description: File Transfer');

		foreach( Yii::app()->log->routes as $route ){
			if( $route instanceof CWebLogRoute ){
				$route->enabled = false;
			}
		}

		$data = "Почта;\r\n";
		$oSubscribe = CmsModuleSubscribes::model()->findAll();


		$fp = fopen('php://output', 'w');
		fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		if ($fp)
		{
			foreach ($oSubscribe as $value) {
				echo $data .= $value->subscribe_email.
					"\r\n";
			}
		}
		Yii::app()->end();
	}

}