<?php

class DefaultController extends AController
{
	public function actionIndex()
	{
		$authForm = new LoginForm();
		if(Yii::app()->request->isPostRequest && empty(Yii::app()->user->id)) {
			$data = Yii::app()->request->getPost('LoginForm');
			$authForm->setAttributes($data);
			if($authForm->validate() && $authForm->login()) {
				$this->refresh();
			}
		}
		if(empty(Yii::app()->user->id)) {
			$this->layout = 'admin_main_login';
			$this->render('login', [
				'auth' => $authForm
			]);
		} else { // Уже авторизованы
			$this->render('index');
		}
	}

	public function filters()
	{
		return ['accessControl'];
	}

	public function accessRules()
	{
		return [
			['allow', 'roles' => ['guest']],
		];
	}
}