<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 14.06.2019
 * Time: 0:44
 */

class ServiceLabelsController extends AController
{
	public function actionIndex() {
		$oLabels = CmsModuleServiceLabels::model()->findAll();
		$this->render('index', [
			'labels' => $oLabels
		]);
	}

	public function actionShow($label = 0) {
		$oLabels = CmsModuleServiceLabels::model();

		if($label) {
			$label = $oLabels->findByPk($label);
		} else {
			$label = $oLabels;
		}

		$this->render('show/index', [
			'label' => $label
		]);
	}

	public function actionSaveAll($label) {

		if(Yii::app()->request->isPostRequest) {

			$oLabels = new CmsModuleServiceLabels();

			$data = Yii::app()->request->getPost('CmsModuleServiceLabels');

			if ($label) {
				$label = $oLabels->findByPk($label);
			} else {
				$label = $oLabels;
			}

			$label->setAttributes($data);

			if($label->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$label->save(false);
			}
			else
				print_r($label->getErrors());

			$this->render('show/index', [
				'label' => $label
			]);

		}
	}

	public function actionRemove() {
		CmsModuleServiceLabels::model()->deleteByPk(Yii::app()->request->getParam('id'));
	}
}