<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/02/2019
 * Time: 13:51
 */

class ClientsController extends AController
{

	public function actionIndex() {

		$this->render('index', [
			'clients' => CmsModuleClients::model()->findAll()
		]);

	}

	public function actionShow($client = null) {

		$oClients = CmsModuleClients::model();

		if($client)
			$client = $oClients->findByPk($client);
		else
			$client = $oClients;

		$this->render('show/index', [
			'client' => $client
		]);

	}

	public function actionSaveAll($client = null) {

		$oClients = new CmsModuleClients;
		if(Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('CmsModuleClients');

			if($client)
				$client = $oClients->findByPk($client);
			else
				$client = $oClients;

			$client->setAttributes($data);
			if($client->validate()) {
				$this->SaveImages($client);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$client->save(false);
			}
			else
				print_r($client->getErrors());

			$this->render('show/index', [
				'client' => $client
			]);

		}

	}


	public function actionRemove() {
		if(Yii::app()->request->isPostRequest) {
			$clientId = Yii::app()->request->getPost('client');

			$clients = CmsModuleClients::model()->findByPk($clientId);
			if ($clients->client_main_image) {
				Files::delete($clients->client_main_image, $clients::IMAGES_DIR);
			}
			$clients->delete();
		}
	}

	protected function SaveImages(CmsModuleClients $clients) {
		if ($imageNameMain = Files::upload($clients::IMAGES_DIR, 'MainImage')) {
			if ($clients->client_main_image) {
				Files::delete($clients->client_main_image, $clients::IMAGES_DIR);
			}
			$clients->client_main_image = $imageNameMain;
		}
		if ($imageNameImage = Files::upload($clients::IMAGES_DIR, 'Image')) {
			if ($clients->client_image) {
				Files::delete($clients->client_image, $clients::IMAGES_DIR);
			}
			$clients->client_image = $imageNameImage;
		}
	}

}