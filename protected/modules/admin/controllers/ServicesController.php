<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 14:55
 */

class ServicesController extends AController
{

	public function actionIndex() {
		$oService = CmsModuleService::model()->findAll(['order' => 'service_sort ASC']);
		$this->render('index', [
			'services' => $oService
		]);

	}
	public function actionCategory() {
		$oCategory = CmsModuleServiceCategory::model()->findAll('category_id != 1');
		$this->render('category/index', [
			'categories' => $oCategory
		]);

	}
	public function actionAdvantages() {
		$oAdvantages = CmsModuleServiceAdvantages::model()->findAll('advantage_id != 1');
		$this->render('advantages/index', [
			'advantages' => $oAdvantages
		]);
	}

	public function actionSubcategory() {
		$oSubCategory = CmsModuleSubcategory::model()->findAll('subcategory_id != 1');
		$this->render('subcategory/index', [
			'categories' => $oSubCategory
		]);
	}

	// -----

	public function actionAdvantageSaveAll($advantage = 0) {
		if(Yii::app()->request->isPostRequest) {

			$oAdvantage = new CmsModuleServiceAdvantages();
			if($advantage) {
				$advantage = $oAdvantage->findByPk($advantage);
			} else {
				$advantage = $oAdvantage;
			}





			$data = Yii::app()->request->getPost('CmsModuleServiceAdvantages');

			$advantage->setAttributes($data);
			if($advantage->validate()) {



				//$this->SaveImageAdvantage($advantage);
				$advantage->save(false);
				$this->saveAdvantageParams($advantage);
			}
			else
				print_r($advantage->getErrors());

			$this->render('advantages/show', [
				'advantage' => $advantage
			]);

		}
	}


	public function actionadvantageRemove() {
		if(Yii::app()->request->isAjaxRequest) {
			$advantage = Yii::app()->request->getPost('advantage');
			CmsAdvantageParams::model()->deleteAllByAttributes(['cms_module_service_advantages_advantage_id' => $advantage]);
			CmsModuleAdvantage::model()->deleteAllByAttributes(['cms_module_service_advantages_advantage_id' => $advantage]);
			CmsModuleServiceAdvantages::model()->deleteByPk($advantage);
		}
	}


	public function actionadvantageRemoveParam() {
		if(Yii::app()->request->isPostRequest) {
			$paramId = Yii::app()->request->getPost('id');
			CmsAdvantageParams::model()->deleteByPk($paramId);
		}
	}

	protected function saveAdvantageParams(CmsModuleServiceAdvantages $advantages) {

		//$oAdvantageParams = CmsAdvantageParams::model()->deleteAllByAttributes(['cms_module_service_advantages_advantage_id' => $advantages->advantage_id]);

		$oAdvantageParams = CmsAdvantageParams::model();

		$paramsId = Yii::app()->request->getPost('params_id');
		$params = Yii::app()->request->getPost('params');
		$paramsTag = Yii::app()->request->getPost('params_tag');

		$newParams = Yii::app()->request->getPost('newParams');
		$newParamsTag = Yii::app()->request->getPost('newParamsTag');


		if($paramsId)
		foreach($paramsId as $key => $row) {
			$params_ = $oAdvantageParams->findByPk($row);
			$params_->params_content = $params[$key];
			$params_->params_tag = $paramsTag[$key];
			$params_->save(false);
		}

		if($newParams)
		foreach($newParams as $key => $row) {
			if(empty($row)) continue;
			$oAdvantageParams->isNewRecord = true;
			$oAdvantageParams->params_id = NULL;
			$oAdvantageParams->params_content = $row;
			$oAdvantageParams->params_tag = $newParamsTag[$key];
			$oAdvantageParams->cms_module_service_advantages_advantage_id = $advantages->advantage_id;
			$oAdvantageParams->save(false);
		}
	}


	protected function SaveImageAdvantage(CmsModuleServiceAdvantages $advantages) {
		if ($imageNameMain = Files::upload($advantages::IMAGES_DIR, 'MainImage')) {
			if ($advantages->advantage_icon) {
				Files::delete($advantages->advantage_icon, $advantages::IMAGES_DIR);
			}
			$advantages->advantage_icon = $imageNameMain;
		}
	}


	public function actionSubcategorySaveAll($subcategory = 0) {
		if(Yii::app()->request->isPostRequest) {

			$oSubCategory = new CmsModuleSubcategory();
			if($subcategory) {
				$subcategory = $oSubCategory->findByPk($subcategory);
			} else {
				$subcategory = $oSubCategory;
			}

			$data = Yii::app()->request->getPost('CmsModuleSubcategory');

			$subcategory->setAttributes($data);
			if($subcategory->validate()) $subcategory->save(false);
			else
				print_r($subcategory->getErrors());

			$this->render('subcategory/show', [
				'category' => $subcategory
			]);

		}
	}

	public function actionCategorySaveAll($category = 0) {
		if(Yii::app()->request->isPostRequest) {
			$oCategory = new CmsModuleServiceCategory();
			if($category) {
				$category = $oCategory->findByPk($category);
			} else {
				$category = $oCategory;
			}

			$data = Yii::app()->request->getPost('CmsModuleServiceCategory');

			$category->setAttributes($data);
			if($category->validate()) $category->save(false);
			else
				print_r($category->getErrors());

			$this->render('category/show', [
				'category' => $category
			]);
		}
	}



	private static function saveAdvantages(CmsModuleService $service) {

		$oAdvantageRelation = new CmsModuleAdvantage();
		$oAdvantageRelation->deleteAllByAttributes(['cms_module_service_service_id' => $service->service_id]);

		$advantage = Yii::app()->request->getPost('Advantages');

		if($advantage)
		foreach($advantage as $item) {
			$str = null;
			$str = explode('_', $item);


			$oAdvantageRelation->isNewRecord = true;
			$oAdvantageRelation->id = null;
			$oAdvantageRelation->cms_module_service_service_id = $service->service_id;
			$oAdvantageRelation->cms_module_service_advantages_advantage_id = $str[0];
			if(isset($str[1])) {
				$oAdvantageRelation->cms_module_params = $str[1];
			} else {
				$oAdvantageRelation->cms_module_params = null;
			}
			$oAdvantageRelation->save(false);
		}
	}



	public function actionSaveAll($service = 0) {
		if(Yii::app()->request->isPostRequest) {


			$oService = new CmsModuleService();


			if($service) {
				$service = $oService->findByPk($service);
			} else {
				$service = $oService;
			}

			$data = Yii::app()->request->getPost('CmsModuleService');




			$service->setAttributes($data);

			if($service->validate()) {

				$this->SaveImageService($service);
				$this->SaveEnImageService($service);
				$service->save(false);

				self::saveAdvantages($service);

			}
			else
				print_r($service->getErrors());



			$this->render('show', [
				'service' => $service,
				'categories' => CmsModuleServiceCategory::model()->findAll(),
				'advantages' => CmsModuleServiceAdvantages::model()->findAll(),
				'advantagesIds' => $service->getAdvantageIds(),
				'categoryController' => CmsModuleSerivceCategoryAction::model()->findAll(),
				'labels' => CmsModuleServiceLabels::model()->findAll(),
				'subcategory' => CmsModuleSubcategory::model()->findAll()
			]);
		}
	}

	protected function SaveImageService(CmsModuleService $service) {
		if ($imageNameMain = Files::upload($service::IMAGES_DIR, 'MainImage')) {
			if ($service->service_image) {
				Files::delete($service->service_image, $service::IMAGES_DIR);
			}
			$service->service_image = $imageNameMain;
		}
	}

	protected function SaveEnImageService(CmsModuleService $service) {
		if ($imageNameMain = Files::upload($service::IMAGES_DIR, 'MainImageEn')) {
			if ($service->service_image_en) {
				Files::delete($service->service_image_en, $service::IMAGES_DIR);
			}
			$service->service_image_en = $imageNameMain;
		}
	}

	// -----
	public function actionShow($service = 0) {
		$oService = CmsModuleService::model();
		if($service) {
			$service = $oService->findByPk($service);
		} else {
			$service = $oService;
			$service->cms_module_service_category_category_id = 1;
		}

//		CVarDumper::dump($service->getAdvantageIds(), true, 10);
//		die();


		$this->render('show', [
			'service' => $service,
			'categories' => CmsModuleServiceCategory::model()->findAll(),
			'advantages' => CmsModuleServiceAdvantages::model()->findAll(),
			'advantagesIds' => $service->getAdvantageIds(),
			'categoryController' => CmsModuleSerivceCategoryAction::model()->findAll(),
			'labels' => CmsModuleServiceLabels::model()->findAll(),
			'subcategory' => CmsModuleSubcategory::model()->findAll()
		]);
	}

	public function actionshowAdvantages($advantage = 0) {
		$oAdvantage = CmsModuleServiceAdvantages::model();
		if($advantage) {
			$advantage = $oAdvantage->findByPk($advantage);
		} else {
			$advantage = $oAdvantage;
		}
		$this->render('advantages/show', [
			'advantage' => $advantage
		]);
	}

	public function actionshowCategory($category = 0) {
		$oCategory = CmsModuleServiceCategory::model();
		if($category) {
			$category = $oCategory->findByPk($category);
		} else {
			$category = $oCategory;
		}
		$this->render('category/show', [
			'category' => $category
		]);
	}

	public function actionshowSubCategory($subcategory = 0) {
		$oSubCategory = CmsModuleSubcategory::model();
		if($subcategory) {
			$subcategory = $oSubCategory->findByPk($subcategory);
		} else {
			$subcategory = $oSubCategory;
		}
		$this->render('subcategory/show', [
			'category' => $subcategory
		]);
	}

	public function actionremoveSubCategory($subcategory) {

		$oSubcategory = CmsModuleSubcategory::model()->findByPk($subcategory);
		foreach($oSubcategory->cmsModuleServices as $row) {
			$product = CmsModuleService::model()->findByPk($row->service_id);
			$product->cms_module_subcategory_subcategory_id = 1;
			$product->save(false);
		}
		$oSubcategory->delete();
		$this->redirect('/admin/services/subcategory');
	}


	public function actionServiceDelete() {
		if(Yii::app()->request->isAjaxRequest) {
			$data = Yii::app()->request->getPost('service');

			$oService = CmsModuleService::model()->findByPk($data);
			if($oService) {

				$oAdvantageRelation = new CmsModuleAdvantage();
				$oAdvantageRelation->deleteAllByAttributes(['cms_module_service_service_id' => $oService->service_id]);

				if ($oService->service_image) {
					Files::delete($oService->service_image, $oService::IMAGES_DIR);
				}

				$oService->delete();



			} else {
				throw new Exception('Cannot delete');
			}

		}
	}

}