<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/12/2018
 * Time: 15:25
 */

class PageController extends AController
{
	//
	public function actionIndex($sort = null, $direction = 'ASC') {
		$oPage = new CmsPage;
		$this->render('index', [
			'page' => $oPage->findAll(['condition' => 'id != 1', 'order' => $sort ?: 'id '.$direction]),
		]);
	}
	public function actionShow($page = 0) {
		$oPage = new CmsPage;
		$this->render('show/index', [
			'page' => $page ? $oPage->findByPk($page) : $oPage
		]);

	}
	public function actionChangeSort() {
		if(Yii::app()->request->isPostRequest) {
			$pageID = Yii::app()->request->getPost('pageId');
			if(!$pageID)
				throw new Exception('pageID empty');

			$oPage = CmsPage::model()->findByPk($pageID);
			$oPage->sort = Yii::app()->request->getPost('pageSort');
			$oPage->save(false);
		}
	}

	public function actionSaveAll($pageID) {
		$oPage = new CmsPage;
		if(Yii::app()->request->isPostRequest && Yii::app()->request->getPost('Save')) {
			$data = Yii::app()->request->getPost('CmsPage');


			$oPage = !$pageID ? $oPage : $oPage->findByPk($pageID);

			$oPage->setAttributes($data);
			if(!$pageID)
				$oPage->active = 1;

			if($oPage->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$oPage->save(false);
			}
			else
				print_r($oPage->getErrors());

			$this->render('show/index', [
				'page' => $oPage
			]);
		}
	}

	public function actionRemove() {
		if(Yii::app()->request->isAjaxRequest) {
			$pageID = Yii::app()->request->getPost('page');
			if(!$pageID)
				throw new Exception('pageID empty');

			$remove = (new CmsNestedPage())->deleteAllByAttributes(['cms_page_id' => $pageID]);
			$remove = (new CmsPage())->deleteByPk($pageID);
		}
	}
}