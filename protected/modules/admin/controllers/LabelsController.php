<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 11:32
 */


class LabelsController extends AController
{

	public function actionIndex() {
		$oLabels = CmsShopProductLabels::model()->findAll();
		$this->render('index', [
			'labels' => $oLabels
		]);
	}

	public function actionShow($label = 0) {
		$oLabels = CmsShopProductLabels::model();

		if($label) {
			$label = $oLabels->findByPk($label);
		} else {
			$label = $oLabels;
		}

		$this->render('show/index', [
			'label' => $label
		]);
	}

	public function actionSaveAll($label) {

		if(Yii::app()->request->isPostRequest) {

			$oLabels = new CmsShopProductLabels();

			$data = Yii::app()->request->getPost('CmsShopProductLabels');

			if ($label) {
				$label = $oLabels->findByPk($label);
			} else {
				$label = $oLabels;
			}

			$label->setAttributes($data);

			if($label->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$label->save(false);
			}
			else
				print_r($label->getErrors());

			$this->render('show/index', [
				'label' => $label
			]);

		}
	}

}