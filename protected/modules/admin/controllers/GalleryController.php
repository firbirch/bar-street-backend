<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 20/12/2018
 * Time: 13:03
 */

class GalleryController extends AController
{
	public function actionIndex() {
		$this->render('index', [
			'galleries' => CmsModuleGallery::model()->findAll(['condition' => 'id != 1', 'order' => 'date DESC'])
		]);
	}
	public function actionShow($galleryID = null) {
		$oGallery = new CmsModuleGallery;


		if($galleryID) {
			$galleryID = $oGallery->findByPk($galleryID);
		} else {
			$galleryID = $oGallery;
		}

		$this->render('show/index', [
			'gallery' => $galleryID,
			'services' => CmsModuleService::model()->findAll(['condition' => 'service_active = 1']),
			'servicesId' => $galleryID->getServiceIds()
		]);
	}

	public function actionSaveAll($galleryID) {

		$oGallery = new CmsModuleGallery;
		if(Yii::app()->request->isPostRequest && Yii::app()->request->getPost('Save')) {
			$data = Yii::app()->request->getPost('CmsModuleGallery');

			if($galleryID) {
				$galleryID = $oGallery->findByPk($galleryID);
				self::SaveServices($galleryID);
			} else {
				$galleryID = $oGallery;
			}

			$galleryID->setAttributes($data);

			if($galleryID->validate()) {

				self::SaveGalleryImages();
				self::SaveAnnounceImage($galleryID);
				self::SaveEngAnnounceImage($galleryID);
				$galleryID->save(false);



				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
			}
			else
				print_r($oGallery->getErrors());

			$this->render('show/index',[
				'gallery' => $galleryID,
				'services' => CmsModuleService::model()->findAll('service_active = 1'),
				'servicesId' => $galleryID->getServiceIds()
			]);
		}
	}

	public function actionDeleteImage() {
		if(Yii::app()->request->isAjaxRequest) {
			$imageID = Yii::app()->request->getPost('imageID');
			$oImage = CmsModuleGalleryImage::model()->findByPk($imageID);
			Files::delete('icon_'.$oImage->image, $oImage::IMAGES_DIR);
			Files::delete('main_'.$oImage->image, $oImage::IMAGES_DIR);
			Files::delete($oImage->image, $oImage::IMAGES_DIR);
			$oImage->delete();
		}
	}

	public function actionChangeImagesSort() {
		if(Yii::app()->request->isAjaxRequest) {
			$oImage = CmsModuleGalleryImage::model();

			$sort = Yii::app()->request->getPost('sort');
			if(is_array($sort))
			foreach($sort as $key => $row) {
				$image = $oImage->findByPk($row);
				$image->i_sort = $key + 1;
				$image->save(false);
			}
		}
	}

	private static function SaveGalleryImages() {
		$oImages = CmsModuleGalleryImage::model();
		$data = Yii::app()->request->getPost('image');
		if($data['imageID']) {
			foreach($data['imageID'] as $key => $imageID) {
				if(!$imageID) continue;
				$image = $oImages->findByPk($imageID);
				$image->imageTitle = $data['imageName'][$key];
				$image->imageAlt = $data['imageAlt'][$key];
				$image->save(false);
			}
		}
	}

	public function actionDelete() {
		if(Yii::app()->request->isAjaxRequest) {
			$data = Yii::app()->request->getPost('gallery');
			$oGallery = CmsModuleGallery::model()->findByPk($data);
			if($oGallery) {
				$oGalleryRelation = new CmsGalleryRelationService;
				$oGalleryRelation->deleteAllByAttributes(['cms_module_gallery_id' => $oGallery->id]);

				if ($oGallery->announce_image) {
					Files::delete('main_' . $oGallery->announce_image, $oGallery::IMAGES_DIR);
					Files::delete($oGallery->announce_image, $oGallery::IMAGES_DIR);
				}

				$oImages = CmsModuleGalleryImage::model();
				$images = $oImages->findAllByAttributes(['cms_module_gallery_id' => $oGallery->id]);
				if($images && is_array($images))
				foreach($images as $image) {
					Files::delete('icon_'.$image->image, $oImages::IMAGES_DIR);
					Files::delete('main_'.$image->image, $oImages::IMAGES_DIR);
					Files::delete('thumb_'.$image->image, $oImages::IMAGES_DIR);
					Files::delete($image->image, $oImages::IMAGES_DIR);
					$image->delete();
				}
				$oGallery->delete();

			} else {
				throw new Exception('Cannot remove');
			}
		}
	}

	private static function SaveServices(CmsModuleGallery $gallery) {
		$services = Yii::app()->request->getPost('GalleryService');

		$oGalleryRelation = new CmsGalleryRelationService;
		$oGalleryRelation->deleteAllByAttributes(['cms_module_gallery_id' => $gallery->id]);
		if(is_array($services))
		foreach($services as $service) {
			$oGalleryRelation->isNewRecord = true;
			$oGalleryRelation->relation_id = null;
			$oGalleryRelation->cms_module_service_service_id = $service;
			$oGalleryRelation->cms_module_gallery_id = $gallery->id;
			$oGalleryRelation->save(false);
		}

	}

	private static function SaveAnnounceImage(CmsModuleGallery $model) {
		if ($imageNameMain = Files::upload($model::IMAGES_DIR, 'AnnounceImage')) {
			if ($model->announce_image) {
				Files::delete('main_' . $model->announce_image, $model::IMAGES_DIR);
				Files::delete($model->announce_image, $model::IMAGES_DIR);
			}
			if (Files::copy($imageNameMain, 'main_' . $imageNameMain, $model::IMAGES_DIR)) {
				Files::imageResize('main_' . $imageNameMain, $model::IMAGES_DIR, 370, 450);
			}
			$model->announce_image = $imageNameMain;
		}
	}

	private static function SaveEngAnnounceImage(CmsModuleGallery $model) {
		if ($imageNameMain = Files::upload($model::IMAGES_DIR, 'AnnounceImageEng')) {
			if ($model->announce_image_eng) {
				Files::delete('main_' . $model->announce_image_eng, $model::IMAGES_DIR);
				Files::delete($model->announce_image_eng, $model::IMAGES_DIR);
			}
			if (Files::copy($imageNameMain, 'eng_main_' . $imageNameMain, $model::IMAGES_DIR)) {
				Files::imageResize('eng_main_' . $imageNameMain, $model::IMAGES_DIR, 476, 350);
			}
			$model->announce_image_eng = $imageNameMain;
		}
	}

	public function actionUploadImage()
	{
		$oGalleryImage = new CmsModuleGalleryImage;
		if (Yii::app()->request->isPostRequest) {
			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

				$data = json_decode(Yii::app()->request->getPost('params'));
				if ($imageName = Files::upload($oGalleryImage::IMAGES_DIR, 'File')) {
					if (Files::copy($imageName, 'icon_' . $imageName, $oGalleryImage::IMAGES_DIR)) {
						Files::imageResize('icon_' . $imageName, $oGalleryImage::IMAGES_DIR, 100, 300);
					}
					if (Files::copy($imageName, 'main_' . $imageName, $oGalleryImage::IMAGES_DIR)) {
						Files::imageResize('main_' . $imageName, $oGalleryImage::IMAGES_DIR, 200, 400);
					}
					$oGalleryImage->cms_module_gallery_id = $data->gallery_id;
					$oGalleryImage->image = $imageName;
					$oGalleryImage->i_active = 1;
					$oGalleryImage->save();
				}
			}
		}
	}

}