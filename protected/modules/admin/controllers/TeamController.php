<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 08/02/2019
 * Time: 15:20
 */


// Мероприятия для
class TeamController extends AController
{
	public function actionIndex() {

		$this->render('index', [
			'events' => CmsModuleTeamEvent::model()->findAll()
		]);

	}

	public function actionShow($event = null) {

		$oEvent = CmsModuleTeamEvent::model();
		if($event) {
			$event = $oEvent->findByPk($event);
		} else {
			$event = $oEvent;
		}

		$this->render('show/index', [
			'event' => $event
		]);
	}

	public function actionSaveAll($event = null) {
		if(Yii::app()->request->isPostRequest) {

			$oEvent = new CmsModuleTeamEvent;
			if ($event) {
				$event = $oEvent->findByPk($event);
			} else {
				$event = $oEvent;
			}
			$data = Yii::app()->request->getPost('CmsModuleTeamEvent');
			$event->setAttributes($data);
			if($event->validate())
				$event->save(false);
			else
				print_r($event->getErrors());
			$this->render('show/index', [
				'event' => $event
			]);
		}
	}

	public function actionDeleteImage() {
		if(Yii::app()->request->isAjaxRequest) {
			$imageID = Yii::app()->request->getPost('imageID');
			$oImage = CmsModuleTeamGallery::model()->findByPk($imageID);
			Files::delete('icon_'.$oImage->image, $oImage::IMAGES_DIR);
			Files::delete('main_'.$oImage->image, $oImage::IMAGES_DIR);
			Files::delete($oImage->image, $oImage::IMAGES_DIR);
			$oImage->delete();
		}
	}

	public function actionUploadImage()
	{
		$oGalleryImage = new CmsModuleTeamGallery();
		if (Yii::app()->request->isPostRequest) {
			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$data = json_decode(Yii::app()->request->getPost('params'));
				if ($imageName = Files::upload($oGalleryImage::IMAGES_DIR, 'File')) {
					if (Files::copy($imageName, 'icon_' . $imageName, $oGalleryImage::IMAGES_DIR)) {
						Files::imageResize('icon_' . $imageName, $oGalleryImage::IMAGES_DIR, 100, 300);
					}
					if (Files::copy($imageName, 'main_' . $imageName, $oGalleryImage::IMAGES_DIR)) {
						Files::imageResize('main_' . $imageName, $oGalleryImage::IMAGES_DIR, 200, 400);
					}
					$oGalleryImage->cms_module_team_event_event_id = $data->event_id;
					$oGalleryImage->gallery_image = $imageName;
					$oGalleryImage->save(false);
				}
			}
		}
	}

}