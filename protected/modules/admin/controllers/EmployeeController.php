<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 08/02/2019
 * Time: 11:02
 */

class EmployeeController extends AController
{
	//employee
	public function actionIndex() {
		$this->render('index', [
			'employies' => CmsModuleEmployee::model()->findAll(['order' => 'employee_sort ASC'])
		]);
	}

	public function actionShow($employee = null) {

		$oEmployee = CmsModuleEmployee::model();

		if($employee)
			$employee = $oEmployee->findByPk($employee);
		else
			$employee = $oEmployee;

		$this->render('show/index', [
			'employee' => $employee
		]);
	}

	public function actionDelete() {
		if(Yii::app()->request->isAjaxRequest) {

			$data = Yii::app()->request->getPost('employee');

			$oEmployee = CmsModuleEmployee::model()->findByPk($data);
			if($oEmployee) {
				if ($oEmployee->employee_icon) {
					Files::delete($oEmployee->employee_icon, $oEmployee::IMAGES_DIR);
					Files::delete('icon_'.$oEmployee->employee_icon, $oEmployee::IMAGES_DIR);
					Files::delete('main_'.$oEmployee->employee_icon, $oEmployee::IMAGES_DIR);
				}
				$oEmployee->delete();

			} else {
				throw new Exception('Cannot delete');
			}


		}
	}

	public function actionSaveAll($employee) {

		if(Yii::app()->request->isPostRequest) {

			$data = Yii::app()->request->getPost('CmsModuleEmployee');

			$oEmployee = new CmsModuleEmployee();
			if ($employee) {
				$employee = $oEmployee->findByPk($employee);
			} else {
				$employee = $oEmployee;
			}

			$employee->setAttributes($data);
			if($employee->validate()) {
				self::saveImage($employee);
				$employee->save(false);
			} else
				print_r($employee->getErrors());

		}
	}
	private static function saveImage(CmsModuleEmployee $employee) {

		if ($image = Files::upload($employee::IMAGES_DIR, 'MainImage')) {
			if ($employee->employee_icon) {
				Files::delete($employee->employee_icon, $employee::IMAGES_DIR);
				Files::delete('icon_'.$employee->employee_icon, $employee::IMAGES_DIR);
				Files::delete('main_'.$employee->employee_icon, $employee::IMAGES_DIR);
			}
			if (Files::copy($image, 'icon_' . $image, $employee::IMAGES_DIR)) {
				Files::imageResize('icon_' . $image, $employee::IMAGES_DIR, 142, 138);
			}
			if (Files::copy($image, 'main_' . $image, $employee::IMAGES_DIR)) {
				Files::imageResize('main_' . $image, $employee::IMAGES_DIR, 200, 400);
			}
			$employee->employee_icon = $image;
		}
	}

}