<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 23/01/2019
 * Time: 14:02
 */

class CoctailsController extends AController
{

	public function actionIndex() {
		$oCoctails = CmsProductCoctails::model()->findAll();
		$this->render('index', [
			'coctails' => $oCoctails
		]);
	}

	public function actionShow($coctail = null) {
		$oCoctails = CmsProductCoctails::model();

		if($coctail) {
			$coctail = $oCoctails->findByPk($coctail);
		} else {
			$coctail = $oCoctails;
		}
		$this->render('show/index', [
			'product' => $coctail,
			'services' => CmsModuleService::model()->findAll(),
			'serviceIds' => $coctail->getServiceIds(),
			'ingredients' => CmsProductCoctailsIngridients::model()->findAll(),
			'ingredientsIds' => $coctail->getIngridientsIds()
		]);
	}

	// Ингредиенты

	public function actionIng() {
		$oIngredients = CmsProductCoctailsIngridients::model()->findAll();
		$this->render('ing/index', [
			'ings' => $oIngredients
		]);
	}
	public function actionShowIng($ing = null) {
		$oIngredients = CmsProductCoctailsIngridients::model();
		if($ing) {
			$ing = $oIngredients->findByPk($ing);
		} else {
			$ing = $oIngredients;
		}
		$this->render('show/ingredients', [
			'ing' => $ing
		]);
	}
	public function actioningSaveAll($ingID) {

		if(Yii::app()->request->isPostRequest) {

			$oIngredients = new CmsProductCoctailsIngridients();

			if ($ingID) {
				$ingID = $oIngredients->findByPk($ingID);
			} else {
				$ingID = $oIngredients;
			}

			$data = Yii::app()->request->getPost('CmsProductCoctailsIngridients');

			$ingID->setAttributes($data);
			if($ingID->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$ingID->save(false);
			} else
				print_r($ingID->getErrors());

			$this->render('show/ingredients', [
				'ing' => $ingID
			]);
		}
	}



	public function actionRemoveIng() {
		if(Yii::app()->request->isPostRequest) {

			$ing = Yii::app()->request->getPost('ing');

			$oRelationIng = new CmsProductRelationsIngridients;
			$oRelationIng->deleteAllByAttributes(['cms_product_coctails_ingridients_ingridient_id' => $ing]);

			CmsProductCoctailsIngridients::model()->deleteByPk($ing);

			$this->redirect('/admin/coctails/ing');
		}
	}



	private static function SaveImage(CmsProductCoctails $model) {


		//echo '<pre>'.print_r($_FILES, true).'</pre>';
		//exit;

		if ($imageNameMain = Files::upload($model::IMAGES_DIR, 'MainImage')) {
			if ($model->product_image) {
				Files::delete('main_' . $model->product_image, $model::IMAGES_DIR);
				Files::delete($model->product_image, $model::IMAGES_DIR);
			}
			if (Files::copy($imageNameMain, 'main_' . $imageNameMain, $model::IMAGES_DIR)) {
				Files::imageResize('main_' . $imageNameMain, $model::IMAGES_DIR, 370, 450);
			}
			$model->product_image = $imageNameMain;
		}
	}

	private static function saveIngredients(CmsProductCoctails $coctail) {


		$oRelationIng = new CmsProductRelationsIngridients;
		$oRelationIng->deleteAllByAttributes(['cms_product_coctails_product_id' => $coctail->product_id]);

		$relation = Yii::app()->request->getPost('Ingridients');

		if($relation)
			foreach ($relation as $item) {
				$oRelationIng->isNewRecord = true;
				$oRelationIng->relation_ingredient_id = NULL;
				$oRelationIng->cms_product_coctails_product_id = $coctail->product_id;
				$oRelationIng->cms_product_coctails_ingridients_ingridient_id = $item;
				$oRelationIng->save(false);
			}
	}

	public function actionDelete() {

		if(Yii::app()->request->isAjaxRequest) {

			$productID = Yii::app()->request->getPost('product');
			$oProduct = CmsProductCoctails::model()->findByPk($productID);
			if ($oProduct) {

				$oRelationIng = new CmsProductRelationsIngridients;
				$oRelationIng->deleteAllByAttributes(['cms_product_coctails_product_id' => $oProduct->product_id]);

				$oRelationService = new CmsProductRelationServices;
				$oRelationService->deleteAllByAttributes(['cms_product_coctails_product_id' => $oProduct->product_id]);

				if ($oProduct->product_image) {
					Files::delete('main_' . $oProduct->product_image, $oProduct::IMAGES_DIR);
					Files::delete($oProduct->product_image, $oProduct::IMAGES_DIR);
				}

				$oProduct->delete();

			} else
				throw new Exception('Remove Error');
		}

	}

	private static function saveServices(CmsProductCoctails $coctail) {
		$oRelationService = new CmsProductRelationServices;
		$oRelationService->deleteAllByAttributes(['cms_product_coctails_product_id' => $coctail->product_id]);
		$relation = Yii::app()->request->getPost('Services');

		if($relation)
		foreach ($relation as $item) {
			$oRelationService->isNewRecord = true;
			$oRelationService->relation_service_id = NULL;
			$oRelationService->cms_product_coctails_product_id = $coctail->product_id;
			$oRelationService->cms_module_service_service_id = $item;
			$oRelationService->save(false);
		}
	}

	public function actionSaveAll($productID)
	{
		if (Yii::app()->request->isPostRequest) {

			$oCoctails = new CmsProductCoctails();
			if($productID) {
				$coctail = $oCoctails->findByPk($productID);
			} else {
				$coctail = $oCoctails;
			}

			$data = Yii::app()->request->getPost('CmsProductCoctails');
			$coctail->setAttributes($data);



			if($coctail->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));

				//self::saveServices($coctail);
				//self::saveIngredients($coctail);
				self::SaveImage($coctail);

				$coctail->save(false);
				self::saveServices($coctail);
				self::saveIngredients($coctail);

			}
			else
				print_r($coctail->getErrors());

			//self::saveIngredients($coctail);

			$this->render('show/index', [
				'product' => $coctail,
				'services' => CmsModuleService::model()->findAll('service_active = 1'),
				'serviceIds' => $coctail->getServiceIds(),
				'ingredients' => CmsProductCoctailsIngridients::model()->findAll(),
				'ingredientsIds' => $coctail->getIngridientsIds()
			]);
		}
	}
}