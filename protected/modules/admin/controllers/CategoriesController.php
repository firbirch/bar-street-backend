<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 10:30
 */

class CategoriesController extends AController
{

	public function actionIndex() {
		$oCategory = CmsShopCategory::model()->findAll();
		$this->render('index', [
			'categories' => $oCategory
		]);
	}
	public function actionShow($category = 0) {
		$oCategory = CmsShopCategory::model();

		if($category) {
			$category = $oCategory->findByPk($category);
		} else {
			$category = $oCategory;
		}

		$this->render('show/index', [
			'category' => $category
		]);
	}


	public function actionDelete() {
		if(Yii::app()->request->isAjaxRequest) {
			$data = Yii::app()->request->getPost('category');
			$oCategory = CmsShopCategory::model()->findByPk($data);
			if($oCategory) {
				$oCategory->delete();
			} else {
				throw new Exception('Cannot delete');
			}

		}
	}

	public function actionSaveAll($category) {

		if(Yii::app()->request->isPostRequest) {

			$oCategory = new CmsShopCategory();
			$data = Yii::app()->request->getPost('CmsShopCategory');

			if($category) {
				$category = $oCategory->findByPk($category);
			} else {
				$category = $oCategory;
			}

			$category->setAttributes($data);
			if($category->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$category->save(false);
			}
			else
				print_r($category->getErrors());

			$this->render('show/index', [
				'category' => $category
			]);
		}
	}
}