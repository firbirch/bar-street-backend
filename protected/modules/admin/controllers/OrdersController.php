<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12/02/2019
 * Time: 18:11
 */

class OrdersController extends AController
{

	public function actionIndex() {
		$this->render('index',[
			'orders' => CmsModuleProductOrder::model()->findAll(['order' => 'order_date DESC'])
		]);
	}

	public function actionRemove($order_id) {
		CmsModuleProductOrder::model()->deleteByPk($order_id);
		$this->redirect('/admin/orders');
	}
}