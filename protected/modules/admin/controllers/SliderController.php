<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 19/12/2018
 * Time: 17:56
 */

class SliderController extends AController
{
	public function actionIndex() {
		$this->render('index', [
			'sliders' => CmsModuleSlider::model()->findAll(['condition' => 'active=:active', 'params' => [':active' => 1]])
		]);
	}
	public function actionShow($sliderID = null) {
		$oSlider = new CmsModuleSlider;
		$this->render('show/index', [
			'slider' => !$sliderID ? $oSlider : $oSlider->findByPk($sliderID)
		]);
	}


	public function actionChangeSort() {
		if(Yii::app()->request->isPostRequest) {
			$sliderID = Yii::app()->request->getPost('sliderId');
			if(!$sliderID)
				throw new Exception('sliderID empty');

			$oSlider = CmsModuleSlider::model()->findByPk($sliderID);
			$oSlider->sort = Yii::app()->request->getPost('sliderSort');
			$oSlider->save(false);
		}
	}

	public function actionSaveAll($sliderID) {
		$oSlider = new CmsModuleSlider;



		if(Yii::app()->request->isPostRequest && Yii::app()->request->getPost('Save')) {
			$data = Yii::app()->request->getPost('CmsModuleSlider');


			$oSlider = !$sliderID ? $oSlider : $oSlider->findByPk($sliderID);
			$oSlider->setAttributes($data);
			if(!$sliderID)
				$oSlider->active = 1;

			
			if($oSlider->validate()) {
				self::SaveImage($oSlider);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$oSlider->save(false);

			} else
				print_r($oSlider->getErrors());

			/*
			$oSlider->validate();

			if($oSlider->hasErrors() == false) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$oSlider->save(false);
			}
			else {
				print_r($oSlider->getErrors());
			}

			*/


			switch(Yii::app()->request->getPost('Save')) {
				case 'Применить':
					$this->render('show/index',[
						'slider' => $oSlider
					]);
					break;
				case 'Применить и выйти':
					$this->redirect('/admin/slider/');
					break;
			}


		}
	}
	private static function SaveImage(CmsModuleSlider $model) {
		if ($imageNameMain = Files::upload($model::IMAGES_DIR, 'MainImage')) {
			if ($model->image) {
				Files::delete('main_' . $model->image, $model::IMAGES_DIR);
				Files::delete($model->image, $model::IMAGES_DIR);
			}
			if (Files::copy($imageNameMain, 'main_' . $imageNameMain, $model::IMAGES_DIR)) {
				Files::imageResize('main_' . $imageNameMain, $model::IMAGES_DIR, 370, 450);
			}
			$model->image = $imageNameMain;
		}


		if ($imageNameMobile = Files::upload($model::IMAGES_DIR, 'MobileImage')) {
			if ($model->mobile_image) {
				Files::delete('mobile_' . $model->mobile_image, $model::IMAGES_DIR);
				Files::delete($model->mobile_image, $model::IMAGES_DIR);
			}
			if (Files::copy($imageNameMobile, 'mobile_' . $imageNameMobile, $model::IMAGES_DIR)) {
				Files::imageResize('mobile_' . $imageNameMobile, $model::IMAGES_DIR, 768, 293);
			}
			$model->mobile_image = $imageNameMobile;
		}
	}
}