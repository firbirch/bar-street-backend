<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 28/01/2019
 * Time: 10:26
 */

class EventsController extends AController
{

	public function actionIndex() {
		$this->render('index', [
			'events' => CmsModuleEvents::model()->findAll(['order' => 'event_date DESC'])
		]);
	}


	public function actionShow($eventID = null) {
		$oEvent = CmsModuleEvents::model();

		if($eventID) {
			$eventID = $oEvent->findByPk($eventID);
		} else {
			$eventID = $oEvent;
		}

		$this->render('show/index', [
			'event' => $eventID,
			'galleries' => CmsModuleGallery::model()->findAll()
		]);
	}

	public function actionDelete() {
		if(Yii::app()->request->isAjaxRequest) {
			$data = Yii::app()->request->getPost('event');
			$oEvent = CmsModuleEvents::model()->findByPk($data);
			if($oEvent) {
				if ($oEvent->event_image) {
					Files::delete($oEvent->event_image, $oEvent::IMAGES_DIR);
				}
				$oEvent->delete();
			} else {
				throw new Exception('Cannot delete');
			}
		}
	}




	private static function saveImage(CmsModuleEvents $event) {

		if ($image = Files::upload($event::IMAGES_DIR, 'MainImage')) {
			if ($event->event_image) {
				Files::delete($event->event_image, $event::IMAGES_DIR);
			}
			if (Files::copy($image, 'main_' . $image, $event::IMAGES_DIR)) {
				Files::imageResize('main_' . $image, $event::IMAGES_DIR, 370, 450);
			}
			$event->event_image = $image;
		}
	}

	public function actionSaveAll($event) {
		if(Yii::app()->request->isPostRequest) {

			$oEvent = new CmsModuleEvents;
			$data = Yii::app()->request->getPost('CmsModuleEvents');

			if($event) {
				$event = $oEvent->findByPk($event);
			} else {
				$event = $oEvent;
			}

			$event->setAttributes($data);
			if($event->validate()) {
				self::saveImage($event);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$event->save(false);
			} else
				print_r($event->getErrors());


			$this->render('show/index', [
				'event' => $event,
				'galleries' => CmsModuleGallery::model()->findAll()
			]);


		}
	}
}