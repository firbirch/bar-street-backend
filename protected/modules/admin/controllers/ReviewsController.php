<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 15:08
 */

class ReviewsController extends AController
{

	public function actionIndex() {
		$oReviews = CmsModuleReviews::model()->findAll();
		$this->render('index', [
			'reviews' => $oReviews
		]);
	}

	public function actionShow($review = 0) {
		$oReviews = CmsModuleReviews::model();
		if($review) {
			$review = $oReviews->findByPk($review);
		} else {
			$review = $oReviews;
		}
		$this->render('show/index', [
			'review' => $review,
			'services' => CmsModuleService::model()->findAll(),
			'gallery' => CmsModuleGallery::model()->findAll()
		]);
	}



	public function actionSaveAll($review = 0) {

		if(Yii::app()->request->isPostRequest) {
			if ($review) {
				$review = CmsModuleReviews::model()->findByPk($review);
			} else {
				$review = new CmsModuleReviews;
			}


			$data = Yii::app()->request->getPost('CmsModuleReviews');
			$review->setAttributes($data);
			if($review->validate()) {
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$review->save(false);
			} else
				print_r($review->getErrors());


			$this->render('show/index', [
				'review' => $review,
				'services' => CmsModuleService::model()->findAll(),
				'gallery' => CmsModuleGallery::model()->findAll()
			]);

		}
	}

	public function actionRemove()
	{
		$id = Yii::app()->request->getParam('page');
		CmsModuleReviews::model()->deleteByPk($id);
	}
}