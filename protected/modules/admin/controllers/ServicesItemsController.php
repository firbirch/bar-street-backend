<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 27.06.2019
 * Time: 11:28
 */

class ServicesItemsController extends AController
{
	public function actionIndex()
	{
		foreach (CmsModuleServiceItems::model()->findAll() as $item) {
			$result[$item->type][] = $item;
		}
		$this->render('index', [
			'items' => $result ?? [],
			'types' => CmsModuleServiceItems::$types
		]);
	}

	public function actionEdit()
	{
		$model = CmsModuleServiceItems::model()->findByPk(Yii::app()->request->getParam('item'));
		$model = $model ? $model : new CmsModuleServiceItems();
		if (Yii::app()->request->isPostRequest) {
			$model->setAttributes(Yii::app()->request->getPost('CmsModuleServiceItems'));
			$model->image = CUploadedFile::getInstance($model, 'image');
			if ($imageNameMain = Files::upload($model::IMAGES_DIR, 'image')) {
				if ($model->imageName) {
					Files::delete($model->imageName, $model::IMAGES_DIR);
				}
				$model->imageName = $imageNameMain;
			}
			$model->validate();
			if ($model->hasErrors() == false) {
				$model->save($runValidation = false);
				if(Yii::app()->request->getPost('Relations'))
					$model->saveRelations(Yii::app()->request->getPost('Relations'));
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				Yii::app()->controller->refresh();
			} else {
				Yii::app()->user->setFlash('failed', true);
			}
		}
		$this->render('edit', [
			'item' => $model,
			'types' => CmsModuleServiceItems::$types,
			'services' => CmsModuleService::model()->findAllByAttributes([], ['order' => 'service_name'])
		]);
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isAjaxRequest) {
			$data = Yii::app()->request->getPost('item');
			$oService = CmsModuleServiceItems::model()->findByPk($data);
			if($oService) {
				if ($oService->imageName) {
					Files::delete($oService->imageName, $oService::IMAGES_DIR);
				}
				$oService->delete();
			} else {
				throw new Exception('Cannot delete');
			}

		}
	}
}