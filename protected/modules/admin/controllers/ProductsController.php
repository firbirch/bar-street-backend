<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 10/01/2019
 * Time: 17:52
 */

class ProductsController extends AController
{
	public function actionIndex() {
		$oProducts = CmsShopProduct::model()->findAll();
		$this->render('index', [
			'products' => $oProducts,
		]);
	}

	public function actionShow($product) {
		$oProduct = CmsShopProduct::model();
		if($product) {
			$product = $oProduct->findByPk($product);
		} else {
			$product = $oProduct;
		}
		$this->render('show/index', [
			'product' => $product,
			'categories' => CmsShopCategory::model()->findAll()
		]);
	}

	public function actionSaveAll($productID) {

		if(Yii::app()->request->isPostRequest) {
			$data = Yii::app()->request->getPost('CmsShopProduct');

			$oProduct = CmsShopProduct::model();
			$oImage = CmsShopProductImages::model();
			if ($productID) {
				$product = $oProduct->findByPk($productID);
			} else {
				$product = $oProduct;
			}


			$dataImage = Yii::app()->request->getPost('image');


			if (!empty($dataImage['imageID'])) {
				foreach ($dataImage['imageID'] as $key => $imageID) {
					if (!$imageID) continue;

					$image = $oImage->findByPk($imageID);
					$image->image_alt = isset($dataImage['imageName'][$key]) ? $dataImage['imageName'][$key] : '-' ;
					$image->image_preview = 0;
					if (isset($dataImage['imagePreview'][$key]))
						$image->image_preview = 1;
					$image->save(false);
				}
			}

			$product->setAttributes($data);
			if($product->validate()) {
				$product->product_slug = $this->slugify($data['product_slug']);
				Yii::app()->user->setFlash('success', Yii::t('app', 'Changes have been successfully saved'));
				$product->save(false);
			}
			else
				print_r($product->getErrors());

			$this->render('show/index', [
				'product' => $product,
				'categories' => CmsShopCategory::model()->findAll()
			]);
		}
	}



	public function actionDeleteImage() {
		if(Yii::app()->request->isAjaxRequest) {
			$image_id = Yii::app()->request->getPost('imageID');
			if (!$image_id)
				throw new Exception('imageID empty');

			$oIBlockElementImages = CmsShopProductImages::model();
			$image = $oIBlockElementImages->findByPk($image_id);

			self::DeleteIBlockElementImages($image);

			$oIBlockElementImages->deleteByPk($image_id);
		}
	}

	private static function DeleteIBlockElementImages($element)
	{
		if ($element->image) {
			Files::delete('main_' . $element->image, $element::IMAGES_DIR);
			Files::delete($element->image, $element::IMAGES_DIR);
		}
		if (isset($element->cmsIblockElementImages))
			foreach ($element->cmsIblockElementImages as $image) {
				Files::delete('main_' . $image->image, $image::IMAGES_DIR);
				Files::delete($image->image, $image::IMAGES_DIR);
			}

	}


	public function actionUploadImage() {
		$oProductImages = CmsShopProductImages::model();
		if (Yii::app()->request->isPostRequest) {
			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$data = json_decode(Yii::app()->request->getPost('params'));
				if ($imageName = Files::upload($oProductImages::IMAGE_PATH, 'File')) {

					if (Files::copy($imageName, 'icon_' . $imageName, $oProductImages::IMAGE_PATH)) {
						Files::imageResize('icon_' . $imageName, $oProductImages::IMAGE_PATH, 100, 300);
					}
					if (Files::copy($imageName, 'main_' . $imageName, $oProductImages::IMAGE_PATH)) {
						Files::imageResize('main_' . $imageName, $oProductImages::IMAGE_PATH, 200, 400);
					}
				}
				$oProductImages->cms_shop_product_product_id = $data->product_id;
				$oProductImages->image_url = $imageName;
				$oProductImages->image_active = 1;
				$oProductImages->save();
			}
		}
	}
}