<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 09:54
 */

$this->pageTitle = 'Ингредиенты';
$this->breadcrumbs = [
	$this->pageTitle
];

?>

<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/coctails/ShowIng') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Товары</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Название</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<?foreach ($ings as $ing): ?>
							<tr class="pageItem">
								<td>
									<?=$ing->ingridient_name?>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/coctails/ShowIng', [
										'ing' => $ing->ingridient_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>
									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/coctails/removeIng',
										[
											'type' => 'POST',
											'data' => [
												'ing' => $ing->ingridient_id
											],
											'success' => 'js:$(this).closest(".pageItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Страница будет удалена'
										]
									); ?>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
