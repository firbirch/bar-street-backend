<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 09:56
 */

$this->pageTitle = $ing->ingridient_name ?: 'Новый ингредиент';
$this->breadcrumbs = [
	'Ингредиенты' => ['/admin/coctails/ing'],
	$this->pageTitle
];

?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>

				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>

				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/coctails/ingSaveAll', ['ingID' => $ing->ingridient_id ?: 0 ]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($ing) ?>
				<div class="widget-content">

					<div class="form-group">
						<label class="control-label">* Название:</label>
						<div class="input-width-xxlarge">
							<?= CHtml::activeTextField($ing, 'ingridient_name',
								array(
									'class' => 'form-control',
									'value' => $ing->ingridient_name
								)
							); ?>
						</div>
					</div>


				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>
