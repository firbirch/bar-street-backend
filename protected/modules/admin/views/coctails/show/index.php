<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/01/2019
 * Time: 12:52
 */
$this->pageTitle = $product->product_name ?: 'Новый коктель';
$this->breadcrumbs = [
	'Коктели' => ['/admin/coctails'],
	$this->pageTitle
];
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>

				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>

				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/coctails/SaveAll', ['productID' => $product->product_id ?: 0 ]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($product) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">

                            <li>
                                <a href="#box_tab5" class="btn btn-outline-secondary" data-toggle="tab">Ингредиенты</a>
                            </li>
                            <li>
                                <a href="#box_tab4" class="btn btn-outline-secondary" data-toggle="tab">Услуги</a>
                            </li>

<!--                            <li>-->
<!--                                <a href="#box_tab3" class="btn btn-outline-secondary" data-toggle="tab">SEO</a>-->
<!--                            </li>-->
							<li>
								<a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">Контент</a>
							</li>
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>
						<div class="tab-content ">
							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($product, 'product_active', [
												'checked' => $product->product_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">* Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($product, 'product_name',
											array(
												'class' => 'form-control',
												'value' => $product->product_name
											)
										); ?>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="control-label">Стоимость:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeNumberField($product, 'product_cost',
											array(
												'class' => 'form-control',
												'value' => $product->product_cost,
											)
										); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Сортировка:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeNumberField($product, 'product_sort',
											array(
												'class' => 'form-control',
												'value' => $product->product_sort,
											)
										); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Изображение: </label>
                                    <input type="file" name="MainImage" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled
                                               placeholder="<?= $product->product_image ? $product->product_image : 'Новый файл' ?>">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
                                                    type="button">Загрузить</button>
                                        </span>
                                    </div>
                                </div>

							</div>
                            <div class="tab-pane" id="box_tab2">

                                <div class="form-group">
                                    <label class="control-label">Алкоголь:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($product, 'product_alchogol',
											array(
												'class' => 'form-control',
												'value' => $product->product_alchogol,
											)
										); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Объем:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($product, 'product_volume',
											array(
												'class' => 'form-control',
												'value' => $product->product_volume,
											)
										); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Бокал:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($product, 'product_glass',
											array(
												'class' => 'form-control',
												'value' => $product->product_glass,
											)
										); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Украшение:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($product, 'product_decor',
											array(
												'class' => 'form-control',
												'value' => $product->product_decor,
											)
										); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Ингредиенты:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $product,
										'attribute' => 'product_content',
										'language' => 'ru',
									)); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Способ приготовления:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $product,
										'attribute' => 'product_method',
										'language' => 'ru',
									)); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Гарнир:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $product,
										'attribute' => 'product_sidish',
										'language' => 'ru',
									)); ?>
                                </div>
                            </div>
<!--                            <div class="tab-pane" id="box_tab3">-->
<!--                                <div class="form-group">-->
<!--                                    <label class="control-label">(SEO) Заголовок:</label>-->
<!--                                    <div class="input-width-xxlarge">-->
<!--										--><?//= CHtml::activeNumberField($product, 'product_seo_title',
//											array(
//												'class' => 'form-control',
//												'value' => $product->product_seo_title,
//											)
//										); ?>
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="form-group">-->
<!--                                    <label class="control-label">(SEO) Описание:</label>-->
<!--                                    <div class="input-width-xxlarge">-->
<!--										--><?//= CHtml::activeNumberField($product, 'product_seo_description',
//											array(
//												'class' => 'form-control',
//												'value' => $product->product_seo_description,
//											)
//										); ?>
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="tab-pane" id="box_tab4">

                                <div class="form-group">
                                    <label for="category">Услуги</label>
                                    <select name="Services[]" multiple style="height: 500px" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($services as $service):?>
                                            <option value="<?=$service->service_id?>" <?=in_array($service->service_id,$serviceIds) == true ? 'selected' : ''  ?> >
												<?=$service->service_name?>
                                            </option>
										<? endforeach; ?>
                                    </select>
                                </div>

                            </div>
                            <div class="tab-pane" id="box_tab5">
                                <div class="form-group">
                                    <label for="category">Ингредиенты</label>
                                    <select name="Ingridients[]" multiple style="height: 500px" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($ingredients as $ingredient):?>
                                            <option value="<?=$ingredient->ingridient_id?>" <?=in_array($ingredient->ingridient_id,$ingredientsIds) == true ? 'selected' : ''  ?> >
												<?=$ingredient->ingridient_name?>
                                            </option>
										<? endforeach; ?>
                                    </select>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>
