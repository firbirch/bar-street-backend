<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 16:40
 */

$this->pageTitle = $article->article_name ?: 'Новая статья';
$this->breadcrumbs = [
	'Статьи' => ['/admin/articles/'],
	$this->pageTitle
];
?>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/articles/SaveAll', ['article' => $article->article_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($article) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li>
								<a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">SEO</a>
							</li>
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>

						<div class="tab-content ">


							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($article, 'article_active', [
												'checked' => $article->article_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">* Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($article, 'article_name',
											array(
												'class' => 'form-control js-slugify',
												'value' => $article->article_name
											)
										); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Ссылка на статью:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($article, 'article_slug',
											array(
												'class' => 'form-control js-slugify-slug',
												'value' => $article->article_slug,
											)
										); ?>
									</div>
								</div>

                                <div class="form-group">
                                    <label class="control-label">Дата:</label>
                                    <div class="input-width-small">
										<?= CHtml::activeTextField($article, 'article_date',
											array(
												'class' => 'form-control datepicker',
												'value' => $article->article_date
											)
										); ?>
                                    </div>
                                </div>

								<div class="form-group">
									<label>Изображение: </label>
									<input type="file" name="MainImage" class="file-upload-default">
									<div class="input-group col-xs-12">
										<input type="text" class="form-control file-upload-info" disabled
											   placeholder="<?= $article->article_image ? $article->article_image : 'Новый файл' ?>">
										<span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
													type="button">Загрузить</button>
                                        </span>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Контент:</label>

									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $article,
										'attribute' => 'article_content',
										'language' => 'ru',
									)); ?>

								</div>
                                <div class="form-group">
                                    <label class="control-label">SS Лента:</label>

									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $article,
										'attribute' => 'article_rss',
										'language' => 'ru',
									)); ?>

                                </div>



							</div>

							<div class="tab-pane" id="box_tab2">
								<div class="form-group">
									<label class="control-label">(SEO) Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($article, 'article_seo_title',
											array(
												'class' => 'form-control',
												'value' => $article->article_seo_title
											)
										); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">(SEO) Описание:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($article, 'article_seo_description',
											array(
												'class' => 'form-control',
												'value' => $article->article_seo_description
											)
										); ?>
									</div>
								</div>
							</div>

						</div>


					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

