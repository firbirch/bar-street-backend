<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 16:33
 */

$this->pageTitle = 'Статьи';
$this->breadcrumbs = [
	$this->pageTitle
];

?>


<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/articles/show') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Товары</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Название</th>
							<th>Дата</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<?foreach ($articles as $article):?>
							<tr class="articleItem">
								<td>
									<?=$article->article_name?>
								</td>
								<td>
									<?=$article->article_date?>
								</td>
								<td>
									<label class="badge badge-<?= $article->article_active ? 'success' : 'danger' ?>"><?= $article->article_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/articles/show', [
										'article' => $article->article_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>
									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/articles/delete',
										[
											'type' => 'POST',
											'data' => [
												'article' => $article->article_id
											],
											'success' => 'js:$(this).closest(".articleItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Статья будет удалена'
										]
									); ?>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
