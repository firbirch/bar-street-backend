<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 20/12/2018
 * Time: 13:07
 */
$this->pageTitle = isset($gallery->name) ? $gallery->name : 'Новая Галерея';
$this->breadcrumbs = [
	'Галерея' => ['/admin/gallery'],
	$this->pageTitle
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= $this->pageTitle ?></h4>

				<? if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success show">
                        <i class="icon-remove close" data-dismiss="alert"></i>
                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                    </div>
				<? endif; ?>

				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/gallery/SaveAll', ['galleryID' => $gallery->id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($gallery) ?>
                <div class="widget-content">


                    <div class="widget-content">
                        <div class="tabbable box-tabs">

                            <ul class="nav nav-tabs btn-group box-tabs-left">
	                            <li>
		                            <a href="#eng_tab" class="btn btn-outline-secondary" data-toggle="tab">ENG</a>
	                            </li>
                                <li>
                                    <a href="#box_tab5" class="btn btn-outline-secondary" data-toggle="tab">SEO</a>
                                </li>
                                <li>
                                    <a href="#box_tab4" class="btn btn-outline-secondary" data-toggle="tab">Услуги</a>
                                </li>
                                <li>
                                    <a href="#box_tab3" class="btn btn-outline-secondary"
                                       data-toggle="tab">Изображения</a>
                                </li>
                                <li>
                                    <a href="#box_tab6" class="btn btn-outline-secondary" data-toggle="tab">Карта</a>
                                </li>
                                <li>
                                    <a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">Анонс</a>
                                </li>
                                <li class="active">
                                    <a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная
                                        часть</a>
                                </li>
                            </ul>


                            <div class="tab-content">

								<? if (Yii::app()->user->hasFlash('success')): ?>
                                    <div class="alert alert-success fade in">
                                        <i class="icon-remove close" data-dismiss="alert"></i>
                                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                                    </div>
								<? endif; ?>


	                            <div class="tab-pane" id="eng_tab">

		                            <div class="form-group">

			                            <div class="form-group">
				                            <div class="form-check form-check-flat">
					                            <label class="form-check-label">
						                            <?= CHtml::activeCheckBox($gallery, 'g_active_eng', [
							                            'checked' => $gallery->g_active_eng ? ('checked') : (''),
							                            'class' => 'form-check-input'
						                            ]); ?>
						                            Активность
					                            </label>
				                            </div>
			                            </div>

			                            <div class="form-group">
				                            <label class="control-label">* Заголовок:</label>
				                            <div class="input-width-xxlarge">
					                            <?= CHtml::activeTextField($gallery, 'name_eng',
						                            array(
							                            'class' => 'form-control',
							                            'value' => $gallery->name_eng
						                            )
					                            ); ?>
				                            </div>
			                            </div>

			                            <div class="form-group">
				                            <label class="control-label">Название места (Eng):</label>
				                            <div class="input-width-large">
					                            <?= CHtml::activeTextField($gallery, 'map_name_en',
						                            array(
							                            'class' => 'form-control',
							                            'value' => $gallery->map_name_en
						                            )
					                            ); ?>
				                            </div>
			                            </div>

			                            <div class="form-group">
				                            <label>Изображение: </label>
				                            <input type="file" name="AnnounceImageEng" class="file-upload-default">
				                            <div class="input-group col-xs-12">
					                            <input type="text" class="form-control file-upload-info" disabled
					                                   placeholder="<?= $gallery->announce_image_eng ? $gallery->announce_image_eng : 'Новый файл' ?>">
					                            <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
                                                    type="button">Загрузить</button>
                                        </span>
				                            </div>
			                            </div>

		                            </div>
	                            </div>

                                <div class="tab-pane active" id="box_tab1">

                                    <div class="form-group">
                                        <div class="form-check form-check-flat">
                                            <label class="form-check-label">
												<?= CHtml::activeCheckBox($gallery, 'g_active', [
													'checked' => $gallery->g_active ? ('checked') : (''),
													'class' => 'form-check-input'
												]); ?>
                                                Активность
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">* Заголовок:</label>
                                        <div class="input-width-xxlarge">
											<?= CHtml::activeTextField($gallery, 'name',
												array(
													'class' => 'form-control',
													'value' => $gallery->name
												)
											); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Дата:</label>
                                        <div class="input-width-large">
											<?= CHtml::activeTextField($gallery, 'date',
												array(
													'class' => 'form-control datepicker',
													'value' => $gallery->date
												)
											); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Контент:</label>

										<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
											'model' => $gallery,
											'attribute' => 'content',
											'language' => 'ru',
										)); ?>
                                    </div>

                                </div>
                                <div class="tab-pane" id="box_tab2">
                                    <div class="form-group">

                                        <div class="form-group">
                                            <label>Изображение: </label>
                                            <input type="file" name="AnnounceImage" class="file-upload-default">
                                            <div class="input-group col-xs-12">
                                                <input type="text" class="form-control file-upload-info" disabled
                                                       placeholder="<?= $gallery->announce_image ? $gallery->announce_image : 'Новый файл' ?>">
                                                <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
                                                    type="button">Загрузить</button>
                                        </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Контент:</label>

											<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
												'model' => $gallery,
												'attribute' => 'announce_content',
												'language' => 'ru',
											)); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="box_tab4">

                                    <div class="form-group">
                                        <select name="GalleryService[]" style="height: 300px; margin-top: 25px" multiple
                                                class="form-control form-control-sm" id="exampleFormControlSelect3">
											<? foreach ($services as $service): ?>
                                                <option value="<?= $service->service_id ?>" <?= in_array($service->service_id, $servicesId) == true ? 'selected' : '' ?> >
													<?= $service->service_name ?>
                                                </option>
											<? endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="tab-pane" id="box_tab5">

                                    <div class="form-group">
                                        <label class="control-label">(SEO) Заголовок:</label>
                                        <div class="input-width-large">
											<?= CHtml::activeTextField($gallery, 'seo_title',
												array(
													'class' => 'form-control',
													'value' => $gallery->seo_title
												)
											); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">(SEO) Описание:</label>
                                        <div class="input-width-large">
											<?= CHtml::activeTextField($gallery, 'seo_description',
												array(
													'class' => 'form-control',
													'value' => $gallery->seo_description
												)
											); ?>
                                        </div>
                                    </div>
                                </div>




                                <div class="tab-pane" id="box_tab6">
                                    <div class="form-group">
                                        <label class="control-label">Название места:</label>
                                        <div class="input-width-large">
											<?= CHtml::activeTextField($gallery, 'map_name',
												array(
													'class' => 'form-control',
													'value' => $gallery->map_name
												)
											); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Lat:</label>
                                        <div class="input-width-large">
											<?= CHtml::activeTextField($gallery, 'lat',
												array(
													'class' => 'form-control',
													'value' => $gallery->lat
												)
											); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Lon:</label>
                                        <div class="input-width-large">
											<?= CHtml::activeTextField($gallery, 'lon',
												array(
													'class' => 'form-control',
													'value' => $gallery->lon
												)
											); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="box_tab3">
                                    <div class="ui-sortable gallery" style="display: flex; flex-wrap: wrap;">
										<? foreach ($gallery->cmsModuleGalleryImagesWithSort as $key => $image): ?>
                                            <div class="productPreviewImageItem" style="background-color: white">
                                                <a data-fancybox="gallery"
                                                   href="<?= $image->getImage(null, true) ?>">
                                                    <img height="200" width="200%" style="object-fit: contain" src="<?= $image->getImage('icon_', true) ?>">
                                                </a>
                                                <input type="hidden" name="image[imageID][<?= $key ?>]"
                                                       value="<?= $image['id'] ?>">
                                                <div class="productPreviewImage__content form-group">
                                                    <input type="text" class="input-width-large" name="image[imageAlt][<?= $key ?>]"
                                                           placeholder="Alt"
                                                           value="<?= $image->imageAlt ?>">
                                                    <input type="text" class="input-width-large" name="image[imageName][<?= $key ?>]"
                                                           placeholder="Image"
                                                           value="<?= $image->imageTitle ?>">

                                                    <input type="hidden" name="sort[]" value="<?= $image['id'] ?>">

													<?= CHtml::ajaxLink('Удалить',
														Yii::app()->createUrl('/admin/gallery/DeleteImage'),
														[
															'type' => 'POST',
															'data' => [
																'imageID' => $image->id
															],
															'success' => 'js:$(this).closest(".productPreviewImageItem").remove()'
														],
														[
															'class' => 'removeLink'
														]);
													?>
                                                </div>
                                            </div>
										<? endforeach; ?>
                                    </div>
                                    <div class="form-group">
                                        <div id="dropzone_profile_photo"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group" style="margin-top: 25px">
                        <div class="col-md-10">
							<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
                        </div>
                    </div>
					<?= CHtml::endForm() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        let myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/gallery/UploadImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            let data = JSON.stringify({
                gallery_id: <?=$gallery->id?>,
            });
            formData.append("params", data);
        });
    });
</script>


