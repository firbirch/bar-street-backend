<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:20
 * CmsModuleServiceItems $item CmsModuleServiceItems
 */

$this->pageTitle = $item->title ?: 'Новый элемент';
$this->breadcrumbs = [
	'Элементы каталога' => ['/admin/servicesItems/'],
	$this->pageTitle
];
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/servicesItems/edit', ['item' => $item->id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($item) ?>

				<div class="form-group">
					<div class="form-check form-check-flat">
						<label class="form-check-label">
							<?= CHtml::activeCheckBox($item, 'active', [
								'checked' => $item->active ? ('checked') : (''),
								'class' => 'form-check-input'
							]); ?>
							Активность
						</label>
					</div>
				</div>
				<div class="jsCoreTranslate">
					<div class="form-group">
						<label class="control-label">* Заголовок:</label>
						<div class="input-width-xxlarge">
							<?= CHtml::activeTextField($item, 'title',
								array(
									'class' => 'form-control name js-slugify',
									'value' => $item->title
								)
							); ?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label">Позиция:</label>
					<div class="input-width-xxlarge">
						<?= CHtml::activeTextField($item, 'position',
							array(
								'class' => 'form-control',
								'value' => $item->position
							)
						); ?>
					</div>
				</div>

				<div class="form-group">
					<label>Изображение: </label>
					<input type="file" name="image" class="file-upload-default">
					<div class="input-group col-xs-12">
						<input type="text" name="CmsModuleServiceItems['image']" class="form-control file-upload-info" disabled
						       placeholder="<?= $item->image ? $item->image : 'Новый файл' ?>">
						<span class="input-group-append">
                            <button class="file-upload-browse btn btn-info"
                                    type="button">Загрузить</button>
                        </span>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label">Цена:</label>
					<div class="input-width-xxlarge">
						<?= CHtml::activeTextField($item, 'price',
							array(
								'class' => 'form-control',
								'value' => $item->price
							)
						); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label">Описание:</label>
					<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
						'model' => $item,
						'attribute' => 'description',
						'language' => 'ru',
					)); ?>
				</div>

				<div class="form-group">
					<label for="category">Тип:</label>
					<select name="CmsModuleServiceItems[type]" class="form-control form-control-sm" id="exampleFormControlSelect3">
						<? foreach($types as $typeVal => $typeName):?>
							<option value="<?=$typeVal?>" <?=$item->type == $typeVal ? 'selected' : ''?>>
								<?=$typeName?>
							</option>
						<? endforeach; ?>
					</select>
				</div>

				<div class="form-group">
					<label for="category">Показывать в услугах</label>
					<select name="Relations[]" multiple style="height: 250px" class="form-control form-control-sm" id="exampleFormControlSelect3">
						<? foreach($services as $service):?>
							<option value="<?=$service->service_id?>" <?=in_array($service->service_id, $item->getUsedServices()) == true ? 'selected' : ''  ?> >
								<?=$service->service_name?>
							</option>
						<? endforeach; ?>
					</select>
				</div>

				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

