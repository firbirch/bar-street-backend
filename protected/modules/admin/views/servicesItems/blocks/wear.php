<div class="tab-pane active" id="wear">
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th>Заголовок</th>
					<th>Активность</th>
					<th>Позиция</th>
					<th>Редактировать</th>
					<th>Удалить</th>
				</tr>
			</thead>
			<tbody>

				<? foreach ($items as $item): ?>
					<tr class="labelItem">
						<td><?= $item->title ?></td>
						<td>
							<label class="badge badge-<?= $item->active ? 'success' : 'danger' ?>"><?= $item->active ? 'Да' : 'Нет' ?></label>
						</td>
						<td><?= $item->position ?></td>
						<td>
							<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/servicesItems/edit', [
								'item' => $item->id
							]) ?>">
								<i class="mdi mdi-eye text-primary"></i>
							</a>
						</td>
						<td>
							<?= CHtml::ajaxLink(
								'<i class="mdi mdi-close text-danger"></i>',
								'/admin/servicesItems/delete',
								[
									'type' => 'POST',
									'data' => [
										'item' => $item->id
									],
									'success' => 'js:$(this).closest(".labelItem").remove()'
								],
								[
									'class' => 'btn btn-light',
									'confirm' => 'Страница будет удалена'
								]
							); ?>
						</td>
					</tr>
				<? endforeach; ?>
			</tbody>
		</table>
	</div>
</div>