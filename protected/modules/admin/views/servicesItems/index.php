<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:01
 */

$this->pageTitle = 'Элементы каталога';
$this->breadcrumbs = [
	$this->pageTitle
];

?>

<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/servicesItems/edit') ?>">Добавить</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-right">
							<? $i=0;foreach ($types as $typeVal => $typeName): ?>
								<li class="<?if($i==0):?> active <?endif;$i++?>">
									<a href="#<?=$typeVal?>"  class="btn btn-outline-secondary" data-toggle="tab"><?=$typeName?></a>
								</li>
							<? endforeach; ?>
						</ul>
						<div class="tab-content ">
							<? $i=0;foreach ($types as $typeVal => $typeName): ?>
								<? $this->renderPartial('blocks/table', ['items' => @$items[$typeVal] ?? [], 'type' => $typeVal, 'active' => $i==0 ? 'active' : '']);$i++; ?>
							<? endforeach; ?>
						</div>
					</div>
				</div>



























			</div>
		</div>
	</div>
</div>

