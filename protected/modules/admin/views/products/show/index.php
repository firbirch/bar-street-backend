<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/01/2019
 * Time: 12:52
 */
$this->pageTitle = $product->product_name ?: 'Новый товар';
$this->breadcrumbs = [
	'Товары' => ['/admin/products'],
	$this->pageTitle
];
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $product->product_name ?: 'Новая страница' ?></h4>

				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>

				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/products/SaveAll', ['productID' => $product->product_id ?: 0 ]), 'POST', ['class' => 'form-horizontal row-border']) ?>
				<?= CHtml::errorSummary($product) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
                            <li>
                                <a href="#box_tab3" class="btn btn-outline-secondary" data-toggle="tab">Изображения</a>
                            </li>
							<li>
								<a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">Контент</a>
							</li>
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>
						<div class="tab-content ">
							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($product, 'product_active', [
												'checked' => $product->product_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">* Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($product, 'product_name',
											array(
												'class' => 'form-control',
												'value' => $product->product_name
											)
										); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Ссылка на товара:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($product, 'product_slug',
											array(
												'class' => 'form-control',
												'value' => $product->product_slug,
											)
										); ?>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="control-label">Стоимость:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeNumberField($product, 'product_cost',
											array(
												'class' => 'form-control',
												'value' => $product->product_cost,
											)
										); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="category">Категория</label>
                                    <select name="CmsShopProduct[cms_shop_category_category_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
                                        <? foreach($categories as $category):?>

                                            <option value="<?=$category->category_id?>" <?=$category->category_id == $product->cmsShopCategoryCategory->category_id ? 'selected' : ''?>>
                                                <?=$category->category_name?>
                                            </option>

                                        <? endforeach; ?>
                                    </select>
                                </div>
							</div>
                            <div class="tab-pane" id="box_tab2">
                                <div class="form-group">
                                    <label class="control-label">Анонс:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $product,
										'attribute' => 'product_preview_content',
										'language' => 'ru',
									)); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Описание:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $product,
										'attribute' => 'product_content',
										'language' => 'ru',
									)); ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="box_tab3">
                                <div class="" style="display: flex; flex-wrap: wrap; margin-top: 25px;">
									<? foreach ($product->cmsShopProductImages as $key => $image): ?>
                                        <div class="productPreviewImageItem">
                                            <a data-fancybox="gallery"
                                               href="<?= $image->getImage() ?>">
                                                <img src="<?= $image->getImage('main_') ?>">
                                            </a>
                                            <input type="hidden" name="image[imageID][<?= $key ?>]"
                                                   value="<?= $image['image_id'] ?>">

                                            <div class="productPreviewImage__preview">
                                                <div class="form-check form-check-flat">
                                                    <label class="form-check-label">
                                                        <input type="checkbox" class="form-check-input" id="image[imagePreview][<?= $key ?>]"
                                                               name="image[imagePreview][<?= $key ?>]"
                                                            <?= $image['image_preview'] ? 'checked="checked"' : '' ?>
                                                        >
                                                    </label>
                                                    На главной
                                                </div>
                                            </div>

                                            <div class="productPreviewImage__content">
                                                <input type="text" name="image[imageName][<?= $key ?>]"
                                                       placeholder="Название изображения"
                                                       value="<?= $image->image_alt ?>">
												<?= CHtml::ajaxLink('Удалить',
													Yii::app()->createUrl('/admin/products/DeleteImage'),
													[
														'type' => 'POST',
														'data' => [
															'imageID' => $image->image_id
														],
														'success' => 'js:$(this).closest(".productPreviewImageItem").remove()'
													],
													[
														'class' => 'removeLink'
													]);
												?>
                                            </div>
                                        </div>
									<? endforeach; ?>
                                </div>
                                <div class="form-group">
                                    <div id="dropzone_profile_photo"></div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    jQuery(function ($) {
        let myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/products/UploadImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            let data = JSON.stringify({
                product_id: <?=$product->product_id?>,
            });
            formData.append("params", data);
        });
    });
</script>
