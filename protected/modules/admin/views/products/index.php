<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 10/01/2019
 * Time: 17:57
 */
$this->pageTitle = 'Товары';
$this->breadcrumbs = [
	'Товары'
];
?>


<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/page/show') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Товары</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Изображение</th>
							<th>Название товара</th>
							<th>Стоимость</th>
							<th>Сортировка</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<?foreach ($products as $product):?>
							<tr class="pageItem">
								<td>
									<img src = "<?=$product->getImage(null, true)?>" style="object-fit: cover">
								</td>
								<td>
									<?=$product->product_name?>
								</td>
								<td>
									<?=$this->format_price($product->product_cost)?> руб.
								</td>
								<td class="clickSort" data-product="<?= $product->id ?>">
									<span><?= $product->product_sort ?: '-' ?></span>
								</td>
								<td>
									<label class="badge badge-<?= $product->product_active ? 'success' : 'danger' ?>"><?= $product->product_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-outline-secondary" href="<?= Yii::app()->createURL('/admin/products/show', [
										'product' => $product->product_id
									]) ?>">
										<i class="mdi mdi-tooltip-edit"></i>
									</a>
								</td>
								<td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-bookmark-remove"></i>',
										'/admin/page/remove',
										[
											'type' => 'POST',
											'data' => [
												'page' => $product->product_id
											],
											'success' => 'js:$(this).closest(".pageItem").remove()'
										],
										[
											'class' => 'btn btn-outline-secondary',
											'confirm' => 'Страница будет удалена'
										]
									); ?>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
