<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 15:13
 */

$this->pageTitle = 'Сотрудники';
$this->breadcrumbs = [
	$this->pageTitle
];
?>

<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/employee/show') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Сотрудники</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Имя</th>
                            <th>Должность</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<?foreach ($employies as $employee):?>
							<tr class="reviewItem">
								<td>
									<?=$employee->employee_name?>
								</td>
                                <td>
									<?=$employee->employee_rang?>
                                </td>
								<td>
									<label class="badge badge-<?= $employee->employee_active ? 'success' : 'danger' ?>"><?= $employee->employee_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/employee/show', [
										'employee' => $employee->employee_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/employee/delete',
										[
											'type' => 'POST',
											'data' => [
												'employee' => $employee->employee_id
											],
											'success' => 'js:$(this).closest(".reviewItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Сотрудник будет удален'
										]
									); ?>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

