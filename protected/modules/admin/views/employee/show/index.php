<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 16:40
 */

$this->pageTitle = $employee->employee_name ?: 'Новый сотрудник';
$this->breadcrumbs = [
	'Сотрудники' => ['/admin/employee/'],
	$this->pageTitle
];
?>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/employee/SaveAll', ['employee' => $employee->employee_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($employee) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>

						<div class="tab-content ">

							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($employee, 'employee_active', [
												'checked' => $employee->employee_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">* Автор:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($employee, 'employee_name',
											array(
												'class' => 'form-control',
												'value' => $employee->employee_name
											)
										); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">* Должность:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($employee, 'employee_rang',
											array(
												'class' => 'form-control',
												'value' => $employee->employee_rang,
											)
										); ?>
									</div>
								</div>

                                <div class="form-group">
                                    <label>Иконка: </label>
                                    <input type="file" name="MainImage" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled
                                               placeholder="<?= $employee->employee_icon ? $employee->employee_icon : 'Новый файл' ?>">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
                                                    type="button">Загрузить</button>
                                        </span>
                                    </div>
                                </div>

							</div>


						</div>


					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

