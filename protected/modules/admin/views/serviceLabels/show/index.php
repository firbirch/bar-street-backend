<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 11:35
 */

$this->pageTitle = $label->label ?: 'Новый лейбл';
$this->breadcrumbs = [
	'Лейблы' => ['/admin/serviceLabels'],
	$this->pageTitle
];

?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success show">
                        <i class="icon-remove close" data-dismiss="alert"></i>
                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                    </div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/serviceLabels/SaveAll', ['label' => $label->id ?: 0]), 'POST', ['class' => 'form-horizontal row-border']) ?>
				<?= CHtml::errorSummary($label) ?>
                <div class="widget-content">
                    <div class="tabbable box-tabs">
                        <ul class="nav nav-tabs btn-group box-tabs-left">
                            <li class="active">
                                <a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная
                                    часть</a>
                            </li>
                        </ul>

                        <div class="tab-content ">


                            <div class="tab-pane active" id="box_tab1">
<!--                                <div class="form-group">-->
<!--                                    <div class="form-check form-check-flat">-->
<!--                                        <label class="form-check-label">-->
<!--											--><?//= CHtml::activeCheckBox($label, 'label_active', [
//												'checked' => $label->label_active ? ('checked') : (''),
//												'class' => 'form-check-input'
//											]); ?>
<!--                                            Активность-->
<!--                                        </label>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="form-group">
                                    <label class="control-label">* Заголовок:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($label, 'label',
											array(
												'class' => 'form-control',
												'value' => $label->label
											)
										); ?>
                                    </div>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label class="control-label">CSS свойство:</label>-->
<!--                                    <div class="input-width-xxlarge">-->
<!--										--><?//= CHtml::activeTextField($label, 'label_css',
//											array(
//												'class' => 'form-control',
//												'value' => $label->label_css,
//											)
//										); ?>
<!--                                    </div>-->
<!--                                </div>-->

                                <div class="form-group">
                                    <div class="asColorPicker-wrap">
                                        <input type="text" name="CmsModuleServiceLabels[hex_color]" data-colorpicker = "colorpicker" class="color-picker" value="<?=$label->hex_color?>">
                                    </div>

                                </div>
                            </div>


                        </div>


                    </div>
                </div>
                <div class="row form-group" style="margin-top: 25px">
                    <div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
                    </div>
                </div>
				<?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>

