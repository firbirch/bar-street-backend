<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 11:35
 */

$this->pageTitle = 'Лейблы';
$this->breadcrumbs = [
	'Лейблы'
];
?>
<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/serviceLabels/show') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Страницы</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Заголовок</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<tbody>

						<? foreach ($labels as $label): ?>
							<tr class="labelItem">
								<td><?= $label->label ?></td>
<!--								<td>-->
<!--									<label class="badge badge---><?//= $label->label_active ? 'success' : 'danger' ?><!--">--><?//= $label->label_active ? 'Да' : 'Нет' ?><!--</label>-->
<!--								</td>-->
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/serviceLabels/show', [
										'label' => $label->id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/serviceLabels/remove',
										[
											'type' => 'POST',
											'data' => [
												'id' => $label->id
											],
											'success' => 'js:$(this).closest(".labelItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Страница будет удалена'
										]
									); ?>
								</td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
