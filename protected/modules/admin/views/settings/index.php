<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 10/01/2019
 * Time: 16:21
 */


$this->pageTitle = 'Основные настройки';
$this->breadcrumbs = [
	'Основные настройки'
];
?>

<? if (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success show">
        <i class="icon-remove close" data-dismiss="alert"></i>
        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
    </div>
<? endif; ?>

<div class="row">
    <div class="col-md-12">
		<?= CHtml::beginForm(Yii::app()->createUrl('/admin/settings/SaveAll'), 'POST', ['class' => 'form-horizontal row-border']) ?>
		<?= CHtml::errorSummary($settingParams) ?>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
					<?foreach ($settings as $setting):?>

						<?if($setting->field_type == 1 || !$setting->field_type):?>
                            <div class="form-group">
                                <div class="control-label"><?=$setting->field_description?>:</div>
                                <div class="input-width-xxlarge" >
                                    <input class="form-control" style="width: 33%; display: inline-block;" value="<?=$setting->field_value?>" name="CurrentSetting[<?=$setting->id?>]">
                                    <a  class="btn btn-outline-danger" onclick="if(!confirm('Вы действительно хотите удалить?')) return false;" href = "<?=Yii::app()->createUrl('/admin/settings/remove', ['field_id' => $setting->id])?>">
                                        <i class="mdi mdi-bookmark-remove"></i>
                                    </a>
                                </div>
                            </div>
						<?elseif($setting->field_type == 2):?>
                            <div class="form-group">
                                <div class="control-label"><?=$setting->field_description?>:</div>
                                <div class="input-width-xxlarge" >
                                    <textarea class="form-control" style="width: 33%; height: 250px; display: inline-block;" name="CurrentSetting[<?=$setting->id?>]"><?=$setting->field_value?></textarea>
                                </div>
                            </div>
						<?
						endif;
					endforeach;?>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <label>Новый параметр</label>
                    <div class="form-group" style="display: flex; justify-content: space-between">

						<?= CHtml::activeTextField($settingParams, 'field_name',
							array(
								'class' => 'form-control',
								'placeholder' => 'Название параметра',
								'style' => 'width: 33%'
							)
						); ?>
						<?= CHtml::activeTextField($settingParams, 'field_description',
							array(
								'class' => 'form-control',
								'placeholder' => 'Описание параметра',
								'style' => 'width: 33%'
							)
						); ?>
						<?= CHtml::activeTextField($settingParams, 'field_value',
							array(
								'class' => 'form-control',
								'placeholder' => 'Значение параметра',
								'style' => 'width: 33%'
							)
						); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-group" style="margin-top: 25px">
            <div class="col-md-10">
				<?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
            </div>
        </div>
		<?= CHtml::endForm() ?>
    </div>
</div>
