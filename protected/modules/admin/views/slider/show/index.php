<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 19/12/2018
 * Time: 18:11
 */
$this->pageTitle = isset($slider->name) ? $slider->name : 'Новый слайдер';
$this->breadcrumbs = [
	'Слайдеры' => ['/admin/slider'],
	$this->pageTitle
];

?>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"> <?= isset($slider->name) ? $slider->name : 'Новая слайдер' ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success show">
                        <i class="icon-remove close" data-dismiss="alert"></i>
                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                    </div>
				<? endif; ?>

				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/slider/SaveAll', ['sliderID' => isset($slider->id) ? $slider->id : 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($slider) ?>


                <div class="widget-content">
                    <div class="tabbable box-tabs">
                        <ul class="nav nav-tabs btn-group box-tabs-left">
                            <li><a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">Изображение</a>
                            </li>
                            <li class="active"><a class="btn btn-outline-secondary" href="#box_tab1"
                                                  data-toggle="tab">Основная часть</a></li>
                        </ul>

                        <div class="tab-content">



                            <div class="tab-pane active" id="box_tab1">


                                <div class="form-group">
                                    <div class="form-check form-check-flat">
                                        <label class="form-check-label">
											<?= CHtml::activeCheckBox($slider, 'active', [
												'checked' => $slider->active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
                                            Активность
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label">* Заголовок:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($slider, 'name',
											array(
												'class' => 'form-control',
												'value' => $slider->name
											)
										); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Ссылка:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($slider, 'slug',
											array(
												'class' => 'form-control',
												'value' => $slider->slug
											)
										); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">CSS свойство:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($slider, 'css_class',
											array(
												'class' => 'form-control',
												'value' => $slider->css_class
											)
										); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Контент:</label>
                                    <div class="">
										<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
											'model' => $slider,
											'attribute' => 'context',
											'language' => 'ru',
										)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="box_tab2">

								<? if ($slider->image): ?>
                                    <div class="form-group">

                                        <div class="productPreviewImageItem col-md-2">
                                            <a data-fancybox="" href="<?= $slider->getImage() ?>">
                                                <img src="<?= $slider->getImage() ?>">
                                            </a>
                                        </div>

                                    </div>

								<? endif; ?>

                                <div class="form-group">
                                    <label>Изображение: </label>
                                    <input type="file" name="MainImage" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled
                                               placeholder="<?= $slider->image ? $slider->image : 'Новый файл' ?>">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
                                                    type="button">Загрузить</button>
                                        </span>
                                    </div>
                                </div>



								<? if ($slider->mobile_image): ?>
                                    <div class="form-group">

                                        <div class="productPreviewImageItem col-md-2">
                                            <a data-fancybox="" href="<?= $slider->getMobileImage() ?>">
                                                <img src="<?= $slider->getMobileImage() ?>">
                                            </a>
                                        </div>

                                    </div>

								<? endif; ?>

                                <div class="form-group">
                                    <label>Мобильное изображение: </label>
                                    <input type="file" name="MobileImage" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled
                                               placeholder="<?= $slider->mobile_image ? $slider->mobile_image : 'Новый файл' ?>">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
                                                    type="button">Загрузить</button>
                                        </span>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group" style="margin-top: 25px">
                    <div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>

						<?= CHtml::submitButton('Применить и выйти', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
                    </div>
                </div>
				<?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>


