<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 19/12/2018
 * Time: 17:58
 */

$this->pageTitle = 'Слайдер';
$this->breadcrumbs = [
	$this->pageTitle
];
?>


<div class="row" style="margin-bottom: 25px;">
    <div class="col-md-12" style="display: flex; justify-content: flex-end">
        <a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/slider/show') ?>">Добавить</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Слайдер</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Заголовок</th>
                            <th>Сортировка <i class="icon-sort"></i></a></th>
                            <th>Активность</th>
                            <th>Редактировать</th>
                            <th>Удалить</th>
                        </tr>
                        </thead>
                        <tbody>

						<? foreach ($sliders as $item): ?>
                            <tr class="sliderItem">
                                <td><?= $item->name ?></td>
                                <td class="clickSort" data-slider="<?= $item->id ?>">
                                    <span><?= $item->sort ?: '-' ?></span>
                                </td>
                                <td>
                                    <span class="badge badge-<?= $item->active ? 'success' : 'danger' ?>"><?= $item->active ? 'Да' : 'Нет' ?></span>
                                </td>
                                <td>
                                    <a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/slider/show', [
										'sliderID' => $item->id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
                                    </a>
                                </td>
                                <td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/slider/remove',
										[
											'type' => 'POST',
											'data' => [
												'page' => $item->id
											],
											'success' => 'js:$(this).closest(".sliderItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Слайдер будет удален'
										]
									); ?>
                                </td>
                            </tr>
						<? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.clickSort').on('dblclick', function () {
            if ($(this).hasClass('active')) return true;
            let content = $(this).find('span');
            let data = content.html();
            content.remove();
            $(this).append('<input class = "sortInput form-control input-width-small" type = "text" value = "' + data + '">').addClass('active');
            $(this).find('.sortInput').focus();
            return false;
        });
    });
    $(document).on('blur', '.sortInput', function () {
        let data = $(this).val();
        let sliderID = $(this).closest('.clickSort').data('slider');
        if (data)
            $.ajax({
                url: '/admin/slider/ChangeSort',
                type: 'POST',
                data: {
                    sliderId: sliderID,
                    sliderSort: data
                },
                success: function (request) {

                }
            });
        $(this).closest('.clickSort').append('<span>' + data + '</span>').removeClass('active');
        $(this).remove();
    });
</script>