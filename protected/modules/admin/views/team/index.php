<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 20/12/2018
 * Time: 13:07
 */

$this->pageTitle = 'Мероприятия';
$this->breadcrumbs = [
	$this->pageTitle
];
?>


<div class="row" style = "margin-bottom: 25px;">
	<div class="col-md-12" style = "display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?=Yii::app()->createURL('/admin/team/show')?>">Добавить</a>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Мероприятия</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Автор</th>
                            <th>Активность</th>
                            <th>Редактировать</th>
                            <th>Удалить</th>
                        </tr>
                        </thead>
					<tbody>

					<? foreach ($events as $event): ?>
						<tr class="galleryItem">
							<td><?= $event->event_author ?></td>
                            <td>
                                <label class="badge badge-<?= $event->event_active ? 'success' : 'danger' ?>"><?= $event->event_active ? 'Да' : 'Нет' ?></label>
                            </td>
							<td>

                                <a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/team/show', [
									'event' => $event->event_id
								]) ?>">
                                    <i class="mdi mdi-eye text-primary"></i>
                                </a>

							</td>
							<td>

								<?= CHtml::ajaxLink(
									'<i class="mdi mdi-close text-danger"></i>',
									'/admin/event/remove',
									[
										'type' => 'POST',
										'data' => [
											'page' => $event->event_id
										],
										'success' => 'js:$(this).closest(".galleryItem").remove()'
									],
									[
										'class' => 'btn btn-light',
										'confirm' => 'Мероприятие будет удалено'
									]
								); ?>

							</td>
						</tr>
					<? endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.clickSort').on('dblclick', function () {
            if ($(this).hasClass('active')) return true;
            let content = $(this).find('span');
            let data = content.html();
            content.remove();
            $(this).append('<input class = "sortInput form-control input-width-small" type = "text" value = "' + data + '">').addClass('active');
            $(this).find('.sortInput').focus();
            return false;
        });
    });
    $(document).on('blur', '.sortInput', function () {
        let data = $(this).val();
        let pageID = $(this).closest('.clickSort').data('page');
        if (data)
            $.ajax({
                url: '/admin/page/ChangeSort',
                type: 'POST',
                data: {
                    pageId: pageID,
                    pageSort: data
                },
                success: function (request) {

                }
            });
        $(this).closest('.clickSort').append('<span>' + data + '</span>').removeClass('active');
        $(this).remove();
    });
</script>
