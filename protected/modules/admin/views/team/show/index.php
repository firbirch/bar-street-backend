<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 20/12/2018
 * Time: 13:07
 */
$this->pageTitle = isset($event->event_author) ? $event->event_author : 'Новая мероприятие';
$this->breadcrumbs = [
	'Мероприятия' => ['/admin/team'],
	$this->pageTitle
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= $this->pageTitle ?></h4>

				<? if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success show">
                        <i class="icon-remove close" data-dismiss="alert"></i>
                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                    </div>
				<? endif; ?>

				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/team/SaveAll', ['event' => $event->event_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($event) ?>
                <div class="widget-content">


                    <div class="widget-content">
                        <div class="tabbable box-tabs">

                            <ul class="nav nav-tabs btn-group box-tabs-left">
                                <li>
                                    <a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">Галерея (максимум 2 изображения)</a>
                                </li>
                                <li class="active">
                                    <a class="btn btn-outline-secondary active" href="#box_tab1" data-toggle="tab">Основная
                                        часть</a>
                                </li>
                            </ul>


                            <div class="tab-content">

								<? if (Yii::app()->user->hasFlash('success')): ?>
                                    <div class="alert alert-success fade in">
                                        <i class="icon-remove close" data-dismiss="alert"></i>
                                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                                    </div>
								<? endif; ?>

                                <div class="tab-pane active" id="box_tab1">

                                    <div class="form-group">
                                        <div class="form-check form-check-flat">
                                            <label class="form-check-label">
												<?= CHtml::activeCheckBox($event, 'event_active', [
													'checked' => $event->event_active ? ('checked') : (''),
													'class' => 'form-check-input'
												]); ?>
                                                Активность
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">* Автор:</label>
                                        <div class="input-width-xxlarge">
											<?= CHtml::activeTextField($event, 'event_author',
												array(
													'class' => 'form-control',
													'value' => $event->event_author
												)
											); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Дата:</label>
                                        <div class="input-width-large">
											<?= CHtml::activeTextField($event, 'event_date',
												array(
													'class' => 'form-control datepicker',
													'value' => $event->event_date
												)
											); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Место:</label>
										<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
											'model' => $event,
											'attribute' => 'event_place',
											'language' => 'ru',
										)); ?>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Количество гостей:</label>
										<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
											'model' => $event,
											'attribute' => 'event_guest',
											'language' => 'ru',
										)); ?>
                                    </div>


                                </div>





                                <div class="tab-pane" id="box_tab2">
                                    <div class="" style="display: flex; flex-wrap: wrap; margin-top: 25px;">
										<? foreach ($event->cmsModuleTeamGalleries as $key => $image): ?>
                                            <div class="productPreviewImageItem">
                                                <a data-fancybox="gallery"
                                                   href="<?= $image->getImage() ?>">
                                                    <img src="<?= $image->getImage('icon_') ?>">
                                                </a>
                                                <div class="productPreviewImage__content form-group">
													<?= CHtml::ajaxLink('Удалить',
														Yii::app()->createUrl('/admin/team/DeleteImage'),
														[
															'type' => 'POST',
															'data' => [
																'imageID' => $image->gallery_id
															],
															'success' => 'js:$(this).closest(".productPreviewImageItem").remove()'
														],
														[
															'class' => 'removeLink'
														]);
													?>
                                                </div>
                                            </div>
										<? endforeach; ?>
                                    </div>
                                    <div class="form-group">
                                        <div id="dropzone_profile_photo"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group" style="margin-top: 25px">
                        <div class="col-md-10">
							<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
                        </div>
                    </div>
					<?= CHtml::endForm() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        let myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/team/UploadImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            let data = JSON.stringify({
                event_id: <?=$event->event_id?>,
            });
            formData.append("params", data);
        });
    });
</script>


