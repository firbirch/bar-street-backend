<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 15:13
 */

$this->pageTitle = 'Управление завками';
$this->breadcrumbs = [
	$this->pageTitle
];
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$this->pageTitle?></h4>


                <div class="tabbable box-tabs">
                    <ul class="nav nav-tabs btn-group box-tabs-left">
						<?
						$index = 0;
                        foreach($requests as $requestHeader => $request): $index++;?>
                            <?if($index == 1):?>
                                <li class="active">
                                    <a class="btn btn-outline-secondary" href="#<?=$index?>" data-toggle="tab"><?=$requestHeader?></a>
                                </li>
                            <?else:?>
                                <li>
                                    <a href="#<?=$index?>" class="btn btn-outline-secondary " data-toggle="tab"><?=$requestHeader?></a>
                                </li>
                            <?endif;?>
						<?endforeach;?>
                    </ul>
                    <div class="tab-content">
						<?$index = 0;
                        foreach($requests as $requestHeader => $request): $index++?>
                            <div class="tab-pane <?=$index == 1 ? 'active' : ''?>" id="<?=$index?>">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Дата</th>
                                        <th>ФИО</th>
                                        <th>Почта</th>
                                        <th>Телефон</th>
                                        <th>UTM Source</th>
                                        <th>Расчетные данные</th>
                                        <th>Удаление</th>
                                    </tr>
                                    </thead>
                                    <?foreach($request as $row):?>
                                        <tr>
                                            <td><?=$row->form_date?></td>
                                            <td><?=$row->form_user_name ?: '-'?></td>
                                            <td><?=$row->form_email ?: '-'?></td>
                                            <td><?=$row->form_phone ?: '-'?></td>
                                            <td>
                                                <?
                                                if(is_object($row->form_utm))
                                                    foreach($row->form_utm as $key => $value) {
                                                        echo sprintf('%s: %s </br>', $key, $value);
                                                    }
                                                else
                                                    echo '-';
                                                ?>
                                            </td>
	                                        <td>
		                                        Часы: <?=$row->form_hours ?: '-'?><br>
		                                        Гости: <?=$row->form_guests ?: '-'?><br>
		                                        Категория: <?=$row->form_bar_category ?: '-'?><br>
		                                        Тип: <?=$row->form_bar_type ?: '-'?><br>
	                                        </td>
                                            <td>
                                                <a  class="btn btn-light" onclick="if(!confirm('Вы действительно хотите удалить?')) return false;" href = "<?=Yii::app()->createUrl('/admin/requests/remove', ['form_id' => $row->form_id])?>">
                                                    <i class="mdi mdi-close text-danger"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?endforeach;?>
                                </table>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

