<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 13/12/2018
 * Time: 10:34
 */


$this->pageTitle = 'Инфоблоки';
$this->breadcrumbs = [
    'Инфоблоки'
];

?>

<div class="row" style = "margin-bottom: 25px;">
	<div class="col-md-12" style = "display: flex; justify-content: flex-end">
		<button class="btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#promo">Добавить</button>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Инфоблоки</h4>
                <div class="table-responsive">
                    <table class="table">
					<thead>
					<tr>
						<th>Название инфоблока</th>
						<th>Тип инфоблока</th>
						<th>Активность</th>
                        <th>Редактировать</th>
                        <th>Удалить</th>
					</tr>
					</thead>
					<tbody>
					<?
					foreach ($iblocks as $key => $iblock):
						?>
						<tr>
							<td><?= $iblock->name ?></td>
							<td><?= $iblock->type ?></td>
							<td><span class="badge badge-<?=$iblock->i_active ? 'success' : 'danger'?>"><?=$iblock->i_active ? 'Да' : 'Нет'?></span></td>
							<td>

                                <a class="btn btn-outline-secondary" href="<?= Yii::app()->createURL('admin/iblock/Iblock', [
									'iblock' => $iblock->id
								]) ?>">
                                    <i class="mdi mdi-tooltip-edit"></i>
                                </a>

							</td>
                            <td>

								<?= CHtml::ajaxLink(
									'<i class="mdi mdi-bookmark-remove"></i>',
									'/admin/iblock/remove',
									[
										'type' => 'POST',
										'data' => [
											'iD' => $iblock->id,
											'element' => 0
										],
										'success' => 'js:$(this).closest(".iBlockElement").remove()'
									],
									[
										'class' => 'btn btn-outline-secondary',
                                        'confirm' => 'Вы уверены, что хотите удалить инфоблок'.PHP_EOL.'[Внимание] Элементы тоже будут удалены'
									]
								); ?>
                            </td>
						</tr>
					<? endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div class="row collapse" id="promo">
	<div class="col-md-12">
		<div class="widget box">
			<div class="widget-content">
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/iblock/add', ['type' => 1]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($iblock) ?>
				<div class="form-group">
					<label class="col-md-2 control-label">Заголовок: </label>
					<div class="col-md-10">
						<?= CHtml::activeTextField($iblock, 'name', [
							'value' => $iblock->name,
							'class' => 'form-control input-width-xxlarge'
						]); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Тип: </label>
					<div class="col-md-10">
						<?= CHtml::activeTextField($iblock, 'type', [
							'value' => $iblock->type,
							'class' => 'form-control input-width-xxlarge'
						]); ?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-10">
						<?= CHtml::submitButton('Создать', array('class' => 'btn btn-success')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>
