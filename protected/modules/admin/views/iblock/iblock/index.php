<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 13/12/2018
 * Time: 10:34
 */


$this->pageTitle = 'Инфоблоки';
$this->breadcrumbs = [
	'Инфоблоки' => '/admin/iblock/',
	'Инфоблок ('.$iblocks->name.')'
];
?>

<div class="row">
	<div class="col-md-12">
        <div class="card">
            <div class="card-body">

				<? if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success show">
                        <i class="icon-remove close" data-dismiss="alert"></i>
                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                    </div>
				<? endif; ?>

				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/iblock/save', ['type' => 1, 'id' => $iblocks->id]), 'POST', ['class' => 'form-horizontal row-border']) ?>
				<?= CHtml::errorSummary($iblocks) ?>



                <div class="form-group">
                    <div class="form-check form-check-flat">
                        <label class="form-check-label">
							<?= CHtml::activeCheckBox($iblocks, 'active', [
								'checked' => $iblocks->i_active ? ('checked') : (''),
								'class' => 'form-check-input'
							]); ?>
                            Активность
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-flat">
                        <label class="form-check-label">
							<?= CHtml::activeCheckBox($iblocks, 'use_gallery', [
								'checked' => $iblocks->use_gallery ? ('checked') : (''),
								'class' => 'form-check-input'
							]); ?>
                            Галерея
                        </label>
                    </div>
                </div>


				<div class="form-group">
					<label class="control-label">Название: </label>
					<div class="">
						<?= CHtml::activeTextField($iblocks, 'name', [
							'class' => 'form-control input-width-xxlarge',
							'value' => $iblocks->name
						]); ?>
					</div>
				</div>
				<div class="form-group">
					<label class=" control-label">Тип: </label>
					<div class="">
						<?= CHtml::activeTextField($iblocks, 'type', [
							'class' => 'form-control input-width-xxlarge',
							'value' => $iblocks->type
						]); ?>
					</div>
				</div>

				<div class="form-group">
                    <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

<div class="row" style = "margin: 25px 0;">
	<div class="col-md-12" style = "display: flex; justify-content: flex-end">
		<button class="btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#promo">Добавить</button>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Элементы инфоблока</h4>
			</div>
            <div class="table-responsive">
                <table class="table">
					<thead>
					<tr>
						<th>Название элемента</th>
						<th>Тип элемента инфоблока</th>
						<th>Активность</th>
                        <th>Редактировать</th>
                        <th>Удалить</th>
					</tr>
					</thead>
					<tbody>
					<?
					foreach ($iblockElements as $key => $element):
						?>
						<tr class="iBlockElement">
							<td><?= $element->title ?></td>
							<td><?= $element->slug ?></td>
							<td><span class="badge badge-<?=$element->el_active ? 'success' : 'danger'?>"><?=$element->el_active ? 'Да' : 'Нет'?></span></td>
							<td>
								<a class="btn btn-outline-secondary" href = "<?= Yii::app()->createURL('admin/iblock/element', [
									'element' => $element->id
								]) ?>">
                                    <i class="mdi mdi-tooltip-edit"></i>
								</a>
							</td>
                            <td>

								<?= CHtml::ajaxLink(
									'<i class="mdi mdi-bookmark-remove"></i>',
									'/admin/iblock/remove',
									[
										'type' => 'POST',
										'data' => [
											'iD' => $element->id,
                                            'element' => 1
										],
										'success' => 'js:$(this).closest(".iBlockElement").remove()'
									],
									[
										'class' => 'btn btn-outline-secondary',
										'confirm' => 'Элемент инфоблока будет удален'
									]
								); ?>
                            </td>
						</tr>
					<? endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="row collapse" id="promo">
	<div class="col-md-12">
		<div class="widget box">
			<div class="widget-content">
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/iblock/add', ['type' => 0, 'iblock' => $iblocks->id]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($element) ?>
				<div class="form-group">
					<label class="col-md-2 control-label">Заголовок: </label>
					<div class="col-md-10">
						<?= CHtml::activeTextField($element, 'title', [
							'value' => $element->title,
							'class' => 'form-control input-width-xxlarge'
						]); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Тип элемента: </label>
					<div class="col-md-10">
						<?= CHtml::activeTextField($element, 'slug', [
							'value' => $element->slug,
							'class' => 'form-control input-width-xxlarge'
						]); ?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-10">
						<?= CHtml::submitButton('Создать', array('class' => 'btn btn-success')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>
