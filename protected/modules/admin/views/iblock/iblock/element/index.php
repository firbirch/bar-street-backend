<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 13/12/2018
 * Time: 10:34
 */

$this->pageTitle = 'Инфоблоки';
$this->breadcrumbs = [
	'Инфоблоки' => '/admin/iblock/',
	'Инфоблок (' . $iblock->name . ')' => Yii::app()->createUrl('/admin/iblock/iblock', ['iblock' => $iblock->id]),
	'Элемент (' . $element->title . ')'
];
?>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Элемент: <?= $element->title ?></h4>
                <div class="widget-content">
					<? if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success show">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                        </div>
					<? endif; ?>
					<?= CHtml::beginForm(Yii::app()->createUrl('/admin/iblock/save', ['type' => 0, 'id' => $element->id]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
					<?= CHtml::errorSummary($element) ?>
                    <div class="tabbable box-tabs">
                        <ul class="nav nav-tabs btn-group box-tabs-left">
							<? if ($iblock->use_gallery): ?>
                                <li>
                                    <a href="#box_tab2" class="btn btn-outline-secondary"
                                       data-toggle="tab">Изображения</a>
                                </li>
							<? endif; ?>
                            <li class="active">
                                <a class="btn btn-outline-secondary active" href="#box_tab1" data-toggle="tab">Основная
                                    часть</a>
                            </li>
                        </ul>

                        <div class="tab-content">



							<? if ($iblock->use_gallery): ?>
                                <div class="tab-pane" id="box_tab2">
                                    <div class="" style="display: flex; flex-wrap: wrap; margin-top: 25px;">
										<? foreach ($element->cmsIblockElementImages as $key => $image): ?>
                                            <div class="productPreviewImageItem">
                                                <a data-fancybox="gallery"
                                                   href="/<?= $image->getImage() ?>">
                                                    <img src="/<?= $image->getImage('main_') ?>">
                                                </a>
                                                <input type="hidden" name="image[imageID][<?= $key ?>]"
                                                       value="<?= $image['id'] ?>">
                                                <div class="productPreviewImage__content">
                                                    <input type="text" name="image[imageName][<?= $key ?>]"
                                                           placeholder="Название изображения"
                                                           value="<?= $image->imageAlt ?>">
													<?= CHtml::ajaxLink('Удалить',
														Yii::app()->createUrl('/admin/iblock/DeleteImageElement'),
														[
															'type' => 'POST',
															'data' => [
																'imageID' => $image->id
															],
															'success' => 'js:$(this).closest(".productPreviewImageItem").remove()'
														],
														[
															'class' => 'removeLink'
														]);
													?>
                                                </div>
                                            </div>
										<? endforeach; ?>
                                    </div>

                                    <div class="form-group">
                                        <div id="dropzone_profile_photo"></div>
                                    </div>

                                </div>
							<? endif; ?>


                            <div class="tab-pane active" id="box_tab1">


                                <div class="form-group">
                                    <div class="form-check form-check-flat">
                                        <label class="form-check-label">
											<?= CHtml::activeCheckBox($element, 'el_active', [
												'checked' => $element->el_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
                                            Активность
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Название: </label>
                                    <div class="">
										<?= CHtml::activeTextField($element, 'title', [
											'class' => 'form-control input-width-xxlarge',
											'value' => $element->title
										]); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Название элемента: </label>
                                    <div class="">
										<?= CHtml::activeTextField($element, 'slug', [
											'class' => 'form-control input-width-xxlarge',
											'value' => $element->slug
										]); ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>Изображение: </label>
                                    <input type="file" name="Images" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled
                                               placeholder="<?=$element->image ? $element->image : 'Новый файл'?>">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info" type="button">Загрузить</button>
                                        </span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label">Контент:</label>
                                    <div class="">
										<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
											'model' => $element,
											'attribute' => 'content',
											'language' => 'ru',
										)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
					<?= CHtml::endForm() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function ($) {
        let myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/iblock/UploadImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            let data = JSON.stringify({
                element_id: <?=$element->id?>,
            });
            formData.append("params", data);
        });
    });
</script>
