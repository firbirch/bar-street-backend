<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/12/2018
 * Time: 15:46
 */

$this->pageTitle = 'Меню';
$this->breadcrumbs = [
	'Меню'
];
//CVarDumper::dump(CmsNestedPage::model()->roots()->findAll(), 10, true);


function buildTree($menu)
{
	foreach ($menu as $menu_item) {
		?>
        <li class="dd-item dd3-item" data-id="<?= $menu_item->id ?>">
            <div class="dd-handle dd3-handle">Drag</div>
            <div class="dd3-content">

                <span href="javascript:;"><?= $menu_item->cmsPage->name ?></span>
                <a href="<?=Yii::app()->createUrl('/admin/menu/delete', ['root' => $menu_item->id]) ?>"
                   onclick="if(!confirm('Вы действительно хотите удалить?')) return false;" style="color:#424964" >
                    <i class="mdi mdi-comment-remove-outline"></i>
                </a>

            </div>

            <div class="dd-handle dd3-handle">

            </div>
            <ol>
				<?php
				if ($menu_item->children()->findAll())
					buildTree($menu_item->children()->findAll());
				?>
            </ol>
        </li>
		<?
	}
}


?>
<? if (Yii::app()->user->hasFlash('success') || Yii::app()->user->hasFlash('error')): ?>
    <div class="alert alert-<?=Yii::app()->user->hasFlash('success') ? 'success' : 'danger'?> show">
        <i class="icon-remove close" data-dismiss="alert"></i>
        <strong><?=Yii::app()->user->hasFlash('success') ? 'Success' : 'Danger'?>!</strong>
		<?= Yii::app()->user->getFlash('success') ?: Yii::app()->user->getFlash('error')  ?>
    </div>
<? endif; ?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Меню</h4>




                <div class="widget-content">

                    <form class="row form-group" method="post"
                          action="<?= Yii::app()->createUrl('/admin/menu/MenuAdd') ?>">

                        <?if($pages):?>
                            <label class="col-md-2 control-label">Добавить страницу:</label>
                            <select name="MenuID" id="Products_catalogId"
                                    class="select2-select-00 col-md-2" data-role="select2">
                                <? foreach ($pages as $page): ?>
                                    <option value="<?= $page->id ?>"><?= $page->name ?></option>
                                <? endforeach; ?>
                            </select>

                            <div class="col-md-1">
                                <button class="btn btn-success">Добавить</button>
                            </div>
                        <?endif;?>
                    </form>

                    <div class="row">
                        <div class="col-md-12">
							<? if ($menu) { ?>
                                <div class="dd" id="nestable3" style="border: 1px dashed lightgrey; padding: 10px">
                                    <ol class="dd-list">
										<? foreach ($menu as $menu_item): ?>
                                            <li class="dd-item dd3-item" data-id="<?= $menu_item->id ?>">
                                                <div class="dd-handle dd3-handle">Drag</div>
                                                <div class="dd3-content">
                                                    <span href="javascript:;"><?= $menu_item->cmsPage->name ?></span>
                                                </div>
                                                <div class="dd-handle dd3-handle">

                                                </div>
                                                <ol>
													<?php
													if ($menu_item->children()->findAll())
														buildTree($menu_item->children()->findAll());
													?>
                                                </ol>
                                            </li>
										<? endforeach; ?>
                                    </ol>
                                </div>
							<? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



