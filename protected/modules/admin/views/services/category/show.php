<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:20
 */
$this->pageTitle = $category->category_name ?: 'Новая категория';
$this->breadcrumbs = [
	'Категории' => ['/admin/services/category'],
	$this->pageTitle
];
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/services/CategorySaveAll', ['category' => $category->category_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border']) ?>
				<?= CHtml::errorSummary($category) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>
						<div class="tab-content ">
							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($category, 'category_active', [
												'checked' => $category->category_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Позиция:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($category, 'category_sort',
											array(
												'class' => 'form-control',
												'value' => $category->category_sort
											)
										); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">* Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($category, 'category_name',
											array(
												'class' => 'form-control',
												'value' => $category->category_name
											)
										); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">* Заголовок en:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($category, 'category_name_en',
											array(
												'class' => 'form-control',
												'value' => $category->category_name_en
											)
										); ?>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="control-label">Иконка:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($category, 'category_icon',
											array(
												'class' => 'form-control',
												'value' => $category->category_icon
											)
										); ?>
                                    </div>
                                </div>


							</div>
						</div>
					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>
