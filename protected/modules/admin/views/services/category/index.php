<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:01
 */

$this->pageTitle = 'Категории';
$this->breadcrumbs = [
	$this->pageTitle
];

?>


<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/services/showCategory') ?>">Добавить</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$this->pageTitle?></h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Заголовок</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<tbody>
						<? foreach ($categories as $category): ?>
							<tr class="categoryItem">
								<td><?= $category->category_name ?></td>
								<td>
									<label class="badge badge-<?= $category->category_active ? 'success' : 'danger' ?>"><?= $category->category_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/services/showCategory', [
										'category' => $category->category_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>
									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/service/serviceRemove',
										[
											'type' => 'POST',
											'data' => [
												'page' => $category->category_id
											],
											'success' => 'js:$(this).closest(".categoryItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Страница будет удалена'
										]
									); ?>
								</td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

