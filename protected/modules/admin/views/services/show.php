<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:20
 */

$this->pageTitle = $service->service_name ?: 'Новая услуга';
$this->breadcrumbs = [
	'Услуги' => ['/admin/services/'],
	$this->pageTitle
];
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/services/SaveAll', ['service' => $service->service_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($service) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li>
								<a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">SEO</a>
							</li>
							<li>
								<a href="#eng_tab" class="btn btn-outline-secondary" data-toggle="tab">Eng</a>
							</li>
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>
						<div class="tab-content ">
							<div class="tab-pane" id="eng_tab">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($service, 'service_active_en', [
												'checked' => $service->service_active_en ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">* Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_name_en',
											array(
												'class' => 'form-control name',
												'value' => $service->service_name_en
											)
										); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Анонс:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $service,
										'attribute' => 'service_anounce_en',
										'language' => 'ru',
									)); ?>
								</div>
								<div class="form-group">
									<label>Обложка: </label>
									<input type="file" name="MainImageEn" class="file-upload-default">
									<div class="input-group col-xs-12">
										<input type="text" class="form-control file-upload-info" disabled
										       placeholder="<?= $service->service_image_en ? $service->service_image_en : 'Новый файл' ?>">
										<span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
                                                    type="button">Загрузить</button>
                                        </span>
									</div>
								</div>
							</div>
							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($service, 'service_active', [
												'checked' => $service->service_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>
                                <div class="form-group">
                                    <div class="form-check form-check-flat">
                                        <label class="form-check-label">
											<?= CHtml::activeCheckBox($service, 'service_background_color', [
												'checked' => $service->service_background_color ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
                                            Черный фон?
                                        </label>
                                    </div>
                                </div>
                                <div class="jsCoreTranslate">
                                    <div class="form-group">
                                        <label class="control-label">* Заголовок:</label>
                                        <div class="input-width-xxlarge">
                                            <?= CHtml::activeTextField($service, 'service_name',
                                                array(
                                                    'class' => 'form-control name js-slugify',
                                                    'value' => $service->service_name
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">* URL:</label>
                                        <div class="input-width-xxlarge">
                                            <?= CHtml::activeTextField($service, 'service_slug',
                                                array(
                                                    'class' => 'form-control slug js-slugify-slug',
                                                    'value' => $service->service_slug
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                </div>

								<div class="form-group">
									<label class="control-label">Сортировка на странице услуги:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'position_service',
											array(
												'class' => 'form-control',
												'value' => $service->position_service
											)
										); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Сортировка в меню:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_sort',
											array(
												'class' => 'form-control',
												'value' => $service->service_sort
											)
										); ?>
									</div>
								</div>

								<div class="form-group">
									<label>Обложка: </label>
									<input type="file" name="MainImage" class="file-upload-default">
									<div class="input-group col-xs-12">
										<input type="text" class="form-control file-upload-info" disabled
											   placeholder="<?= $service->service_image ? $service->service_image : 'Новый файл' ?>">
										<span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
													type="button">Загрузить</button>
                                        </span>
									</div>
								</div>

                                <div class="form-group">
                                    <label class="control-label">Иконка:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_icon',
											array(
												'class' => 'form-control',
												'value' => $service->service_icon
											)
										); ?>
                                    </div>
                                </div>

								<div class="form-group">
									<label class="control-label">Класс иконки:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_icon_class',
											array(
												'class' => 'form-control',
												'value' => $service->service_icon_class
											)
										); ?>
									</div>
								</div>

                                <div class="form-group">
                                    <label class="control-label">Заголовок для каталога:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_catalog_title',
											array(
												'class' => 'form-control',
												'value' => $service->service_catalog_title
											)
										); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Комментарий раздела:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_comment',
											array(
												'class' => 'form-control',
												'value' => $service->service_comment
											)
										); ?>
                                    </div>
                                </div>


								<div class="form-group">
									<label class="control-label">Анонс:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $service,
										'attribute' => 'service_anounce',
										'language' => 'ru',
									)); ?>
								</div>
								<div class="form-group">
									<label class="control-label">Контент:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $service,
										'attribute' => 'service_content',
										'language' => 'ru',
									)); ?>
								</div>

								<div class="form-group">
									<label for="category">Подкатегория:</label>
									<select name="CmsModuleService[cms_module_service_category_category_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($categories as $category):?>
											<option value="<?=$category->category_id?>" <?=$category->category_id == $service->cms_module_service_category_category_id ? 'selected' : ''?>>
												<?=$category->category_name?>
											</option>
										<? endforeach; ?>
									</select>
								</div>

                                <div class="form-group">
                                    <label for="category">Категория:</label>
                                    <select name="CmsModuleService[cms_module_serivce_category_action_category_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($categoryController as $controller):?>
                                            <option value="<?=$controller->category_id?>" <?=$controller->category_id == $service->cms_module_serivce_category_action_category_id ? 'selected' : ''?>>
												<?=$controller->category_name?>
                                            </option>
										<? endforeach; ?>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="category">Подкатегория (Страница Услуги):</label>
                                    <select name="CmsModuleService[cms_module_subcategory_subcategory_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($subcategory as $category):?>
                                            <option value="<?=$category->subcategory_id?>" <?=$category->subcategory_id == $service->cms_module_subcategory_subcategory_id ? 'selected' : ''?>>
												<?=$category->subcategory_name?>
                                            </option>
										<? endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="category">Преимущество</label>
                                    <select name="Advantages[]" multiple style="height: 250px" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($advantages as $advantage):?>
                                            <option value="<?=$advantage->advantage_id?>" <?=in_array($advantage->advantage_id,$advantagesIds) == true ? 'selected' : ''  ?> >
												<?=$advantage->advantage_name?>
                                            </option>
                                            <?foreach($advantage->cmsAdvantageParams as $advantage_):?>
                                                <option value="<?=$advantage->advantage_id?>_<?=$advantage_->params_id?>" <?=in_array($advantage->advantage_id.'_'.$advantage_->params_id,$advantagesIds) == true ? 'selected' : ''  ?>>
													[<?=$advantage_->params_tag ?: '-'?>] <?=$advantage->advantage_name?>
                                                </option>
                                            <?endforeach;?>
										<? endforeach; ?>
                                    </select>
                                </div>

								<div class="form-group">
									<label for="category">Лейбл:</label>
									<select name="CmsModuleService[cms_module_label_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<option></option>
										<? foreach($labels as $label):?>
											<option value="<?=$label->id?>" <?=$label->id == $service->cms_module_label_id ? 'selected' : ''?>>
												<?=$label->label?> [<?=$label->hex_color?>]
											</option>
										<? endforeach; ?>
									</select>
								</div>

								<div class="form-group">
									<label for="category">Слайдер:</label>
									<select name="CmsModuleService[sliderId]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<option></option>
										<? foreach($service->getSliders() as $id => $label):?>
											<option value="<?=$id?>" <?=$id == $service->sliderId ? 'selected' : ''?>>
												<?=$label?>
											</option>
										<? endforeach; ?>
									</select>
								</div>

							</div>
							<div class="tab-pane" id="box_tab2">
								<div class="form-group">
									<label class="control-label">(SEO) Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_seo_title',
											array(
												'class' => 'form-control',
												'value' => $service->service_seo_title
											)
										); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">(SEO) Описание:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($service, 'service_seo_description',
											array(
												'class' => 'form-control',
												'value' => $service->service_seo_description
											)
										); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

