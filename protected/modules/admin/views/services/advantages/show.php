<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:20
 */

$this->pageTitle = $advantage->advantage_name ?: 'Новое преимущество';
$this->breadcrumbs = [
	'Преимущества' => ['/admin/services/advantages'],
	$this->pageTitle
];
?>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/services/AdvantageSaveAll', ['advantage' => $advantage->advantage_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($advantage) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>
						<div class="tab-content ">
							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($advantage, 'advantage_active', [
												'checked' => $advantage->advantage_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">* Заголовок:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($advantage, 'advantage_name',
											array(
												'class' => 'form-control',
												'value' => $advantage->advantage_name
											)
										); ?>
									</div>
								</div>

                                <div class="form-group">
                                    <label class="control-label">Иконка:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($advantage, 'advantage_icon',
											array(
												'class' => 'form-control',
												'value' => $advantage->advantage_icon
											)
										); ?>
                                    </div>
                                </div>

<!--                                <div class="form-group">-->
<!--                                    <label>Иконка: </label>-->
<!--                                    <input type="file" name="MainImage" class="file-upload-default">-->
<!--                                    <div class="input-group col-xs-12">-->
<!--                                        <input type="text" class="form-control file-upload-info" disabled-->
<!--                                               placeholder="--><?//= $advantage->advantage_icon ? $advantage->advantage_icon : 'Новый файл' ?><!--">-->
<!--                                        <span class="input-group-append">-->
<!--                                            <button class="file-upload-browse btn btn-info"-->
<!--                                                    type="button">Загрузить</button>-->
<!--                                        </span>-->
<!--                                    </div>-->
<!--                                </div>-->





                                <div class="form-group">
									<label class="control-label">Контент:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $advantage,
										'attribute' => 'advantage_content',
										'language' => 'ru',
									)); ?>
								</div>


                                <div class="form-group" id="cmsAdvantageParams" style="display: flex; flex-direction: column; flex-wrap: wrap">
                                    <? if (!empty($advantage->cmsAdvantageParams)) {
                                        foreach ($advantage->cmsAdvantageParams as $param): ?>
                                            <div class="params-block" style="display: flex; align-items: center">
                                                <textarea class="form-control" type="text" style="width: 60%; margin-top: 10px;" name="params[]"
                                                       placeholder="Альтернативный текст"><?= $param['params_content'] ?></textarea>
                                                <input type="text" class="form-control" style="width: 30%; margin-left: 10px" placeholder="Tag" name="params_tag[]" value="<?= $param['params_tag'] ?>">
                                                <input type="hidden"  name="params_id[]" value="<?= $param['params_id'] ?>">
                                                <?= CHtml::ajaxLink(
													'<i class="mdi mdi-close text-white"></i>',
                                                    Yii::app()->createUrl('/admin/services/advantageRemoveParam'),
                                                    array(
                                                        'type' => 'POST',// method
                                                        'data' => array(
                                                            'id' => $param['params_id'],
                                                        ),
                                                        'success' => 'js:$(this).parent(".params-block").remove()'
                                                    ),
                                                    array('class' => 'btn btn-danger', 'style' => 'margin-left: 25px;')
                                                );
                                                ?>
                                            </div>
                                        <? endforeach; ?>
                                    <? } else { ?>
                                        <div class="params-block" style="display: flex; align-items: center">
                                            <textarea type="text" class="form-control" name="newParams[]"
                                                      placeholder="Альтернативный текст" style=" width: 60%;"></textarea>
                                            <input type="text" name="newParamsTag[]" class="form-control" style="margin-left: 10px; width: 30%;" value="" placeholder="Tag">
                                        </div>
                                    <? } ?>
                                </div>
                                <a class="btn btn-success _textareaadd" href="javascript:;">
                                    <i class="mdi mdi-plus text--white"></i>
                                </a>
							</div>
						</div>
					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        $('._textareaadd').on('click', function () {
            let html = '<div class="params-block" style="display: flex; align-items: center">\n' +
                '<textarea type="text" class="form-control" name="newParams[]"\n' +
                '    placeholder="Альтернативный текст" style=" width: 60%; margin-top: 10px"></textarea>\n' +
                '    <input type="text" name="newParamsTag[]" class="form-control" style="margin-left: 10px; width: 30%;" value="" placeholder="Tag">\n' +
                '</div>';
            $('#cmsAdvantageParams').append(html);
        })

    })

</script>