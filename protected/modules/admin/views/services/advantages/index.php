<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:01
 */

$this->pageTitle = 'Преимущества';
$this->breadcrumbs = [
	$this->pageTitle
];
?>

<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/services/showAdvantages') ?>">Добавить</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$this->pageTitle?></h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Заголовок</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<tbody>
						<? foreach ($advantages as $advantage): ?>
							<tr class="advantageRemove">
								<td><?= $advantage->advantage_name ?></td>
								<td>
									<label class="badge badge-<?= $advantage->advantage_active ? 'success' : 'danger' ?>"><?= $advantage->advantage_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/services/showAdvantages', [
										'advantage' => $advantage->advantage_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>
									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/services/advantageRemove',
										[
											'type' => 'POST',
											'data' => [
												'advantage' => $advantage->advantage_id
											],
											'success' => 'js:$(this).closest(".advantageRemove").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Страница будет удалена'
										]
									); ?>
								</td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
