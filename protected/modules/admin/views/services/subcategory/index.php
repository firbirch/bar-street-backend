<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 21/01/2019
 * Time: 15:01
 */

$this->pageTitle = 'Подкатегория';
$this->breadcrumbs = [
	$this->pageTitle
];

?>


<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/services/showSubcategory') ?>">Добавить</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$this->pageTitle?></h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Заголовок</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<tbody>
						<? foreach ($categories as $category): ?>
							<tr class="categoryItem">
								<td><?= $category->subcategory_name ?></td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/services/showSubcategory', [
										'subcategory' => $category->subcategory_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
                                <td>
                                    <a  class="btn btn-light" onclick="if(!confirm('Вы действительно хотите удалить?')) return false;" href = "<?=Yii::app()->createUrl('/admin/services/removeSubCategory', ['subcategory' => $category->subcategory_id])?>">
                                        <i class="mdi mdi-close text-danger"></i>
                                    </a>

                                </td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

