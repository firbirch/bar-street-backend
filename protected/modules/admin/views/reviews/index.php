<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 15:13
 */

$this->pageTitle = 'Отзывы';
$this->breadcrumbs = [
	$this->pageTitle
];
?>

<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/reviews/show') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Отзывы</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Отзыв</th>
							<th>Автор</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<?foreach ($reviews as $review):?>
							<tr class="reviewItem">
								<td>
									<?=strip_tags(substr($review->review_content, 0,100).'...')?>
								</td>
								<td>
									<?=$review->review_name?>
								</td>

								<td>
									<label class="badge badge-<?= $review->review_active ? 'success' : 'danger' ?>"><?= $review->review_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/reviews/show', [
										'review' => $review->review_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/reviews/remove',
										[
											'type' => 'POST',
											'data' => [
												'page' => $review->review_id
											],
											'success' => 'js:$(this).closest(".reviewItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Отзыв будет удален'
										]
									); ?>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

