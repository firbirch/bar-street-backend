<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 16:40
 */

$this->pageTitle = $review->review_name ?: 'Новый отзыв';
$this->breadcrumbs = [
	'Отзывы' => ['/admin/reviews/'],
	$this->pageTitle
];
?>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/reviews/SaveAll', ['review' => $review->review_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($review) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>

						<div class="tab-content ">

							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($review, 'review_active', [
												'checked' => $review->review_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">* Автор:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($review, 'review_name',
											array(
												'class' => 'form-control',
												'value' => $review->review_name
											)
										); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Должность:</label>
									<div class="input-width-xxlarge">
										<?= CHtml::activeTextField($review, 'review_rang',
											array(
												'class' => 'form-control',
												'value' => $review->review_rang,
											)
										); ?>
									</div>
								</div>

                                <div class="form-group">
                                    <label class="control-label">Дата:</label>
                                    <div class="input-width-small">
										<?= CHtml::activeTextField($review, 'review_date',
											array(
												'class' => 'form-control datepicker',
												'value' => $review->review_date
											)
										); ?>
                                    </div>
                                </div>


								<div class="form-group">
									<label class="control-label">Контент:</label>
									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $review,
										'attribute' => 'review_content',
										'language' => 'ru',
									)); ?>
								</div>

                                <div class="form-group">
                                    <label for="category">Услуга</label>
                                    <select name="CmsModuleReviews[cms_module_service_service_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($services as $service):?>
                                            <option value="<?=$service->service_id?>" <?=$service->service_id == $review->cms_module_service_service_id ? 'selected' : ''  ?> >
												<?=$service->service_name?>
                                            </option>
										<? endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="category">Фотогалерея</label>
                                    <select name="CmsModuleReviews[cms_module_gallery_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($gallery as $item):?>
                                            <option value="<?=$item->id?>" <?=$item->id == $review->cms_module_gallery_id  ? 'selected' : ''  ?> >
												<?=$item->name?>
                                            </option>
										<? endforeach; ?>
                                    </select>
                                </div>

							</div>


						</div>


					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

