<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/02/2019
 * Time: 13:57
 */

$this->pageTitle = 'Клиенты';
$this->breadcrumbs = [
	$this->pageTitle
];

?>


<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/clients/show') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$this->pageTitle?></h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Имя</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<?foreach ($clients as $client):?>
							<tr class="eventItem">
								<td>
									<?=$client->client_name?>
								</td>
								<td>
									<label class="badge badge-<?= $client->client_active ? 'success' : 'danger' ?>"><?= $client->client_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/clients/show', [
										'client' => $client->client_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/clients/remove',
										[
											'type' => 'POST',
											'data' => [
												'client' => $client->client_id
											],
											'success' => 'js:$(this).closest(".eventItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Клиент будет удален'
										]
									); ?>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>