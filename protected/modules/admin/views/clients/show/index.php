<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/02/2019
 * Time: 13:57
 */

$this->pageTitle = $client->client_name ?: 'Добавление нового клиента';
$this->breadcrumbs = [
	'Статьи' => ['/admin/clients/'],
	$this->pageTitle
];
?>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/clients/SaveAll', ['client' => $client->client_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($client) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>

						<div class="tab-content ">


							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($client, 'client_active', [
												'checked' => $client->client_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>



								<div class="jsCoreTranslate">
									<div class="form-group">
										<label class="control-label">* Заголовок:</label>
										<div class="input-width-xxlarge">
											<?= CHtml::activeTextField($client, 'client_name',
												array(
													'class' => 'form-control name',
													'value' => $client->client_name
												)
											); ?>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label">* URL:</label>
										<div class="input-width-xxlarge">
											<?= CHtml::activeTextField($client, 'client_slug',
												array(
													'class' => 'form-control slug',
													'value' => $client->client_slug
												)
											); ?>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Дата:</label>
									<div class="input-width-small">
										<?= CHtml::activeTextField($client, 'client_date',
											array(
												'class' => 'form-control datepicker',
												'value' => $client->client_date
											)
										); ?>
									</div>
								</div>

								<div class="form-group">
									<label>Изображение (Главная): </label>
									<input type="file" name="MainImage" class="file-upload-default">
									<div class="input-group col-xs-12">
										<input type="text" class="form-control file-upload-info" disabled
											   placeholder="<?= $client->client_main_image ? $client->client_main_image : 'Новый файл' ?>">
										<span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
													type="button">Загрузить</button>
                                        </span>
									</div>
								</div>

								<div class="form-group">
									<label>Изображение [Блок Клиенты]: </label>
									<input type="file" name="Image" class="file-upload-default">
									<div class="input-group col-xs-12">
										<input type="text" class="form-control file-upload-info" disabled
											   placeholder="<?= $client->client_image ? $client->client_image : 'Новый файл' ?>">
										<span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
													type="button">Загрузить</button>
                                        </span>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Заголовок:</label>

									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $client,
										'attribute' => 'client_content',
										'language' => 'ru',
									)); ?>

								</div>




							</div>


						</div>


					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>
