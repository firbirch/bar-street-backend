<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 01/02/2019
 * Time: 10:37
 */

$this->pageTitle = 'Авторизация';
$this->breadcrumbs = [
	$this->pageTitle
];
?>

<div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
	<div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
		<div class="row w-100">
			<div class="col-lg-4 mx-auto">
				<div class="auto-form-wrapper">

					<?= CHtml::beginForm(Yii::app()->createUrl('/admin/'), 'POST') ?>
					<?= CHtml::errorSummary($auth) ?>
						<div class="form-group">
							<label class="label">Логин</label>
							<div class="input-group">

								<?= CHtml::activeTextField($auth, 'username',
									array(
										'class' => 'form-control',
										'value' => $auth->username,
										'placeholder' => 'Логин'
									)
								); ?>
								<div class="input-group-append">
								  <span class="input-group-text">
									<i class="mdi mdi-check-circle-outline"></i>
								  </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="label">Пароль</label>
							<div class="input-group">

								<?= CHtml::activePasswordField($auth, 'password',
									array(
										'class' => 'form-control',
										'value' => $auth->password,
										'placeholder' => '*********'
									)
								); ?>

								<div class="input-group-append">
								  <span class="input-group-text">
									<i class="mdi mdi-check-circle-outline"></i>
								  </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<button class="btn btn-primary submit-btn btn-block">Авторизация</button>
						</div>
<!--						<div class="form-group d-flex justify-content-between">-->
<!--							<div class="form-check form-check-flat mt-0">-->
<!--								<label class="form-check-label">-->
<!--									--><?//= CHtml::activeCheckBox($auth, 'rememberMe', [
//										'checked' => $auth->rememberMe ? ('checked') : (''),
//										'class' => 'form-check-input'
//									]); ?>
<!--									Запомнить ?-->
<!--								</label>-->
<!--							</div>-->
<!--						</div>-->
					<?=CHtml::endForm()?>
				</div>
				<p class="footer-text text-center">copyright © 2018 Bootstrapdash. All rights reserved.</p>
			</div>
		</div>
	</div>
	<!-- content-wrapper ends -->
</div>
