<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 16:40
 */

$this->pageTitle = $event->event_date ?: 'Новое событие';
$this->breadcrumbs = [
	'Статьи' => ['/admin/events/'],
	$this->pageTitle
];
?>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?= $this->pageTitle ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success show">
						<i class="icon-remove close" data-dismiss="alert"></i>
						<strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
					</div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/events/SaveAll', ['event' => $event->event_id ?: 0]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
				<?= CHtml::errorSummary($event) ?>
				<div class="widget-content">
					<div class="tabbable box-tabs">
						<ul class="nav nav-tabs btn-group box-tabs-left">
							<li class="active">
								<a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
							</li>
						</ul>

						<div class="tab-content ">


							<div class="tab-pane active" id="box_tab1">
								<div class="form-group">
									<div class="form-check form-check-flat">
										<label class="form-check-label">
											<?= CHtml::activeCheckBox($event, 'event_active', [
												'checked' => $event->event_active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
											Активность
										</label>
									</div>
								</div>


                                <div class="form-group">
                                    <label class="control-label">* Дата:</label>
                                    <div class="input-width-small">
										<?= CHtml::activeTextField($event, 'event_date',
											array(
												'class' => 'form-control datepicker',
												'value' => $event->event_date
											)
										); ?>
                                    </div>
                                </div>

								<div class="form-group">
									<label>Изображение: </label>
									<input type="file" name="MainImage" class="file-upload-default">
									<div class="input-group col-xs-12">
										<input type="text" class="form-control file-upload-info" disabled
											   placeholder="<?= $event->event_image ? $event->event_image : 'Новый файл' ?>">
										<span class="input-group-append">
                                            <button class="file-upload-browse btn btn-info"
													type="button">Загрузить</button>
                                        </span>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Ссылка:</label>
									<div class="input-width-small">
										<?= CHtml::activeTextField($event, 'event_link',
											array(
												'class' => 'form-control',
												'value' => $event->event_link
											)
										); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Заголовок:</label>

									<?php $this->widget('application.extensions.ckeditor.CKEditor', array(
										'model' => $event,
										'attribute' => 'event_header',
										'language' => 'ru',
									)); ?>

								</div>

                                <div class="form-group">
                                    <label for="category">Галерея</label>
                                    <select name="CmsModuleEvents[cms_module_gallery_id]" class="form-control form-control-sm" id="exampleFormControlSelect3">
										<? foreach($galleries as $gallery):?>
                                            <option value="<?=$gallery->id?>" <?=$event->cms_module_gallery_id == $gallery->id ? 'selected' : ''  ?> >
												<?=$gallery->name?>
                                            </option>
										<? endforeach; ?>
                                    </select>
                                </div>



							</div>


						</div>


					</div>
				</div>
				<div class="row form-group" style="margin-top: 25px">
					<div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
					</div>
				</div>
				<?= CHtml::endForm() ?>
			</div>
		</div>
	</div>
</div>

