<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 25/01/2019
 * Time: 16:33
 */

$this->pageTitle = 'Лента на главной';
$this->breadcrumbs = [
	$this->pageTitle
];

?>


<div class="row" style="margin-bottom: 25px;">
	<div class="col-md-12" style="display: flex; justify-content: flex-end">
		<a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/events/show') ?>">Добавить</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Лента на главной</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Дата</th>
							<th>Активность</th>
							<th>Редактировать</th>
							<th>Удалить</th>
						</tr>
						</thead>
						<?foreach ($events as $event):?>
							<tr class="eventItem">
								<td>
									<?=$event->event_date?>
								</td>
								<td>
									<label class="badge badge-<?= $event->event_active ? 'success' : 'danger' ?>"><?= $event->event_active ? 'Да' : 'Нет' ?></label>
								</td>
								<td>
									<a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/events/show', [
										'eventID' => $event->event_id
									]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
									</a>
								</td>
								<td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/events/delete',
										[
											'type' => 'POST',
											'data' => [
												'event' => $event->event_id
											],
											'success' => 'js:$(this).closest(".eventItem").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Лента будет удалена'
										]
									); ?>
								</td>
							</tr>
						<?endforeach;?>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
