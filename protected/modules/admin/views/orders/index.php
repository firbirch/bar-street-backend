<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 15:13
 */

$this->pageTitle = 'Управление заказами';
$this->breadcrumbs = [
	$this->pageTitle
]
?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$this->pageTitle?></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Дата оформления</th>
                            <th>Имя</th>
                            <th>Почта</th>
                            <th>Телефон</th>
                            <th>Товары</th>
                            <th>Удалить</th>
                        </tr>
                        </thead>
                        <?php foreach($orders as $row):?>
                            <tr class="even">
                                <td><?=$row->order_date?></td>
                                <td><?=$row->order_name ?: '-'?></td>
                                <td><?=$row->order_email ?: '-'?></td>
                                <td><?=$row->order_phone?></td>
                                <td>
                                    <ul>
                                        <?php 
                                        if($row->getProducts())
                                        foreach($row->getProducts() as $product):?>
                                            <li><?=$product->product_name?> <?=$product->product_quantity?> шт - <?=$product->product_price?> руб.</li>
                                        <?endforeach;?>
                                    </ul>
                                    Итого: <b><?=$row->order_cost?></b> руб
                                </td>
                                <td>
                                    <a class="btn btn-light" onclick="if(!confirm('Вы действительно хотите удалить?')) return false;" href = "<?=Yii::app()->createUrl('/admin/orders/remove', ['order_id' => $row->order_id])?>">
                                        <i class="mdi mdi-close text-danger"></i>Удалить
                                    </a>
                                </td>
                            </tr>
                        <?endforeach;?>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>

