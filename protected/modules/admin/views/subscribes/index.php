<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 27/01/2019
 * Time: 15:13
 */

$this->pageTitle = 'Управление подписками';
$this->breadcrumbs = [
	$this->pageTitle
];
?>

<div class="row" style="margin-bottom: 25px;">
    <div class="col-md-12" style="display: flex; justify-content: flex-end">
        <a class="btn btn-success" target="_blank" href="<?= Yii::app()->createURL('/admin/subscribes/export') ?>">Export CSV</a>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><?=$this->pageTitle?></h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Почта</th>
                            <th>Дата добавления</th>
                            <th>Активность</th>
                            <th>Удалить</th>
                        </tr>
                        </thead>
                        <?foreach($subscribes as $row):?>
                            <tr class="subScribe">
                                <td><?=$row->subscribe_email ?: '-'?></td>
                                <td><?=$row->subscribe_date ?: '-'?></td>
                                <td>
                                    <label class="badge badge-<?= $row->subscribe_active ? 'success' : 'danger' ?>"><?= $row->subscribe_active ? 'Да' : 'Нет' ?></label>
                                </td>
                                <td>
									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/subscribes/remove',
										[
											'type' => 'POST',
											'data' => [
												'subscribe_id' => $row->subscribe_id
											],
											'success' => 'js:$(this).closest(".subScribe").remove()'
										],
										[
											'class' => 'btn btn-light',
											'confirm' => 'Элемент будет удален'
										]
									); ?>
                                </td>
                            </tr>
                        <?endforeach;?>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>

