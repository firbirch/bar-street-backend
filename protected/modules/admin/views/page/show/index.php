<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/12/2018
 * Time: 15:28
 */


$this->pageTitle = isset($page->name) ? $page->name : 'Новая страница';
$this->breadcrumbs = [
	'Страницы' => ['/admin/page'],
	$this->pageTitle
]
?>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?= isset($page->name) ? $page->name : 'Новая страница' ?></h4>
				<? if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success show">
                        <i class="icon-remove close" data-dismiss="alert"></i>
                        <strong>Success!</strong> <?= Yii::app()->user->getFlash('success') ?>
                    </div>
				<? endif; ?>
				<?= CHtml::beginForm(Yii::app()->createUrl('/admin/page/SaveAll', ['pageID' => isset($page->id) ? $page->id : 0]), 'POST', ['class' => 'form-horizontal row-border']) ?>
				<?= CHtml::errorSummary($page) ?>
                <div class="widget-content">
                    <div class="tabbable box-tabs">
                        <ul class="nav nav-tabs btn-group box-tabs-left">
                            <li>
                                <a href="#box_tab3" class="btn btn-outline-secondary" data-toggle="tab">Настройки для разработчика</a>
                            </li>
                            <li>
                                <a href="#box_tab2" class="btn btn-outline-secondary" data-toggle="tab">SEO</a>
                            </li>
                            <li class="active">
                                <a class="btn btn-outline-secondary" href="#box_tab1" data-toggle="tab">Основная часть</a>
                            </li>
                        </ul>

                        <div class="tab-content ">

                            <div class="tab-pane" id="box_tab3">
                                <div class="form-group">
                                    <label class="control-label">Controller:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($page, 'controller',
											array(
												'class' => 'form-control',
												'value' => $page->controller
											)
										); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Action:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($page, 'action',
											array(
												'class' => 'form-control',
												'value' => $page->action
											)
										); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane active" id="box_tab1">
                                <div class="form-group">
                                    <div class="form-check form-check-flat">
                                        <label class="form-check-label">
											<?= CHtml::activeCheckBox($page, 'active', [
												'checked' => $page->active ? ('checked') : (''),
												'class' => 'form-check-input'
											]); ?>
                                            Активность
                                        </label>
                                    </div>
                                </div>
                                <div class="jsCoreTranslate">
                                    <div class="form-group">
                                        <label class="control-label">* Заголовок:</label>
                                        <div class="input-width-xxlarge">
                                            <?= CHtml::activeTextField($page, 'name',
                                                array(
                                                    'class' => 'form-control name js-slugify',
                                                    'value' => $page->name
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">* URL:</label>
                                        <div class="input-width-xxlarge">
                                            <?= CHtml::activeTextField($page, 'slug',
                                                array(
                                                    'class' => 'form-control slug js-slugify-slug',
                                                    'value' => $page->slug,
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Контент:</label>

                                    <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $page,
                                        'attribute' => 'content',
                                        'language' => 'ru',
                                    )); ?>

                                </div>
                            </div>

                            <div class="tab-pane" id="box_tab2">
                                <div class="form-group">
                                    <label class="control-label">(SEO) Заголовок:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($page, 'seo_title',
											array(
												'class' => 'form-control',
												'value' => $page->seo_title
											)
										); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">(SEO) Описание:</label>
                                    <div class="input-width-xxlarge">
										<?= CHtml::activeTextField($page, 'seo_description',
											array(
												'class' => 'form-control',
												'value' => $page->seo_description
											)
										); ?>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
                <div class="row form-group" style="margin-top: 25px">
                    <div class="col-md-10">
						<?= CHtml::submitButton('Применить', array('class' => 'btn btn-success', 'name' => 'Save')); ?>
                    </div>
                </div>
				<?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>

