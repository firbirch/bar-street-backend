<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 17/12/2018
 * Time: 15:28
 */

$this->pageTitle = 'Страницы';
$this->breadcrumbs = [
	'Страницы'
];
?>


<div class="row" style="margin-bottom: 25px;">
    <div class="col-md-12" style="display: flex; justify-content: flex-end">
        <a class="btn btn-success" href="<?= Yii::app()->createURL('/admin/page/show') ?>">Добавить</a>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Страницы</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Заголовок</th>
                            <th>URL <i class="icon-sort"></i></a></th>
                            <th>Сортировка <i class="icon-sort"></i></a></th>
                            <th>Активность</th>
                            <th>Редактировать</th>
                            <th>Удалить</th>
                        </tr>
                        </thead>
                        <tbody>

						<? foreach ($page as $item): ?>
                            <tr class="pageItem">
                                <td><?= $item->name ?></td>
                                <td><?= $item->slug ?: '-' ?></td>
                                <td class="clickSort" data-page="<?= $item->id ?>">
                                    <span><?= $item->sort ?: '-' ?></span>
                                </td>
                                <td>
                                    <label class="badge badge-<?= $item->active ? 'success' : 'danger' ?>"><?= $item->active ? 'Да' : 'Нет' ?></label>
                                </td>
                                <td>
                                    <a class="btn btn-light" href="<?= Yii::app()->createURL('/admin/page/show', [
										'page' => $item->id
									    ]) ?>">
                                        <i class="mdi mdi-eye text-primary"></i>
                                    </a>
                                </td>
                                <td>

									<?= CHtml::ajaxLink(
										'<i class="mdi mdi-close text-danger"></i>',
										'/admin/page/remove',
										[
											'type' => 'POST',
											'data' => [
												'page' => $item->id
											],
											'success' => 'js:$(this).closest(".pageItem").remove()'
										],
                                        [
											'class' => 'btn btn-light',
											'confirm' => 'Страница будет удалена'
                                        ]
									); ?>
                                </td>
                            </tr>
						<? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('.clickSort').on('dblclick', function () {
            if ($(this).hasClass('active')) return true;
            let content = $(this).find('span');
            let data = content.html();
            content.remove();
            $(this).append('<input class = "sortInput form-control input-width-small" type = "text" value = "' + data + '">').addClass('active');
            $(this).find('.sortInput').focus();
            return false;
        });
    });
    $(document).on('blur', '.sortInput', function () {
        let data = $(this).val();
        let pageID = $(this).closest('.clickSort').data('page');
        if (data)
            $.ajax({
                url: '/admin/page/ChangeSort',
                type: 'POST',
                data: {
                    pageId: pageID,
                    pageSort: data
                },
                success: function (request) {

                }
            });
        $(this).closest('.clickSort').append('<span>' + data + '</span>').removeClass('active');
        $(this).remove();
    });
</script>