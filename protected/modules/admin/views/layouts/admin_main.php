<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Административная панель 0.1</title>



    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.0/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="/css/admin_template_new/materialdesignicons.css">
    <link rel="stylesheet" href="/css/admin_template_new/vendor.bundle.base.css">
    <link rel="stylesheet" href="/css/admin_template_new/vendor.bundle.addons.css">
    <link rel="stylesheet" href="/css/admin_template_new/style.css">
    <link rel="shortcut icon" href="/image_admin/images/favicon.png"/>
    <link rel="stylesheet" href="/css/admin/dropzone.css">
    <link rel="stylesheet" href="/css/admin/custom.admin.css">


    <script src="/js/admin/main.admin.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>


    <!-- Bootstrap colorpicker --->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css">


    <!-- Zebra Picker --->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Zebra_datepicker/1.9.12/css/bootstrap/zebra_datepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Zebra_datepicker/1.9.12/zebra_datepicker.min.js"></script>

	<style>
		body { overflow-x: hidden; overflow: auto;}
	</style>

</head>
<body style="overflow-x: unset;">
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <a class="navbar-brand brand-logo" href="index.html">
                <img src="/image_admin/images/logo.svg" alt="logo"/>
            </a>
            <a class="navbar-brand brand-logo-mini" href="index.html">
                <img src="/image_admin/images/logo-mini.svg" alt="logo"/>
            </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">



            <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">




                <li class="nav-item">
                    <a href="/admin" class="nav-link">Панель управления</a>
                </li>
                <!--
                <li class="nav-item">
                    <a href="/admin" class="nav-link">Главная</a>
                </li>
                -->

            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                <span class="icon-menu"></span>
            </button>
        </div>
    </nav>
    <div class="container-fluid page-body-wrapper">
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item nav-profile">
                    <div class="nav-link">
                        <div class="user-wrapper">
                            <div class="profile-image">
                                <img src="/image_admin/images/faces/face1.jpg" alt="profile image">
                            </div>
                            <div class="text-wrapper">
                                <p class="profile-name">Admin</p>
                                <div>
                                    <small class="designation text-muted">Dev</small>
                                    <span class="status-indicator online"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin">
                        <i class="menu-icon mdi mdi-television"></i>
                        <span class="menu-title">Главная</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#shop" data-toggle="collapse" aria-expanded="true" aria-controls="module">
                        <i class="menu-icon mdi mdi-shopping"></i>
                        <span class="menu-title">Магазин</span>
                        <i class="menu-arrow down"></i>
                    </a>
                    <div class="active in" id="shop">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/coctails') ?>">
                                    Коктели
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/coctails/ing') ?>">
                                    Ингредиенты
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="#services" data-toggle="collapse" aria-expanded="true" aria-controls="module">
                        <i class="menu-icon mdi mdi-clock"></i>
                        <span class="menu-title">Услуги</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="active in" id="services">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/services') ?>">
                                    Услуги
                                </a>
                            </li>
	                        <li class="nav-item">
		                        <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/serviceSlider') ?>">
			                        Слайдеры
		                        </a>
	                        </li>
	                        <li class="nav-item">
		                        <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/servicesItems') ?>">
			                        Элементы каталога
		                        </a>
	                        </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/services/category') ?>">
                                    Категории
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/services/subcategory') ?>">
                                    Подкатегории
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/services/advantages') ?>">
                                    Преимущества
                                </a>
                            </li>
	                        <li class="nav-item">
		                        <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/serviceLabels') ?>">
			                       Лейблы
		                        </a>
	                        </li>
                        </ul>
                    </div>
                </li>



                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="true"
                       aria-controls="ui-basic">
                        <i class="menu-icon mdi mdi-content-copy"></i>
                        <span class="menu-title">Контент</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="active in" id="ui-basic">
                        <ul class="nav flex-column sub-menu">
	                        <li class="nav-item">
		                        <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/gallery') ?>">Фотоотчеты</a>
	                        </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/page') ?>">Страницы</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/articles') ?>">
                                    Статьи
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/events') ?>">
                                    Лента на главной
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/employee') ?>">
                                    Сотрудники
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/team') ?>">
                                    Мероприятия (Команда)
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#module" aria-expanded="true" aria-controls="module">
                        <i class="menu-icon mdi mdi-weather-cloudy"></i>
                        <span class="menu-title">Модули</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="module">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/gallery') ?>">
                                    Галерея
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/slider') ?>">
                                    Слайдер
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/subscribes') ?>">
                                    Подписчики (Рассылка)
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/clients') ?>">
                                    Клиенты
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="<?= Yii::app()->createUrl('/admin/reviews') ?>">
                                    Отзывы
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?=Yii::app()->createUrl('/admin/orders')?>">
                        <i class="menu-icon mdi mdi-account"></i>
                        <span class="menu-title">Управление заказами</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=Yii::app()->createUrl('/admin/requests')?>">
                        <i class="menu-icon mdi mdi-account"></i>
                        <span class="menu-title">Управление заявками</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="true" aria-controls="settings">
                        <i class="menu-icon mdi mdi-settings"></i>
                        <span class="menu-title">Настройки</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="settings">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="<?=Yii::app()->createUrl('/admin/settings')?>"> Основные настройки </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?=Yii::app()->createUrl('/admin/menu')?>"> Меню </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <nav aria-label="breadcrumb">
                    <?php if (isset($this->breadcrumbs)): ?>
                        <?=$this->widget('ABreadCrumbs', [
                            'links' => $this->breadcrumbs
                        ], true)?>
                    <?php endif ?>
                </nav>

				<?= $content ?>
            </div>
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                      <a href="http://www.bootstrapdash.com/"
                         target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                      <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.0/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/3.8.4/dropzone.js"></script>

<!--<script src="/js/admin_template_new/vendor.bundle.base.js" async ></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>

<script src="/js/admin/plugins/slugify.js"></script>
<script src="/js/admin/plugins/CyrillicToTranslit.js"></script>
<script src="/js/admin_template_new/off-canvas.js"></script>
<script src="/js/admin_template_new/misc.js"></script>
<script src="/js/admin_template_new/dashboard.js"></script>
<script src="/js/admin_template_new/bootstrap-colorpicker.js"></script>
<script src="/js/admin/common.js"></script>
<script src="/js/admin_template_new/jquery-ui.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

        // assuming the controls you want to attach the plugin to
        // have the "datepicker" class set
        $('input.datepicker').Zebra_DatePicker();

        // SORT GALLERY

        $('.ui-sortable.gallery').sortable({
            stop: function() {
                let data = $('.ui-sortable.gallery input').serialize();
                console.log(data);
                $.ajax({
                    url: '/admin/gallery/changeimagessort',
                    method: 'post',
                    data: data,
                    success: function () {
                        
                    }
                });
            }
        });

    });
</script>


<!--<script src="/js/admin_template_new/vendor.bundle.addons.js"></script>-->



</body>
</html>



