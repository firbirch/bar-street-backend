<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 01/02/2019
 * Time: 10:38
 */
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?=$this->pageTitle?></title>
	<!-- plugins:css -->
	<link rel="stylesheet" href="/css/admin_template_new/materialdesignicons.css">
	<link rel="stylesheet" href="/css/admin_template_new/vendor.bundle.base.css">
	<link rel="stylesheet" href="/css/admin_template_new/vendor.bundle.addons.css">
	<link rel="stylesheet" href="/css/admin_template_new/style.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="/css/admin_template_new/style.css">
	<!-- endinject -->
</head>

<body>
<div class="container-scroller">
	<?=$content?>
	<!-- page-body-wrapper ends -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="/js/admin/main.admin.js"></script>
<script src="/js/admin_template_new/off-canvas.js"></script>
<script src="/js/admin_template_new/misc.js"></script>
<script src="/js/admin_template_new/dashboard.js"></script>




<!--
<script src="/js/admin_template_new/off-canvas.js"></script>
<script src="/js/admin_template_new/hoverable-collapse.js"></script>
<script src="/js/admin_template_new/misc.js"></script>
<script src="/js/admin_template_new/settings.js"></script>
<script src="/js/admin_template_new/js/todolist.js"></script>
-->



</body>

</html>
