(function ($) {
    'use strict';
    $(function () {
        $('.file-upload-browse').on('click', function () {
            var file = $(this).parent().parent().parent().find('.file-upload-default');
            file.trigger('click');
        });
        $('.file-upload-default').on('change', function () {
            $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
        });
    });
})(jQuery);

(function ($) {
    'use strict';
    $(function () {

        let $element;
        $('#nestable3').nestable({
            beforeDragStop: function (l, e, p) {
                $element = e;
            }
        }).on('change', function () {
            let action;
            let id = $element.attr('data-id');
            let targetId;
            if ($element.next().attr('data-id')) {
                targetId = $element.next().attr('data-id');
                action = 'before';
            } else if ($element.prev().attr('data-id')) {
                targetId = $element.prev().attr('data-id');
                action = 'after';
            } else if ($element.closest('ol').closest('li').attr('data-id')) {
                targetId = $element.closest('ol').closest('li').attr('data-id');
                action = 'append';
            }
            if (action && id && targetId) {
                let data = {'pos': {'id': id, 'targetId': targetId, 'action': action}};
                console.log(data);
                $.ajax({
                    url: '/admin/menu/changePosition/',
                    method: 'post',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        });





        //Change sidebar and content-wrapper height
        applyStyles();

        function applyStyles() {
            //Applying perfect scrollbar
            if ($('.scroll-container').length) {
                const ScrollContainer = new PerfectScrollbar('.scroll-container');
            }
        }

        //checkbox and radios
        $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');


        $(".purchace-popup .popup-dismiss").on("click", function () {
            $(".purchace-popup").slideToggle();
        });
    });
})(jQuery);