$(document).ready(function(){
	let $element;
	$('#nestable3').nestable({
		beforeDragStop: function(l,e, p){
			$element = e;
		}
	}).on('change', function () {
		let action;
		let id = $element.attr('data-id');
		let targetId;
		if($element.next().attr('data-id')) {
			targetId = $element.next().attr('data-id');
			action = 'before';
		} else if($element.prev().attr('data-id')) {
			targetId = $element.prev().attr('data-id');
			action = 'after';
		} else if($element.closest('ol').closest('li').attr('data-id')) {
			targetId = $element.closest('ol').closest('li').attr('data-id');
			action = 'append';
		}
		if(action && id && targetId) {
			let data = {'pos':{'id': id, 'targetId': targetId, 'action': action}};
			console.log(data);
			$.ajax({
				url: '/admin/menu/changePosition/',
				method: 'post',
				data: data,
				dataType: 'json',
				success: function (data) {
					console.log(data);
				}
			});
		}
	});
});