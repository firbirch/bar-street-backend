<?php

if(!class_exists('Imagick')) {
	exit('Нужно установить imagick');
}

require_once (dirname(__FILE__).'/protected/vendor/autoload.php');

define('DOC_ROOT', realpath(__DIR__) . "/");

if($_SERVER['SERVER_PORT'] == 8888)
	define('YII_DEBUG', true);
else
	define('YII_DEBUG', false);

// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/vendor/yiisoft/yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

date_default_timezone_set('Europe/Moscow');

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
Yii::setPathOfAlias('giix', dirname(__FILE__).'/protected/vendor/assisrafael/giix');
require_once($yii);
Yii::createWebApplication($config)->run();
